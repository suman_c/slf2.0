


@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
cd C:\Framework_Desktop
set slf-framework=C:\Framework_Desktop\
set jars-dir=C:\Framework_Desktop\Jars
::set brand-name=%~1
set propertypath=%~1
::if not exist %jars-dir% (mkdir %jars-dir%)
::call downloadSLFJarAndDrivers.bat "%slf-framework%"

javac -d %slf-framework%\bin -cp "%jars-dir%\*;%jars-dir%\AppliTool_Libraries\*;%jars-dir%\*" %slf-framework%\src\com\driver\DriverExcuteThrough_Threads.java %slf-framework%\src\com\driver\Driver.java src\com\utilities\DriverReusable.java %slf-framework%\src\com\commonfunctions\CommonFunctions.java src\com\commonfunctions\Action.java %slf-framework%\src\com\utilities\Constants.java src\com\utilities\ReadExcel.java %slf-framework%\src\com\utilities\Constants.java %slf-framework%\src\com\utilities\CreateXMLSummery.java

::javac -d bin -cp "libraries\*;libraries\AppliTool_Libraries\*;drivers\*" src\com\commonfunctions\CommonFunctions.java
start java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.driver.DriverExcuteThrough_Threads %propertypath%
endlocal
goto :eof


