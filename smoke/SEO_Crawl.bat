@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
cd C:\Framework_Desktop
set slf-framework=C:\Framework_Desktop\
set jars-dir=C:\Jars

javac -d %slf-framework%\bin -cp "%jars-dir%\*;" %slf-framework%\src\com\summaryreportgenerator\crawl.java
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.summaryreportgenerator.crawl C:\Tomcat\webapps\Reports\Live.csv C:\Tomcat\webapps\Reports\PAT.csv 

endlocal
goto :eof

pause

