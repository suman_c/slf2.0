@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
cd C:\Framework_Desktop
set slf-framework=C:\Framework_Desktop\
set jars-dir=C:\Jars
::taskkill /im chromedriver.exe /f
::taskkill /im chrome.exe /f
::if not exist %jars-dir% (mkdir %jars-dir%)
::call downloadSLFJarAndDrivers.bat "%slf-framework%"
javac -d %slf-framework%\bin -cp "%jars-dir%\*;%jars-dir%\AppliTool_Libraries\*;%jars-dir%\*" %slf-framework%\src\com\driver\DriverExcuteThrough_Threads.java %slf-framework%\src\com\driver\Driver.java %slf-framework%\src\com\utilities\DriverReusable.java %slf-framework%\src\com\commonfunctions\CommonFunctions.java %slf-framework%\src\com\commonfunctions\Action.java %slf-framework%\src\com\utilities\Constants.java %slf-framework%\src\com\utilities\ReadExcel.java

:::::::::::::::::::::::::::::::::ST7 Environment:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_TH-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_FC-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IE-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_CR-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_THFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IEFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_THMC-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_FCMC-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_THCA-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_CDHH-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_CD-desktop

start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_SE-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_DK-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_FI-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NO-desktop

start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_SEFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_DKFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_NOFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_FIFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_Retail_SEFO-desktop
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_NOR_Retail_NOFO-desktop

start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_TH-RetailUK-HO
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_TH-RetailUK-3P
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IE-RetailUK-HO
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IE-RetailUK-3P
::start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IE-RetailIEFO-3PHO
::start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_TH-RetailTHFO-3PHO

::start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_TH-RetailUK-STK
::start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads ST7_IE-RetailUK-STK


:::::::::::::::::::::::::::::::::ST7 Environment:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
endlocal
goto :eof

pause
endlocal
goto :eof