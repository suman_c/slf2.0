<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/"> 
  <html>
  <head>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <script type='text/javascript' src='http://code.jquery.com/jquery-1.10.1.min.js'></script>
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	<script src='http://cdn.rawgit.com/noelboss/featherlight/1.0.4/release/featherlight.min.js' type='text/javascript' charset='utf-8'></script>
	<script type='text/javascript'>
	
	    google.load('visualization','1', {packages:['corechart']})
	    google.setOnLoadCallback(testsChart)
	    
	   function testsChart() {
		var data = google.visualization.arrayToDataTable([
				  ['Test Status','count'],
				  ['Pass', $('td.pass').length],
				  ['Fail', $('td.fail').length]
				]);
				var options = {
				  backgroundColor: { fill:'transparent' },
				  chartArea: {'width': '65%', 'height': '65%'},
				  colors: ['#00af00', 'red'],
				  fontName: 'Source Sans Pro',
				  fontSize: '12',
				  titleTextStyle: { color: '#1366d7', fontSize: '14' },
				  height: 275,
				  is3D: true,
				  pieSliceText: 'value', 
				  title: 'Pass/Fail summary', 
				  width: 450
				};
				var chart = new google.visualization.PieChart(document.getElementById('step-status-dashboard'));
				chart.draw(data, options);
			  }
	 </script>
 </head>
   <body>
   <div style='margin:0 auto;'>
     <div align="center" id ="header" style="display: block; height: 10%; width: 100%; top: 0px;background: none repeat scroll 0% 0% rgb(194, 194, 195);">
	  
<h3 style="font-size:20px"><u>Booking Reference Numbers of Successful Bookings</u></h3>
	 </div>
	 
	<br></br>
	  
      
    <div class="scenarioinfo">
	 <table border="2" style="width:100%;">
	      <thead>
		   <tr bgcolor="yellow">
		      	  <th style="text-align:center;">Scenario Name</th>
			  <th style="text-align:center;">FileName</th>
			  <th style="text-align:center;">Booking Reference Numbers</th>
			</tr>
          </thead>
         <tbody>
          <xsl:for-each select="SummaryResult/Scenarios/ScenDescStatus">
          <xsl:choose>
            <xsl:when test="ScenStatus = 'FAIL'">
             <tr>
			  
			   <td style="text-align:left;"><xsl:value-of select="ScenarioName"></xsl:value-of></td>
			   <td style="text-align:left;"><xsl:value-of select="FileName"></xsl:value-of></td>
			   <td>
			      <xsl:for-each select="ErrorDetails/Tracks">
			      <table>
			       <tr>
			          <td style="text-align:left; border: 1px solid black;"><xsl:value-of select="PassFail"></xsl:value-of></td>
			        </tr>
			      </table>
			     </xsl:for-each> 
			  </td>
			
		   </tr>
		   </xsl:when>

		  <xsl:otherwise>
           <tr>
			 
			  <td style="text-align:left;"><xsl:value-of select="ScenarioName"></xsl:value-of></td>
			  <td style="text-align:left;"><xsl:value-of select="FileName"></xsl:value-of></td>
			  <td>
			     <xsl:for-each select="ErrorDetails/Tracks">
			     <table>
			        <tr>
			           <td style="text-align:left;border: 1px solid black;"><xsl:value-of select="PassFail"></xsl:value-of></td>
			        </tr>
			     </table>
			     </xsl:for-each>
			   </td>
			   
		    </tr>	
		   </xsl:otherwise> 
		   </xsl:choose>
		 </xsl:for-each>
		</tbody>
     </table>
   </div>
   </div>
</body>
 </html>
</xsl:template>
</xsl:stylesheet>