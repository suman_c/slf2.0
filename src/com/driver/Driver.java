package com.driver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.commonfunctions.Action;
import com.relevantcodes.extentreports.LogStatus;
import com.utilities.Constants;
import com.utilities.DriverReusable;
import com.utilities.ReadExcel;

public class Driver {

	public static int rep,crtReport,totTestData = 1, ct = 0, t;
	public static ReadExcel excelSheet = new ReadExcel();
	public static List<Integer> stEnd = new ArrayList<>();
	public static boolean isExecute = false, flagRepeat = false,
			isTestDatNa = false;
	public static Iterator<Object[]> testData = null;
	public static boolean isTestPassed = true;
	public static String scriptPath;
	public static String applicationexecutrurl;
	public Set<String> prptyData;
	public ConcurrentHashMap<Integer, String> browserEnv;
	int totSuccess, testDataNo;
	Map<Integer, List<Integer>> tstCount = new HashMap<>();
	DriverReusable reusable = new DriverReusable();
	Action actObj = new Action();
	Constants cons = new Constants();
	Logger logger = Logger.getLogger(this.getClass().toString());

	String brandName = null;
	boolean brandFound = false;

	/**
	 * The method to read the test script sheet path from property file It
	 * accepts the index value test script sheet
	 * 
	 * @param bndName
	 *            ,prptyPath
	 * @throws InterruptedException
	 */
	public void fileToRead(String bndName, String prptyPath)
			throws InterruptedException {
		Properties prop = new Properties();
		String tstScriptpath, tstShtName, testDataPath, reportName;
		String tstScrptPrptyPath, drvrPrptyPath, envPrptyPath;
		/**
		 * The following code used to get all properties keys From test script
		 * sheet properties file set the boolean value true if given brand name
		 * found in property file.
		 */
		if (prptyPath.length() == 0) {
			tstScrptPrptyPath = actObj.getProjectPath()
					+ Constants.CONSTANTS_SCRIPTS_PATH;
			drvrPrptyPath = actObj.getProjectPath()
					+ Constants.CONSTANTS_DRIVERS_PATH;
			envPrptyPath = "";
		} else {
			tstScrptPrptyPath = prptyPath
					+ File.separator
					+ Constants.CONSTANTS_SCRIPTS_PATH.replace("conf", "")
							.replace("\\", "");
			drvrPrptyPath = prptyPath
					+ File.separator
					+ Constants.CONSTANTS_DRIVERS_PATH.replace("conf", "")
							.replace("\\", "");
			envPrptyPath = prptyPath
					+ File.separator
					+ Constants.CONSTANTS_BROWSERENV_PATH.replace("conf", "")
							.replace("\\", "");
		}
		prptyData = actObj.readPropertiesData(tstScrptPrptyPath);
		Iterator<String> repeat = prptyData.iterator();
		while (repeat.hasNext()) {
			if (repeat != null) {
				brandName = repeat.next();
				if (bndName.trim().equals(brandName.trim())) {
					logger.info("The given brandname found in property file hance execution will start "
							+ brandName.trim());
					brandFound = true;
					break;
				}
			}
		}
		if (brandFound) {
			/**
			 * The following code for reading the driver details. From SLF
			 * driver properties file.
			 */
			try {
				prop.load(new FileInputStream(drvrPrptyPath));
				try {
					String platform = prop.getProperty("execution.platform")
							.trim();
					Action.reportPath = prop.getProperty(
							platform + ".report.path").trim();
					Action.onMode = prop
							.getProperty("testscrpt.execution.mode").trim();
					if (!Action.onMode.toLowerCase().contains("browserstack")) {
						Constants.CONSTANTS_CHROME_DRIVER_PATH = prop
								.getProperty(platform + ".chrome.driver.path")
								.trim();
						Constants.CONSTANTS_IE_DRIVER_PATH = prop.getProperty(
								platform + ".ie.driver.path").trim();
						Constants.CONSTANTS_PHANTOM_DRIVER_PATH = prop
								.getProperty(platform + ".phantom.driver.path")
								.trim();
						Constants.CONSTANTS_GECKO_DRIVER_PATH = prop
								.getProperty(platform + ".gecko.driver.path")
								.trim();
					}
					Action.browserStack_UserName = prop.getProperty(
							"browserstack.user.name").trim();
					Action.browserStack_Password = prop.getProperty(
							"browserstack.user.password").trim();
				} catch (NullPointerException n) {
					logger.error("Null pointer exception while reading driver property file:"
							+ n.getMessage());
					System.exit(0);
				}
			} catch (FileNotFoundException f) {
				logger.error("File not found exception for " + drvrPrptyPath
						+ "\t" + f.getMessage());
				System.exit(0);
			} catch (Exception e) {
				logger.error("Exception while reading driver properties file:"
						+ e.getMessage());
			}

			/**
			 * The following code used to read the value for Test script,data
			 * path and report name etc. From the test script sheet properties
			 * file
			 */
			try {
				// rnMode = actObj.prop.getProperty(brandName +
				// ".runMode").trim();
				tstScriptpath = actObj.prop.getProperty(
						brandName + ".scriptPath").trim();
				tstShtName = actObj.prop.getProperty(
						brandName + ".scriptSheetName").trim();
				Constants.CONSTANTS_OR_PATH = actObj.prop.getProperty(
						brandName + ".objDir").trim();
				testDataPath = actObj.prop.getProperty(brandName + ".dataPath")
						.trim();
				reportName = actObj.prop.getProperty(brandName + ".reportName")
						.trim();
	 applicationexecutrurl=			Constants.CONSTANTS_AAPLNURL = actObj.prop.getProperty(
						brandName + ".appUrl").trim();
	
				Constants.CONSTANTS_EXECUTIONCRITERIA = actObj.prop
						.getProperty(brandName + ".executionCriteria").trim();
				actObj.browserreportscreenshot=Constants.CONSTANTS_EXECUTIONCRITERIA;
				actObj.setDifferentBrowserInfo(brandName, envPrptyPath);
				if (Action.onMode.toLowerCase().contains("grid")) {
					Constants.CONSTANTS_IPDETAILS = actObj.prop.getProperty(
							brandName + ".ipdetails").trim();
				}
				ScriptlessAutomation(tstScriptpath, tstShtName, testDataPath,
						reportName);
			} catch (NullPointerException n) {
				logger.error("Null pointer exeception occured while reading the test script properties file:"
						+ n.getMessage());
				System.exit(0);
			}
		} else {
			logger.info("The given brand couldn't found in property file please look the property file");
		}
	}

	/**
	 * The following method used to read the test case and test data in order to
	 * execute the tests steps
	 * 
	 * @param RunMode
	 * @param path
	 * @param tsShtName
	 * @param testDataPath
	 * @param report
	 * @throws InterruptedException
	 */
	// @Test(dataProvider="TestScript")
	public void ScriptlessAutomation(String path, String tsShtName,
			String testDataPath, String report) throws InterruptedException {

		// if(runMode.equalsIgnoreCase("yes"))
		// {
		try {
			logger.info("Path: " + path + " *** Report " + report + "\t"
					+ testDataPath);
			Action.successCount.clear();
			scriptPath = path;
			excelSheet.openSheet(path, tsShtName);
			Integer rowCt = excelSheet.getRowCount();
			tstCount = reusable.testCaseCount(rowCt);
			int j;
			outerloop:
			for (t = 0; t < tstCount.size(); t++) // Loop to iterate the no of test cases
															
			{
				crtReport = 0;
				if (!testDataPath.equals("")) {
					totTestData = actObj.getTestDataAtCoreNew(testDataPath);
				}
				logger.info("The total test data:" + totTestData);
				isTestDatNa = false;
				//actObj.setReportingDetails(report + t);
				stEnd = tstCount.get(t);
				if(totTestData==0) {
				actObj.setReportingDetails(report+" No test data for Execution");
				actObj.report.log(LogStatus.ERROR, "There is No test data for execution from File "+testDataPath);
				}
				// int totTestData = actObj.getTestData1(testDataPath), j;
				innerloop: 
					for (rep = 1; rep <= totTestData; rep++) { // Loop to iterate the test data												
					if (t != 0) {
						totSuccess = Action.successCount.size();
						// for (j = 0; j < totSuccess; j++) { //old one
						for (j = 0; j < totSuccess && isTestDatNa == false; j++) {
							rep = Action.successCount.get(j);
							if (rep != 0) {
								reusable.captureContent(stEnd,report);
							}
							Iterator<Integer> it = Action.successCount.iterator();
							while (it.hasNext()) {
								int val = it.next();
								if (val == Driver.rep
										&& Driver.isExecute == false) {
									Action.successCount.set(j, 0);
									break;
								}
							}
							 /* if(j==totSuccess-1){ //Unable this if block when we want to execute the testcase for N no of testdata
							    break innerloop;
							  }*/
						}
						if ((isExecute == false && totSuccess == 0)
								&& rep != totTestData)
							// if (isExecute == false && totSuccess == 0) //old one
							reusable.captureContent(stEnd,report);
						if (isExecute)
							break innerloop;
						if ((totSuccess == 0 && rep == totTestData)
								&& flagRepeat == true) {
							logger.info("There is no success data page for proceeding "
									+ "the next testCase execution");
							break outerloop;
						}
						if ((totSuccess == 0 && rep == totTestData)
								&& flagRepeat == false) {
							reusable.captureContent(stEnd,report);
						}
						Action.successCount.clear(); // To clear the succestestdata for each iteration
					} else {
						   reusable.captureContent(stEnd,report);
						if (isExecute) { // For executing the test case for different set of test data comment this if block
							break;
						} else {
							
							if ((isExecute == false && rep == totTestData)
									&& flagRepeat == true) {
								logger.info("There is no success data page for proceeding "
										+ "the next testCase execution so its stopped");
								break outerloop;
							}
						}
					}
					crtReport++;
				}
			}
		} catch (Exception e) {
			isTestPassed = false;
		}
		// }
	} // Method ends here
}
