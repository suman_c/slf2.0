package com.utilities;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import jxl.Workbook;
import jxl.write.Alignment;
import jxl.write.Border;
import jxl.write.BorderLineStyle;
import jxl.write.Colour;
import jxl.write.Label;
import jxl.write.VerticalAlignment;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/*
</p>
* This class is responsible for crate the Summery report in excel format
* @author Harivardhan.a
* @company Sonata Software
* @createdDate 29/06/2017
* @history Initial Version. 
* </p>  
*/
@SuppressWarnings("deprecation")
public class ExportSummeryReport {

	private static final String EXCEL_FILE_LOCATION = "src\\SummaryReport.xls";
	private static final String EXCEL_FILE_LOCATION_FOR_FAIL = "src\\FailSummaryReport.xls";
	private static final String PASSED_LABEL = "Passed";
	private static final String FILED_LABEL = "Failed";
	private static final String IE_BROWSER = "IE";
	private static final String FIREFOX_BROWSER = "Firefox";
	private static final String CHROME_BROWSER = "Chrome";
	private static final String SAFARI_BROWSER = "Safari";
    private static final String IPAD_BROWSER = "Ipad";
    private static final String IPHONE_BROWSER = "Iphone";

	private static final String MOBILE_IOS = "Mobile-iOS";
	private static final String MOBILE_ANDROID = "Mobile-Andriod";
	private static final String TAB_IOS = "Tab-iOS";
	private static final String TAB_ANDROID = "Tab-Andriod";

	private static final String ENVIRONMENT = "Environment";
	private static final String BRAND = "Brand";
	private static final String SCENARIONAME ="SecnarioName";
	private static final String FAILURE = "Failure";
	private static final String ERROR = "Error";
	private static final String URL = "Url";

	// Logger variable
	private static Logger logger = Logger.getLogger(ExportSummeryReport.class);

	@SuppressWarnings("rawtypes")
	public static void createXSLSummaryReport(HashMap<String, SummaryReportVO> summeryMap) {

		// workbook objet reference
		WritableWorkbook workbook = null;
		try {

			workbook = Workbook.createWorkbook(new File(EXCEL_FILE_LOCATION));

			// create an Excel sheet
			WritableSheet excelSheet = workbook.createSheet("Sheet 1", 0);

			// creating font style
			WritableFont cellFont = new WritableFont(WritableFont.ARIAL, 10);
			cellFont.setColour(Colour.BLACK);
			WritableCellFormat cellDataFormat = new WritableCellFormat(cellFont);
			cellDataFormat.setAlignment(Alignment.CENTRE);
			cellDataFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

			// creating cell format
			WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
			cellFormat.setBackground(Colour.GRAY_25);
			cellFormat.setAlignment(Alignment.CENTRE);
			cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

			// creating cell format
			WritableCellFormat passedCellFormat = new WritableCellFormat(cellFont);
			passedCellFormat.setBackground(Colour.GREEN);
			passedCellFormat.setAlignment(Alignment.CENTRE);
			passedCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			passedCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

			// creating cell format
			WritableCellFormat failedCellFormat = new WritableCellFormat(cellFont);
			failedCellFormat.setBackground(Colour.RED);
			failedCellFormat.setAlignment(Alignment.CENTRE);
			failedCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
			failedCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);

			// label Creations
			Label envLabel = new Label(0, 0, ENVIRONMENT, cellFormat);
			Label brandLabel = new Label(1, 0, BRAND, cellFormat);

			Label ieLabel = new Label(2, 0, IE_BROWSER, cellFormat);
			Label fireFoxLabel = new Label(4, 0, FIREFOX_BROWSER, cellFormat);
			Label chromeLabel = new Label(6, 0, CHROME_BROWSER, cellFormat);
			Label mobileIOSLabel = new Label(8, 0, MOBILE_IOS, cellFormat);
			Label mobileANDROIDLabel = new Label(10, 0, MOBILE_ANDROID, cellFormat);
			Label tabIOSLabel = new Label(12, 0, TAB_IOS, cellFormat);
			Label tabANDROIDLabel = new Label(14, 0, TAB_ANDROID, cellFormat);
			Label safariLabel = new Label(16, 0, SAFARI_BROWSER, cellFormat);
            Label ipadLabel = new Label(18, 0, IPAD_BROWSER, cellFormat);
            Label iphoneLabel = new Label(20, 0, IPHONE_BROWSER, cellFormat);

			Label iePassedLabel = new Label(2, 1, PASSED_LABEL, cellFormat);
			Label ieFailedLabel = new Label(3, 1, FILED_LABEL, cellFormat);

			Label fireFoxPassedLabel = new Label(4, 1, PASSED_LABEL, cellFormat);
			Label fireFoxFailedLabel = new Label(5, 1, FILED_LABEL, cellFormat);

			Label chromePassedLabel = new Label(6, 1, PASSED_LABEL, cellFormat);
			Label chromeFailedLabel = new Label(7, 1, FILED_LABEL, cellFormat);

			Label mobIOSPassedLabel = new Label(8, 1, PASSED_LABEL, cellFormat);
			Label mobIOSFailedLabel = new Label(9, 1, FILED_LABEL, cellFormat);

			Label mobANDROIDPassedLabel = new Label(10, 1, PASSED_LABEL, cellFormat);
			Label mobANDROIDFailedLabel = new Label(11, 1, FILED_LABEL, cellFormat);

			Label tabIOSPassedLabel = new Label(12, 1, PASSED_LABEL, cellFormat);
			Label tabIOSFailedLabel = new Label(13, 1, FILED_LABEL, cellFormat);

			Label tabANDROIDPassedLabel = new Label(14, 1, PASSED_LABEL, cellFormat);
			Label tabANDROIDFailedLabel = new Label(15, 1, FILED_LABEL, cellFormat);
			
			Label safariPassedLabel = new Label(16, 1, PASSED_LABEL, cellFormat);
            Label safariFailedLabel = new Label(17, 1, FILED_LABEL, cellFormat);
            
            Label ipadPassedLabel = new Label(18, 1, PASSED_LABEL, cellFormat);
            Label ipadFailedLabel = new Label(19, 1, FILED_LABEL, cellFormat);
            
            Label iphonePassedLabel = new Label(20, 1, PASSED_LABEL, cellFormat);
            Label iphoneFailedLabel = new Label(21, 1, FILED_LABEL, cellFormat);

			excelSheet.setColumnView(0, 15);
			excelSheet.setColumnView(1, 38);
			excelSheet.setColumnView(4, 30);
			excelSheet.addCell(envLabel);

			excelSheet.addCell(brandLabel);
			excelSheet.mergeCells(2, 0, 3, 0);
			excelSheet.mergeCells(4, 0, 5, 0);
			excelSheet.mergeCells(6, 0, 7, 0);
			excelSheet.mergeCells(8, 0, 9, 0);
			excelSheet.mergeCells(10, 0, 11, 0);
			excelSheet.mergeCells(12, 0, 13, 0);
			excelSheet.mergeCells(14, 0, 15, 0);
			excelSheet.mergeCells(16, 0, 17, 0);
            excelSheet.mergeCells(18, 0, 19, 0);
            excelSheet.mergeCells(20, 0, 21, 0);

			excelSheet.addCell(ieLabel);
			excelSheet.addCell(iePassedLabel);
			excelSheet.addCell(ieFailedLabel);

			excelSheet.addCell(fireFoxPassedLabel);
			excelSheet.addCell(fireFoxFailedLabel);
			excelSheet.addCell(chromePassedLabel);
			excelSheet.addCell(chromeFailedLabel);
			excelSheet.addCell(mobIOSPassedLabel);
			excelSheet.addCell(mobIOSFailedLabel);
			excelSheet.addCell(mobANDROIDPassedLabel);
			excelSheet.addCell(mobANDROIDFailedLabel);
			excelSheet.addCell(tabIOSPassedLabel);
			excelSheet.addCell(tabIOSFailedLabel);
			excelSheet.addCell(tabANDROIDPassedLabel);
			excelSheet.addCell(tabANDROIDFailedLabel);
			excelSheet.addCell(safariPassedLabel);
            excelSheet.addCell(safariFailedLabel);
            excelSheet.addCell(ipadPassedLabel);
            excelSheet.addCell(ipadFailedLabel);
            excelSheet.addCell(iphonePassedLabel);
            excelSheet.addCell(iphoneFailedLabel);

			excelSheet.addCell(fireFoxLabel);
			excelSheet.addCell(chromeLabel);
			excelSheet.addCell(mobileIOSLabel);
			excelSheet.addCell(mobileANDROIDLabel);
			excelSheet.addCell(tabIOSLabel);
			excelSheet.addCell(tabANDROIDLabel);
			excelSheet.addCell(safariLabel);
            excelSheet.addCell(ipadLabel);
            excelSheet.addCell(iphoneLabel);

			int i = 0;

			Iterator it = summeryMap.entrySet().iterator();
			while (it.hasNext()) {

				Map.Entry pair = (Map.Entry) it.next();

				SummaryReportVO vo = (SummaryReportVO) pair.getValue();

				excelSheet.mergeCells(0, i + 2, 0, i + 5);
				Label envrLabel = new Label(0, i + 2, pair.getKey().toString(), cellDataFormat);
				excelSheet.addCell(envrLabel);

				for (String bsrand : vo.getBrands()) {

					Label brLabel = new Label(1, i + 2, bsrand, cellDataFormat);
					excelSheet.addCell(brLabel);

					HashMap<String, TestCaseCount> cc = vo.getTestCaseCount();
					Iterator it1 = cc.entrySet().iterator();
					while (it1.hasNext()) {
						Map.Entry pair1 = (Map.Entry) it1.next();
						TestCaseCount tc = (TestCaseCount) pair1.getValue();
						if (tc.getBrowserClass().get(IE_BROWSER) != null) {
							// if (tc.getBrowser().equals("IE")) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(2, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IE_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(3, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IE_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get(FIREFOX_BROWSER) != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(4, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get(FIREFOX_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(5, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get(FIREFOX_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get(CHROME_BROWSER) != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(6, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get(CHROME_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(7, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get(CHROME_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get("Mobile-iOS") != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(8, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Mobile-iOS").getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(9, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Mobile-iOS").getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get("Mobile-Andriod") != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(10, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get("Mobile-Andriod").getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(11, i + 2,
										String.valueOf(Math
												.round(tc.getBrowserClass().get("Mobile-Andriod").getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get("Tab-iOS") != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(12, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Tab-iOS").getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(13, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Tab-iOS").getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}

						}
						if (tc.getBrowserClass().get("Tab-Andriod") != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(14, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Tab-Andriod").getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(15, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get("Tab-Andriod").getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}
						}
						if (tc.getBrowserClass().get(SAFARI_BROWSER) != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(16, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(SAFARI_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(17, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(SAFARI_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}
						}
						if (tc.getBrowserClass().get(IPAD_BROWSER) != null) {
							if (bsrand.contains(pair1.getKey().toString())) {

								Label passLabel = new Label(18, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IPAD_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(19, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IPAD_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}
						}
						if (tc.getBrowserClass().get(IPHONE_BROWSER) != null) {
							if (bsrand.contains(pair1.getKey().toString())) {
								Label passLabel = new Label(20, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IPHONE_BROWSER).getPassPercentage()))
												+ "%",
										passedCellFormat);
								excelSheet.addCell(passLabel);

								Label failLabel = new Label(21, i + 2,
										String.valueOf(
												Math.round(tc.getBrowserClass().get(IPHONE_BROWSER).getFailPercentage()))
												+ "%",
										failedCellFormat);
								excelSheet.addCell(failLabel);

								/*
								 * Label tot = new Label(16, i + 2,
								 * String.valueOf(tc.getPassed() + "_" +
								 * tc.getFailed() + "_" + tc.getTotal()),
								 * cellDataFormat); excelSheet.addCell(tot);
								 */

							}
						}

					}

					i++;
				}

			}

			int j = i + 7;
			Label envLabel1 = new Label(0, j, ENVIRONMENT, cellFormat);
			Label brandLabel1 = new Label(1, j, BRAND, cellFormat);

			Label passed1 = new Label(2, j, PASSED_LABEL, cellFormat);
			Label failed1 = new Label(3, j, FILED_LABEL, cellFormat);
			Label avg = new Label(4, j, "Avg Time taken for each test (Min)", cellFormat);

			excelSheet.addCell(envLabel1);
			excelSheet.addCell(brandLabel1);
			excelSheet.addCell(passed1);
			excelSheet.addCell(failed1);
			excelSheet.addCell(avg);

			int totPassed = 0, totFailed = 0, count = 0;
			float totAvgTime = 0;

			Iterator iterator = summeryMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry reportPair = (Map.Entry) iterator.next();
				SummaryReportVO reportVo = (SummaryReportVO) reportPair.getValue();
				excelSheet.mergeCells(0, j + 2, 0, j + 5);
				Label envrLabel1 = new Label(0, j + 2, reportPair.getKey().toString(), cellDataFormat);
				excelSheet.addCell(envrLabel1);

				for (String bsrand : reportVo.getBrands()) {

					Label brLabel = new Label(1, j + 2, bsrand, cellDataFormat);
					excelSheet.addCell(brLabel);

					HashMap<String, TestCaseCount> cc = reportVo.getTestCaseCount();
					Iterator it1 = cc.entrySet().iterator();
					while (it1.hasNext()) {
						Map.Entry pair1 = (Map.Entry) it1.next();
						if (bsrand.contains(pair1.getKey().toString())) {

							TestCaseCount tc = (TestCaseCount) pair1.getValue();
							Label passedlabel = new Label(2, j + 2, String.valueOf(Math.round(tc.getPassed())),
									cellDataFormat);
							totPassed = totPassed + Math.round(tc.getPassed());
							excelSheet.addCell(passedlabel);

							Label Failedlabel = new Label(3, j + 2, String.valueOf(Math.round(tc.getFailed())),
									cellDataFormat);
							totFailed = totFailed + Math.round(tc.getFailed());
							excelSheet.addCell(Failedlabel);
							Label avgTime = new Label(4, j + 2,
									String.valueOf(Math.round(tc.getExecutionTime() / tc.getTotal())), cellDataFormat);
							count++;

							totAvgTime = totAvgTime + (tc.getExecutionTime() / tc.getTotal());
							excelSheet.addCell(avgTime);
						}

					}

					j++;
				}

			}

			int k = j + 2;

			Label garandTotal = new Label(0, k + 1, "Grand Total", cellDataFormat);
			Label totPass = new Label(2, k + 1, String.valueOf(totPassed), cellDataFormat);
			Label totFail = new Label(3, k + 1, String.valueOf(totFailed), cellDataFormat);
			Label totalAvgTime = new Label(4, k + 1, String.valueOf(totAvgTime / count), cellDataFormat);

			excelSheet.addCell(garandTotal);
			excelSheet.addCell(totPass);
			excelSheet.addCell(totFail);
			excelSheet.addCell(totalAvgTime);

			workbook.write();
            logger.info("Summery report exported..");
            CreateXMLSummery.addToZipFile("src\\SummeryReport.xml");

		} catch (IOException e) {
			logger.error("The I/O error while adding the summary" + e);

		} catch (RowsExceededException e) {
			logger.error("The RowsExceededException  while adding the summary" + e);
		} catch (WriteException e) {
			logger.error("The WriteException  while adding the summary" + e);
		} finally {

			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
					logger.error("The IOException  while closing work book" + e);
				} catch (WriteException e) {
					logger.error("The WriteException  while closing work book" + e);
				}
			}

		}

	}

	public static void readXML() {

		HashMap<String, SummaryReportVO> summeryMap = new LinkedHashMap<String, SummaryReportVO>();
		try {
			File fXmlFile = new File("src\\SummeryReport.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();
			logger.info("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList nList = doc.getElementsByTagName("ENV");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String environment = eElement.getAttribute("id");
					String brand = eElement.getElementsByTagName("BRAND").item(0).getTextContent();
					String browser = eElement.getElementsByTagName("BROWSER").item(0).getTextContent();
					String passed = eElement.getElementsByTagName("PASSED").item(0).getTextContent();
					String failed = eElement.getElementsByTagName("FAILED").item(0).getTextContent();
					String total = eElement.getElementsByTagName("TOTAL").item(0).getTextContent();
					String differenceTime = eElement.getElementsByTagName("EXECUTIONTIME").item(0).getTextContent();

					if (!summeryMap.containsKey(environment)) {

						SummaryReportVO reportVo = new SummaryReportVO();
						reportVo.setBrandName(brand);
						reportVo.setEnv(environment);
						HashMap<String, TestCaseCount> testCaseMap = new HashMap<String, TestCaseCount>();
						TestCaseCount count = new TestCaseCount();
						count.setPassed(new Float(passed));
						count.setFailed(new Float(failed));
						count.setTotal(new Float(total));
						count.setBrowser(browser);
						count.setExecutionTime(new Long(differenceTime));

						HashMap<String, Browser> browserMap = new HashMap<String, Browser>();
						Browser browserClass = new Browser();
						browserClass.setTotal(new Float(new Float(passed) + new Float(failed)));
						browserClass.setFailed(new Float(failed));
						browserClass.setPassed(new Float(passed));
						browserClass.setFailPercentage((browserClass.getFailed() / browserClass.getTotal()) * 100);
						browserClass.setPassPercentage((browserClass.getPassed() / browserClass.getTotal()) * 100);
						browserMap.put(browser, browserClass);

						count.setBrowserClass(browserMap);

						testCaseMap.put(brand, count);

						reportVo.setTestCaseCount(testCaseMap);

						summeryMap.put(environment, reportVo);

					} else {

						SummaryReportVO summeryVo = summeryMap.get(environment);
						if (!summeryVo.getTestCaseCount().containsKey(brand)) {
							HashMap<String, TestCaseCount> testCaseCount = summeryVo.getTestCaseCount();
							TestCaseCount count = new TestCaseCount();
							count.setPassed(count.getPassed() + new Float(passed));
							count.setFailed(count.getFailed() + new Float(failed));
							count.setTotal(count.getTotal() + new Float(total));
							count.setExecutionTime(new Long(count.getExecutionTime() + new Long(differenceTime)));
							count.setBrowser(browser);

							HashMap<String, Browser> browserMap = new HashMap<String, Browser>();
							Browser browserClass = new Browser();
							browserClass.setPassed(new Float(passed));
							browserClass.setFailed(new Float(failed));
							browserClass.setTotal(new Float(new Float(passed) + new Float(failed)));
							browserClass.setFailPercentage((browserClass.getFailed() / browserClass.getTotal()) * 100);
							browserClass.setPassPercentage((browserClass.getPassed() / browserClass.getTotal()) * 100);

							browserMap.put(browser, browserClass);

							count.setBrowserClass(browserMap);

							testCaseCount.put(brand, count);

							summeryVo.setTestCaseCount(testCaseCount);

						} else {

							HashMap<String, TestCaseCount> count = summeryVo.getTestCaseCount();
							TestCaseCount testCase = count.get(brand);
							testCase.setPassed(testCase.getPassed() + new Float(passed));
							testCase.setFailed(testCase.getFailed() + new Float(failed));
							testCase.setTotal(testCase.getTotal() + new Float(total));
							testCase.setExecutionTime(new Long(testCase.getExecutionTime() + new Long(differenceTime)));

							if (!testCase.getBrowserClass().containsKey(browser)) {

								HashMap<String, Browser> browserMap = testCase.getBrowserClass();
								Browser b = new Browser();
								b.setPassed(new Float(passed));
								b.setFailed(new Float(failed));
								b.setTotal(new Float(passed) + new Float(failed));
								b.setFailPercentage((b.getFailed() / b.getTotal()) * 100);
								b.setPassPercentage((b.getPassed() / b.getTotal()) * 100);
								browserMap.put(browser, b);

								// testCase.setBrowserClass(browserMap);

							} else {

								HashMap<String, Browser> browserMap = testCase.getBrowserClass();
								Browser b = browserMap.get(browser);

								b.setPassed(new Float(passed) + new Float(b.getPassed()));
								b.setFailed(new Float(failed) + new Float(b.getFailed()));
								b.setTotal(new Float(b.getTotal() + new Float(total)));

								b.setFailPercentage((b.getFailed() / b.getTotal()) * 100);
								b.setPassPercentage((b.getPassed() / b.getTotal()) * 100);

							}

						}
					}

				}

			}
			
			createXSLSummaryReport(summeryMap);
		}

		catch (IOException e) {
			logger.error("The IOException  while reading XML" + e);
		} catch (ParserConfigurationException e) {
			logger.error("The ParserConfigurationException   while reading XML" + e);
		} catch (SAXException e) {
			logger.error("The SAXException   while reading XML" + e);
		}

	}
	
	/**
	 * The method to read failed xml file
	 */
	public static void readXMlForFailure() 
	{
		DocumentBuilderFactory factory;
		String scenarioName, urlVal = null, brandName = null, environment = null;
		SummaryReportVO reportObj = new SummaryReportVO();
		Map<String, Map<String, Map<String, Map<String, List<List<String>>>>>> ennvMap = new LinkedHashMap<String, Map<String, Map<String, Map<String, List<List<String>>>>>>();
		Map<String, Map<String, Map<String, List<List<String>>>>> brandMap = new LinkedHashMap<String, Map<String, Map<String, List<List<String>>>>>();
		Map<String, Map<String, List<List<String>>>> scenarioMap = new LinkedHashMap<String, Map<String, List<List<String>>>>();
		Map<String, List<List<String>>> urlMap;
		List<List<String>> failErrList;
		List<String> failList, errList;

		 try {
			factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder
					.parse(new File("src\\FailSummeryReport.xml"));
			doc.getDocumentElement().normalize();

			String rootNode = doc.getDocumentElement().getNodeName();
			logger.info("The root node of xml file:" + rootNode);

            NodeList nList =  doc.getElementsByTagName("ENV");
            for(int temp=0;temp<nList.getLength();temp++){
            	   Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element eElement = (Element) nNode;
					environment = eElement.getAttribute("id");
					brandMap = new LinkedHashMap<String,Map<String,Map<String,List<List<String>>>>>();

					brandName = eElement.getElementsByTagName("BRAND").item(0).getTextContent();
					NodeList scnNodes = eElement.getElementsByTagName("SCENARIOS").item(0).getChildNodes();
					scenarioMap = new LinkedHashMap<String,Map<String,List<List<String>>>>();
					for (int b = 0; b < scnNodes.getLength(); b++) {
						
						urlMap = new HashMap<String,List<List<String>>>();
						Node scnNode = scnNodes.item(b);
						Element subelemet = (Element) scnNode;
						scenarioName = subelemet.getElementsByTagName("SCENARIONAME").item(0)
								.getTextContent();
					
						NodeList urlValnode = subelemet.getElementsByTagName("URLVAL");
						boolean isEnter=true;
						for (int u = 0; u < urlValnode.getLength(); u++) {
							
							  Node uNode = urlValnode.item(u);
							  
							  failList = reportObj.getListObject();
							  errList = reportObj.getListObject();
							  failErrList = reportObj.getListOfListTypeObject();
							  
							 if (uNode.getNodeType() == Node.ELEMENT_NODE) {
								Element urlNode = (Element) uNode;
								urlVal = urlNode.getElementsByTagName("URL").item(0).getTextContent();
								
								NodeList failNode = urlNode.getElementsByTagName("FAIL");
								for (int f = 0; f < failNode.getLength(); f++) {
									failList.add(failNode.item(f).getTextContent());
								}

								
								NodeList errorNode = urlNode.getElementsByTagName("ERROR");
								for (int f = 0; f < errorNode.getLength(); f++) {
									errList.add(errorNode.item(f).getTextContent());
								}
							}
							
						//if (u == 0 || (failList.size()!=0 ||errList.size()!=0)) {
					    if ( isEnter ) {
					    	   failErrList.add(failList);
					           failErrList.add(errList);
					       if(failList.size()!=0 ||errList.size()!=0){
						        urlMap.put(urlVal, failErrList);
						        isEnter = false;
					        }
						 }else{
							 for(Map.Entry<String, List<List<String>>> mpVal:urlMap.entrySet()){
								 List<List<String>> ltVal = mpVal.getValue();
								 List<String> failLt = ltVal.get(0);
								 List<String> passLt = ltVal.get(1);
								 
								 failList.removeAll(failLt);
								 errList.removeAll(passLt);
								 
								if(failList.size()!=0 || errList.size()!=0){
								   failErrList.add(failList);
								   failErrList.add(errList);
								   urlMap.put(urlVal, failErrList);
								}
							 }
						  }
					   }
						if(urlMap.size()!=0)
						  scenarioMap.put(scenarioName, urlMap);
					}
				 }
				if(scenarioMap.size()!=0){
					if(!ennvMap.containsKey(environment)){
						 brandMap.put(brandName, scenarioMap);
						 ennvMap.put(environment, brandMap);
						//brandMap = new LinkedHashMap<String,Map<String,Map<String,List<List<String>>>>>();
					}
					else{
					//if(ennvMap.containsKey(environment)){
						   Map<String,Map<String,Map<String,List<List<String>>>>> brandFinalMap;
						   brandFinalMap = ennvMap.get(environment);
						   
						if(!brandFinalMap.containsKey(brandName)){
							brandMap.put(brandName, scenarioMap);
							brandFinalMap.putAll(brandMap);
							ennvMap.put(environment, brandFinalMap);
						}else{
							Map<String,Map<String,List<List<String>>>> scenarioUpdatedMap = new LinkedHashMap<String,Map<String,List<List<String>>>>();
							Map<String,List<List<String>>> urlUpdatedMap = null;
							Map<String,Map<String,List<List<String>>>> stredScnMap;
							
							stredScnMap = brandFinalMap.get(brandName);
							Iterator<String> strdScnKeySet =  stredScnMap.keySet().iterator();
							Iterator<String> ltstScnKeySet =  scenarioMap.keySet().iterator();
							
							boolean scenExit,urlExit;
							while (ltstScnKeySet.hasNext()) {
								    String ltstScnKey = ltstScnKeySet.next();
								    scenExit = false;
							  while (strdScnKeySet.hasNext()) {
								   String strdScnKey = strdScnKeySet.next(); 
								   if(strdScnKey.contains(ltstScnKey)){
									   scenExit = true;
									  Map<String,List<List<String>>> strdUrlMap = stredScnMap.get(strdScnKey);
									  Map<String,List<List<String>>> ltstUrlMap = scenarioMap.get(ltstScnKey);
									  
									 Iterator<String> strdUrlIterator = strdUrlMap.keySet().iterator();
									 Iterator<String> ltstUrlIterator = ltstUrlMap.keySet().iterator();
									 
									 urlUpdatedMap = new HashMap<String,List<List<String>>>();
									 
									 while(ltstUrlIterator.hasNext()){
										 String ltstUrlKey = ltstUrlIterator.next();
										 urlExit = false;
										 List<List<String>> ltstFailErrList = new ArrayList<List<String>>();
									  while(strdUrlIterator.hasNext()){
										 String strdUrlKey = strdUrlIterator.next();  
										  if(strdUrlKey.equals(ltstUrlKey)){
											   urlExit = true;
											  List<List<String>> strdList = strdUrlMap.get(strdUrlKey);
											  List<List<String>> ltstList = ltstUrlMap.get(ltstUrlKey);
											  
											  List<String> strdFailList = strdList.get(0);
											  List<String> strdErrorList = strdList.get(1);
											  
											  List<String> ltstFailList = ltstList.get(0);
											  List<String> ltstErrorList = ltstList.get(1);
											  
											  ltstFailList.removeAll(strdFailList);
											  ltstErrorList.removeAll(strdErrorList);
											  strdFailList.addAll(ltstFailList);
											  strdErrorList.addAll(ltstErrorList);
											  
											 // if(ltstFailList.size()!=0 || ltstErrorList.size()!=0){
											   ltstFailErrList.add(strdFailList);
											   ltstFailErrList.add(strdErrorList);
											   urlUpdatedMap.put(ltstUrlKey,ltstFailErrList);
												 //scenarioUpdatedMap.put(ltstScnKey, urlUpdatedMap);
											//} 
										 }
									   }if(urlExit == false){
										   urlUpdatedMap.put(ltstUrlKey, ltstUrlMap.get(ltstUrlKey));
									   }
									 }
									  urlUpdatedMap.putAll(strdUrlMap);
									  scenarioUpdatedMap.put(ltstScnKey, urlUpdatedMap);
								   }
								}if(scenExit==false){
									scenarioUpdatedMap.put(ltstScnKey, scenarioMap.get(ltstScnKey));
								}
							 }
							stredScnMap.putAll(scenarioUpdatedMap);
							brandMap.put(brandName, stredScnMap);
							brandFinalMap.putAll(brandMap);
							ennvMap.put(environment, brandFinalMap);
						}
					}
				// }
               }
            }
            creatXslSummaryForFailure(ennvMap);
		} catch (ParserConfigurationException e) {
			logger.error("The Pasre configuration exception in creating the xml file:"+e);
		} catch (SAXException e) {
			logger.error("The SAX Exception in creating the xml file:");
		} catch (IOException e) {
			logger.error("The IO Exception in creating the xml file:");
		}
		
	}
	
	/**
	 * The method to create the XSL summary report for failure
	 * @param envMap
	 */
	public static void creatXslSummaryForFailure(Map<String,Map<String, Map<String, Map<String, List<List<String>>>>>> envMap) {		
		WritableWorkbook  workbook = null;
     try{
    	if(EXCEL_FILE_LOCATION_FOR_FAIL.contains(".xls")){
    	 workbook = Workbook.createWorkbook(new File(EXCEL_FILE_LOCATION_FOR_FAIL));
    	 WritableSheet excelSheet =  workbook.createSheet("Sheet 1", 1);
    	 
    
    	 WritableFont wfont = new WritableFont(WritableFont.ARIAL,10);
    	 wfont.setColour(Colour.BLACK);
    	 WritableCellFormat cellTextFormat = new WritableCellFormat(wfont);
    	 cellTextFormat.setAlignment(Alignment.LEFT);
    	 cellTextFormat.setVerticalAlignment(VerticalAlignment.TOP);
    	 cellTextFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	 
    	 
    	 WritableCellFormat cellBorder = new WritableCellFormat(wfont);
    	 cellBorder.setBorder(Border.ALL, BorderLineStyle.THIN);
    	 
    	 WritableCellFormat bandEnvFormat = new WritableCellFormat(wfont);
    	 bandEnvFormat.setAlignment(Alignment.CENTRE);
    	 bandEnvFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
    	 bandEnvFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
    	 bandEnvFormat.setBackground(Colour.GRAY_25);
    	 
    	 WritableCellFormat cellForamt = new WritableCellFormat(wfont);
    	 cellForamt.setAlignment(Alignment.CENTRE);
    	 cellForamt.setBackground(Colour.GRAY_25);
    	 cellForamt.setBorder(Border.ALL, BorderLineStyle.THIN);
    	 cellForamt.setVerticalAlignment(VerticalAlignment.CENTRE);
    	  
    	 
    	 excelSheet.setColumnView(0, 20);
    	 excelSheet.setColumnView(1, 20);
    	 excelSheet.setColumnView(2, 70);
         excelSheet.setColumnView(3, 70);
    	 excelSheet.setColumnView(4, 70);
    	 excelSheet.setColumnView(5, 70);
    	
    	 
    	 Label envLabel = new Label(0, 0, ENVIRONMENT, cellForamt);
    	 Label brandLabel = new Label(1, 0, BRAND,cellForamt);
    	 Label secnariolabel = new Label(2, 0, SCENARIONAME,cellForamt);
    	 Label failLabel = new Label(3, 0, FAILURE,cellForamt);
    	 Label errLabel = new Label(4, 0, ERROR,cellForamt);
    	 Label urlLabel = new Label(5, 0, URL,cellForamt);
    	 
    	 excelSheet.addCell(envLabel);
    	 excelSheet.addCell(brandLabel);
    	 excelSheet.addCell(secnariolabel);
    	 excelSheet.addCell(failLabel);
    	 excelSheet.addCell(errLabel);
    	 excelSheet.addCell(urlLabel);
    	 
    	 int i=1,envCount=1,brandCount=0;     
    	
    	for(Map.Entry<String, Map<String, Map<String, Map<String, List<List<String>>>>>> envMapVal:envMap.entrySet())
     {
    		
    	if(envCount<=envMap.size() && i!=1){
          		 i = i+1;
          		 brandCount=0 ;
         }
    	 String envName = envMapVal.getKey();
    	 Label environment = new Label(0, i, envName, bandEnvFormat);
    	 excelSheet.addCell(environment);
    	 envCount++;
    	 
    	 Map<String, Map<String, Map<String, List<List<String>>>>> brandMap = envMapVal.getValue();
    	 for(Map.Entry<String, Map<String, Map<String, List<List<String>>>>> mpVal:brandMap.entrySet()){
    		 if(brandCount!=0){
           		 i = i+1;
           	 }
    		 String brandName = mpVal.getKey();
    		 Label brand = new Label(1, i, brandName, bandEnvFormat);
        	 excelSheet.addCell(brand);
        	 brandCount++;
        	 
             Map<String, Map<String, List<List<String>>>> scenmap = mpVal.getValue();
   
    	  for(Map.Entry<String, Map<String, List<List<String>>>> scnIterate:scenmap.entrySet())
    	  {
    		   String scenarioName = scnIterate.getKey();
    		   Label scenario = new Label(2, i, scenarioName, cellTextFormat);
          	   excelSheet.addCell(scenario);       	   
          	   Map<String, List<List<String>>> urlMap =  scnIterate.getValue();
          	   
          	  for(Map.Entry<String, List<List<String>>> urlIterate:urlMap.entrySet()){
          		 
          		 String firstCellContent,secdCellContent,thirdCellContent;
          	     secdCellContent = excelSheet.getCell(1, i).getContents();
          	     firstCellContent = excelSheet.getCell(0, i).getContents();
          	     thirdCellContent = excelSheet.getCell(2, i).getContents();
             	 if(secdCellContent.length()==0){
             		  Label branBorder = new Label(1, i, "", cellBorder);
               	      excelSheet.addCell(branBorder); 
             	 } 
             	 if(firstCellContent.length()==0){
            		  Label branBorder = new Label(0, i, "", cellBorder);
              	      excelSheet.addCell(branBorder); 
            	 } 
             	 if(thirdCellContent.length()==0){
            		  Label branBorder = new Label(2, i, "", cellBorder);
              	      excelSheet.addCell(branBorder); 
            	 } 
          		String urlVAl = urlIterate.getKey();
          		Label url = new Label(5, i, urlVAl, cellTextFormat);
           	    excelSheet.addCell(url);
                
           	    
           	    List<List<String>> ltVal = urlIterate.getValue();
           	    List<String> failList = ltVal.get(0);
           	    List<String> errList = ltVal.get(1);
           	    
           	    String failstring="";int f1=1;
           	    for(int f=0;f<failList.size();f++,f1++){
           	    	failstring=failstring+f1+"."+failList.get(f)+"\n";
           	    }
           	    Label failVal = new Label(3, i, failstring, cellTextFormat);
        	    excelSheet.addCell(failVal);
        	   
        	    
        	    String errorString="";int e1=1;
           	    for(int e=0;e<errList.size();e++,e1++){
           	    	errorString=errorString+e1+"."+errList.get(e)+"\n";
        	    }
          	  
           	   Label errVal = new Label(4, i, errorString, cellTextFormat);
     	       excelSheet.addCell(errVal);
     	       i= i+1;
          	  }
    	    }
    	 }
    	}
    	workbook.write();
        logger.info("Failure XSL File created successfully");
        CreateXMLSummery.addToZipFile("src\\FailSummeryReport.xml");
    	}
       } catch(IOException i){
    	     logger.error("The IO Exception in creating the xsl file:"+i);
       } catch(RowsExceededException r){
    	     logger.error("The RowsExceededException Exception in creating the xsl file:"+r);
       } catch(WriteException w){
    	     logger.error("The WriteException Exception in creating the xsl file:"+w);
       }
    	 finally{
    	   if(workbook!=null){
    		   try{
    		      workbook.close();
    		   }catch(Exception e){
    			   logger.error("Th execption in cretaing the xsl file:"+e);
    		   }
    	   }
    	   
       }
	}

	// return summeryMap;

}
