package com.utilities;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jsoup.Jsoup;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class JUnitReport {

	public static final String SUMMERY_REPORT_PATH = "test-output\\junitreports\\JunitSample.xml";
	
	/**
	 * The method to create the junit xml file
	 * @author anant
	 */
	private static void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("testsuites");
			doc.appendChild(root);
			
			Attr attr = doc.createAttribute("name");
			attr.setValue("Automation Execution");
			root.setAttributeNode(attr);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			File file = new File(SUMMERY_REPORT_PATH);
			if(file.exists()){
				file.delete();
				//logger.info("The existing file deleted by this name:"+SUMMERY_REPORT_PATH);
			}
			
			StreamResult result = new StreamResult(new File(SUMMERY_REPORT_PATH));
			trans.transform(src, result);
			//logger.info("New JUNIT XML File created :" + SUMMERY_REPORT_PATH + " !");
				
		} catch (ParserConfigurationException e1) {
			
			//logger.error("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			//logger.error("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			//logger.error("TransfomerException:" + e3);
		}
	}

	
	/**
	 * <p>
	 * This method add the report data in xml file data.
	 * </p>
	 */
	private static void addSummary(Map<String,List<String>> passFailMap) {
		try {
            Integer failed=0,mapSize;
            mapSize=passFailMap.size();
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(SUMMERY_REPORT_PATH);

			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element testsuite = doc1.createElement("testsuite");
				root1.appendChild(testsuite);
				
				Attr attr = doc1.createAttribute("name");
				attr.setValue("Smoke Test Automation Execution");
				testsuite.setAttributeNode(attr);
				
				Attr tests = doc1.createAttribute("tests");
				tests.setValue(mapSize.toString());
				testsuite.setAttributeNode(tests);
				
				Attr failure = doc1.createAttribute("failures");
				failure.setValue(failed.toString());
				testsuite.setAttributeNode(failure);
				
				Iterator<String> iterator =  passFailMap.keySet().iterator();
               while(iterator.hasNext()){
            	    String testCaseName =  iterator.next();
            	    List<String> listValue = passFailMap.get(testCaseName);
            	  
            	    Element testcase = doc1.createElement("testcase");
            	    testsuite.appendChild(testcase);
    				
    				Attr classname = doc1.createAttribute("classname");
    				classname.setValue(testCaseName);
    				testcase.setAttributeNode(classname);
    				
    				Attr name = doc1.createAttribute("name");
    				name.setValue(testCaseName);
    				testcase.setAttributeNode(name);
    				
    				Attr time = doc1.createAttribute("time");
    				time.setValue("0.0");
    				testcase.setAttributeNode(time);
            	  
				  if (listValue.get(0).equalsIgnoreCase("fail")) {
					 failed++;
      				Element fail = doc1.createElement("failure");
      				testcase.appendChild(fail);
      				
      				Attr message = doc1.createAttribute("message");
      				message.setValue("test failure");
      				fail.setAttributeNode(message);
      				fail.setTextContent(listValue.get(1));
            		  
            	  }
               }
                failure.setValue(failed.toString());
				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(SUMMERY_REPORT_PATH));
				trans.transform(src, result);

				//logger.info("Junit file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			//logger.error("The Sax error:" + e1);
		} catch (IOException e2) {
			//logger.error("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			//logger.error("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			//logger.error("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e5) {
			//logger.error("The TransformerException  error while adding the summary:" + e5);
		}
	}
	
/**
 * This reads the summary html report
 * Stores the value in map
 * @author ananth	
 */
public static void generateJunitXml() {
		//JUnitReport report = new JUnitReport();
		Map<String,List<String>> passFailmap = new LinkedHashMap<String,List<String>>();
		List<String> pasErrList;String testCaseName;
		String path = System.getProperty("user.dir")+File.separator+"reports"+File.separator;
		File rptFlderPath = new File(path);
	if(rptFlderPath.exists()){
		    String[] fileArray = rptFlderPath.list();
	  for(String htmlFlName: fileArray){
		  System.out.println(htmlFlName);
		    File htmlFile = new File(rptFlderPath+File.separator+htmlFlName);
		    if(htmlFlName.contains("AutomationDetailedSummaryReport.html")==true){
		try {
		  org.jsoup.nodes.Document doc = Jsoup.parse(htmlFile,"utf-8");
		//  org.jsoup.select.Elements pass = doc.select("td:matchesOwn(title=\"PASS\"\\s*class=\"(.+?)\")");
		  org.jsoup.select.Elements pass = doc.select("td[class=status pass]");
		  System.out.println(pass.size());

		  for(int e=0;e<pass.size();e++){
			  pasErrList = new  ArrayList<String>();
			  pasErrList.add("PASS");
			  pasErrList.add("");
			  org.jsoup.nodes.Element ele = (org.jsoup.nodes.Element)pass.get(e).nextSibling();
			  testCaseName = ele.text();
			  passFailmap.put(testCaseName, pasErrList);
		  }
	
		  org.jsoup.select.Elements fail = doc.select("td[class=status fail]");
		  System.out.println(fail.size());
		  for (int e = 0; e < fail.size(); e++) {
			  StringBuffer buf = new StringBuffer();int size;
			  pasErrList = new  ArrayList<String>();
			  org.jsoup.nodes.Element ele = (org.jsoup.nodes.Element)fail.get(e).nextSibling();
			  testCaseName = ele.text();
			  org.jsoup.nodes.Element td = fail.get(e).siblingElements().get(3);
			  org.jsoup.select.Elements errMsg = td.getAllElements().select("td[style^=text-align]");
			  size = errMsg.size();
			  if (size > 4) {
				  size = 4;
			  }
			  
			  for(int i=0;i<size;i++){
				 buf.append(errMsg.get(i).text());
				 buf.append("|");
			  }
			  pasErrList.add("FAIL");
		      pasErrList.add(buf.toString());
		      passFailmap.put(testCaseName, pasErrList);  
		   } 
		} catch (FileNotFoundException e) {
		//	logger.error("FileNotFoundException exception:"+htmlFile);
		}catch (IOException e) {
			//logger.error("FileNotFoundException exception:"+htmlFile);
		 }
	    }
	   }
	    createXmlFile();
		addSummary(passFailmap);
	  }else{
		// logger.error("The path could't find in framework:"+path);
	  }
	}
}
