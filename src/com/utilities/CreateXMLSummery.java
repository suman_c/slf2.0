package com.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.apache.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/*
</p>
* @author Harivardhan.a
* @company Sonata Software
* @createdDate 29/06/2017
* @history Initial Version. 
* </p>  
*/
public class CreateXMLSummery {

	/**
	 * <p>
	 * This method create the temporary xml file for adding the summery report
	 * data.
	 * </p>
	 */
    // variable to hold the summery report path
	private static final String SUMMERY_REPORT_PATH = "src\\SummeryReport.xml";
	private static final String SUMMERY_REPORT_PATH_FAIL = "src\\FailSummeryReport.xml";
	
	// Logger variable
	private static Logger logger = Logger.getLogger(CreateXMLSummery.class);

	public static void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("SummaryResult");
			doc.appendChild(root);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			File file = new File(SUMMERY_REPORT_PATH);
			if (!file.exists()) {
				StreamResult result = new StreamResult(new File(SUMMERY_REPORT_PATH));
				trans.transform(src, result);
				logger.info("New XML File created :" + SUMMERY_REPORT_PATH + " !");
				
			} else {
				logger.info("XML File already exist :" + SUMMERY_REPORT_PATH + " !");
				
			}

		} catch (ParserConfigurationException e1) {
			
			logger.error("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			logger.error("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			logger.error("TransfomerException:" + e3);
		}
	}

	

	/**
	 * Method to create xml file for failure
	 */
	public static void createXmlForFail() {
		     DocumentBuilderFactory factory;
		try {
			factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element rootElement = doc.createElement("SummaryResult");
			doc.appendChild(rootElement);

			TransformerFactory transFactory = TransformerFactory.newInstance();
			Transformer transfer = transFactory.newTransformer();
			DOMSource dom = new DOMSource(doc);
			if (SUMMERY_REPORT_PATH_FAIL.contains(".xml")) {
				File file = new File(SUMMERY_REPORT_PATH_FAIL);
				if (!file.exists()) {
					StreamResult result = new StreamResult(file);
					transfer.transform(dom, result);
					logger.info("New XML File created :"
							+ SUMMERY_REPORT_PATH_FAIL + " !");

				} else {
					logger.info("XML File already exist :"
							+ SUMMERY_REPORT_PATH_FAIL + " !");

				}
			} else {
				logger.error("The file doesnt have extension the xml please check it");
			}

		} catch (ParserConfigurationException e) {
			logger.error("The Pasre configuration exception in creating the xml file:"
					+ e);
		} catch (TransformerConfigurationException e) {
			logger.error("The transformer configuration exception in creating the xml file:"
					+ e);
		} catch (TransformerException e) {
			logger.error("Transformer Exception:" + e);
		}
	}

	
	/**
	 * <p>
	 * This method add the report data in xml file data.
	 * </p>
	 */
	public static void addSummary(String env, String browser, String brand, float Total, float Passed, float Failed,
			long executionTime) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(SUMMERY_REPORT_PATH);

			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element status = doc1.createElement("ENV");
				root1.appendChild(status);
				Attr attr = doc1.createAttribute("id");
				attr.setValue(env);
				status.setAttributeNode(attr);

				Element elementBrand = doc1.createElement("BRAND");
				elementBrand.appendChild(doc1.createTextNode(String.valueOf(brand)));
				status.appendChild(elementBrand);

				Element elementBrowser = doc1.createElement("BROWSER");
				elementBrowser.appendChild(doc1.createTextNode(String.valueOf(browser)));
				status.appendChild(elementBrowser);

				Element elementPassed = doc1.createElement("PASSED");
				elementPassed.appendChild(doc1.createTextNode(String.valueOf(Passed)));
				status.appendChild(elementPassed);

				Element elementFailed = doc1.createElement("FAILED");
				elementFailed.appendChild(doc1.createTextNode(String.valueOf(Failed)));
				status.appendChild(elementFailed);

				Element elementTotal = doc1.createElement("TOTAL");
				elementTotal.appendChild(doc1.createTextNode(String.valueOf(Total)));
				status.appendChild(elementTotal);

				Element elementTime = doc1.createElement("EXECUTIONTIME");
				elementTime.appendChild(doc1.createTextNode(String.valueOf(executionTime)));
				status.appendChild(elementTime);

				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(SUMMERY_REPORT_PATH));
				trans.transform(src, result);

				logger.info("file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			logger.error("The Sax error:" + e1);
		} catch (IOException e2) {
			logger.error("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			logger.error("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			logger.error("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e5) {
			logger.error("The TransformerException  error while adding the summary:" + e5);
		}
	}

	/**
	 * The method to add failure data to xml file
	 * @param env
	 * @param branName
	 * @param mpVal
	 */
	public static void addXmlSummaryForFail(String env,String branName,Map<String,Map<String,List<List<String>>>> mpVal)
	{
		    DocumentBuilderFactory factory ;
		    File xmlFile = new File(SUMMERY_REPORT_PATH_FAIL);
		try {
		  if(xmlFile.exists()){
				factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(xmlFile);

				Element rootNode = doc.getDocumentElement();
				logger.info("The root node of xml file:" + rootNode);

				Element envNode = doc.createElement("ENV");
				Attr attr = doc.createAttribute("id");
				attr.setValue(env);
				envNode.setAttributeNode(attr);
				rootNode.appendChild(envNode);

				Element brand = doc.createElement("BRAND");
				brand.appendChild(doc.createTextNode(branName.toString()));
				envNode.appendChild(brand);

				Element scenarios = doc.createElement("SCENARIOS");
				envNode.appendChild(scenarios);
            
          
            for(Map.Entry<String, Map<String,List<List<String>>>> scenarName:mpVal.entrySet())
            {

					Element scenDesc = doc.createElement("SCENARIODESC");
					scenarios.appendChild(scenDesc);

					String scenario = scenarName.getKey();
					Element scenarioName = doc.createElement("SCENARIONAME");
					scenarioName.appendChild(doc.createTextNode(scenario
							.toString()));
					scenDesc.appendChild(scenarioName);

					Map<String, List<List<String>>> urlMap = scenarName
							.getValue();
					for (Map.Entry<String, List<List<String>>> urlVal : urlMap
							.entrySet()) {
						String urlAppln = urlVal.getKey();

						Element urlNode = doc.createElement("URLVAL");
						scenDesc.appendChild(urlNode);

						Element url = doc.createElement("URL");
						url.appendChild(doc.createTextNode(urlAppln));
						urlNode.appendChild(url);

						List<List<String>> pasFailLt = urlVal.getValue();
						List<String> failList = pasFailLt.get(0);
						List<String> errList = pasFailLt.get(1);

						for (int f = 0; f < failList.size(); f++) {
							Element fail = doc.createElement("FAIL");
							fail.appendChild(doc.createTextNode(failList.get(f)));
							urlNode.appendChild(fail);
						}

						for (int f = 0; f < errList.size(); f++) {
							Element error = doc.createElement("ERROR");
							error.appendChild(doc.createTextNode(errList.get(f)));
							urlNode.appendChild(error);
						}
					}
            }  
            
            TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(SUMMERY_REPORT_PATH_FAIL));
		    trans.transform(src, result);
		    logger.info("file updated and added summary successfully for failure");
		   }else{
			   logger.info("The xml file doesn't exist while adding summary to it:");
		   }
		} catch (ParserConfigurationException e) {
			logger.error("The Pasre configuration exception in creating the xml file:"+e);
		} catch (SAXException e) {
			logger.error("The SAX Exception in creating the xml file:"+e);
		} catch (IOException e) {
			logger.error("The IO Exception in creating the xml file:"+e);
		} catch (TransformerConfigurationException e) {
			logger.error("Thranformer configuration exception:"+e);
		}  catch (TransformerException e) {
			logger.error("Transformer exception:"+e);
		}
		
	}
	
	/**
	 * <p>
	 * This method create the xml back up file
	 * </p>
	 */
	
	public static void addToZipFile(String xmlFilePath) throws FileNotFoundException, IOException {

        try {
            Calendar now = Calendar.getInstance();
            int year = now.get(Calendar.YEAR);
            int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
            int day = now.get(Calendar.DAY_OF_MONTH);
            int hour = now.get(Calendar.HOUR_OF_DAY);
            int minute = now.get(Calendar.MINUTE);
            int second = now.get(Calendar.SECOND);
            int millis = now.get(Calendar.MILLISECOND);

            String fileName = String.valueOf(day) + "-" + String.valueOf(month) + "-" + String.valueOf(year) + "_"
                    + String.valueOf(hour) + "_" + String.valueOf(minute) + "_" + String.valueOf(second);
            //String zipFile = "D:\\" + fileName + ".zip";
            String zipFile = System.getProperty("user.dir").substring(0,2)+File.separator + fileName + ".zip";
        
            // create byte buffer
            byte[] buffer = new byte[1024 * 4];

            // create object of FileOutputStream
            FileOutputStream fout = new FileOutputStream(zipFile);

            // create object of ZipOutputStream from FileOutputStream
            ZipOutputStream zout = new ZipOutputStream(fout);


            // create object of FileInputStream for source file
            //FileInputStream fin = new FileInputStream(new File(SUMMERY_REPORT_PATH));
            //zout.putNextEntry(new ZipEntry("SummeryReport.xml"));
            File xmlPath = new File(xmlFilePath);
            FileInputStream fin = new FileInputStream(xmlPath);
            zout.putNextEntry(new ZipEntry(xmlPath.getName()));

            int length;

            while ((length = fin.read(buffer)) > 0) {
                zout.write(buffer, 0, length);
            }

            zout.flush();
            zout.closeEntry();

            // close the InputStream
            fin.close();

            // close the ZipOutputStream
            zout.close();

            logger.info("Zip file has been created!");
            //File f = new File(SUMMERY_REPORT_PATH);
            if (xmlPath.exists()) {
            	xmlPath.delete();
                logger.info("SummeryReport copied to back up folder :");
            }

        } catch (IOException ioe) {
            logger.error("IOException :" + ioe);
        }
    }

}
