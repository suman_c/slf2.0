package com.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class WebBAUGridGenerator {

	public String filename, htmlContent;

	/**
	 * The method is used to extract the file name
	 * 
	 * @return string file name
	 * @author Anant Patil
	 */
	public String getFileName() {
		return filename;
	}

	/**
	 * Method is used to set the file name return void
	 * 
	 * @author Anant Patil
	 */
	public void setFileName(String filename) {
		this.filename = filename;
	}

	/**
	 * The following method is used to create the xml file It returns the void
	 * 
	 * @author Anant Patil
	 * @param filename
	 */
	public void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("MasterSummaryResult");
			doc.appendChild(root);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(getFileName()));
			trans.transform(src, result);
			//System.out.println("File created!");
		} catch (ParserConfigurationException e1) {
			//System.out.println("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			//System.out.println("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			//System.out.println("TransfomerException:" + e3);
		}
	}

	public void addSummary(String Envt, String TH, String FC, String FL, String CR, String FO, String FLFO,
			String color) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(getFileName());
			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element status = doc1.createElement("Status");
				root1.appendChild(status);

				Element tot = doc1.createElement("Envt");
				tot.appendChild(doc1.createTextNode(String.valueOf(Envt)));
				status.appendChild(tot);

				Element pass = doc1.createElement("TH");
				pass.appendChild(doc1.createTextNode(String.valueOf(TH)));
				status.appendChild(pass);

				Element fC = doc1.createElement("FC");
				pass.appendChild(doc1.createTextNode(String.valueOf(FC)));
				status.appendChild(fC);

				Element fL = doc1.createElement("FL");
				pass.appendChild(doc1.createTextNode(String.valueOf(FL)));
				status.appendChild(fL);

				Element cR = doc1.createElement("CR");
				pass.appendChild(doc1.createTextNode(String.valueOf(CR)));
				status.appendChild(cR);

				Element fO = doc1.createElement("FO");
				pass.appendChild(doc1.createTextNode(String.valueOf(FO)));
				status.appendChild(fO);

				Element fLFo = doc1.createElement("FLFO");
				pass.appendChild(doc1.createTextNode(String.valueOf(FLFO)));
				status.appendChild(fLFo);

				Element col = doc1.createElement("color");
				pass.appendChild(doc1.createTextNode(String.valueOf(color)));
				status.appendChild(col);

				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(getFileName()));
				trans.transform(src, result);
				//System.out.println("file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			//System.out.println("The Sax error:" + e1);
		} catch (IOException e2) {
			//System.out.println("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			//System.out.println("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			//System.out.println("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public static void transormXmlToHtml(String path, String rsltHtmlFile, String xslFile) {
		try {
			//System.out.println("the file:" + xslFile);
			File dltHtmlFile = new File(rsltHtmlFile + ".html");
			if (dltHtmlFile.exists()) {
				if (dltHtmlFile.getName().contains(".html")) {
					dltHtmlFile.delete();
				}
			}
			TransformerFactory tfactory = TransformerFactory.newInstance();
			StreamSource xmlfile = new StreamSource(path + ".xml");
			StreamSource xslfile = new StreamSource(xslFile);
			FileOutputStream htmlfile = new FileOutputStream(rsltHtmlFile + ".html");
			Transformer trans = tfactory.newTransformer(xslfile);
			trans.transform(xmlfile, new StreamResult(htmlfile));

		} catch (TransformerConfigurationException e1) {
			//System.out.println("The TransformerConfigurationException:" + e1);
		} catch (FileNotFoundException e2) {
			//System.out.println("The FileNotFoundException:" + e2);
		} catch (TransformerException e3) {
			//System.out.println("The TransformerException:" + e3);
		}
	}

	/**
	 * @author anant Patil
	 * @param args
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public static TreeMap<String, String> reportForamtion() throws ParseException, IOException {
		// ============================================================================
		String rptFlderPath, finalResultFldPath, prjPath;
		String line, scenarioName = null, line1 = "", testCaseName = null, flagStatus, iteratorVal, brandName = null,
				envtName = null, envBranPass = null, runEnvt = null, buildName, buildTH = "false", envName = null;

		String htmlFName, fStatus = "false";

		TreeSet<String> brandPassPer = new TreeSet<String>();

		TreeMap<String, TreeSet<String>> eMap = new TreeMap<String, TreeSet<String>>();
		TreeMap<String, String> envBrandMap = new TreeMap<String, String>();
		TreeSet<String> brandSet = new TreeSet<String>();
		TreeMap<String, String> allMap = new TreeMap<String, String>();

		Map<String, Map<String, String>> tableStructure = new TreeMap<>();
		Map<String, Object> requestBody = new HashMap<String, Object>();
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();

		rptFlderPath = prjPath + "\\Tomcat\\webapps\\Reports\\execution_reports\\";
	

		finalResultFldPath = prjPath + "//reports//";

		WebBAUGridGenerator report = new WebBAUGridGenerator();
		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();

		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();

// Kirti's code for new grid master summary report starts here

		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);
				}
			}
		}

		if (datFolder.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);
			dateValue = filPath.getAbsolutePath();
			try {
				strdDate = fmt.parse(filPath.getName().trim());
				diff = sysDate.getTime() - strdDate.getTime();
				diffDays = diff / (24 * 60 * 60 * 1000);
				small = diffDays;
			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();
				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);
					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);
			long lastMod = Long.MIN_VALUE;
			File choice = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();
				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
							//System.out.println(choice + "====sadh\n\n");

						}
					}
				}
			}

			String[] htmlFlName = choice.list();
			List<String[]> finalList = new ArrayList<String[]>();

			for (String htFlName : htmlFlName) {

				if (!htFlName.contains("images") && !htFlName.contains("AutomationSummary")) {

					File htmlFileName = new File(choice.toString() + File.separator + htFlName);

// Code for checking fail or pass status of the report

					Scanner scanner = new Scanner(htmlFileName);

					while (scanner.hasNextLine()) {
						final String lineFromFile = scanner.nextLine();
						if (lineFromFile.contains("FAIL") || lineFromFile.contains("ERROR")) {
							fStatus = "true";
							break;
						}

					}
// end of code				 				 
					BufferedReader reader;

					try {
						reader = new BufferedReader(new FileReader(htmlFileName));
						scenarioName = htmlFileName.getName().replace(".html", "");

						// code for getting the file name

						htmlFName = htmlFileName.toString();
						htmlFName = htmlFName.replace(choice.toString(), "").replace("\\", "");

						if (htmlFileName.length() != 0) {

							while ((line = reader.readLine()) != null) {

								line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "")
										.trim();
								line1 = line1.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "")
										.trim();

//Code capturing envt & brand details

								if (line.contains("Launching URL:")) {
									envName = line.replace("http:", "").replace("https:", "")
											.replace("Launching URL:", "").replace("Page Url:", "").trim();
									String comUrl = envName.replace("//", "");

									String[] allenv = envName.split("\\.");
									brandName = allenv[1];
									envtName = allenv[0].replace("/", "");
									envtName = envtName.toLowerCase();

									if (envtName.contains("-")) {
										String[] envArray = envtName.split("-");
										envtName = envArray[1];
									} else if ((envtName.equals("www"))
											&& (brandName.toLowerCase().contains("prjuat"))) {
										envtName = "pat";
									} else {
										envtName = envtName;
									}

									if (comUrl.contains("holidayhypermarket")) {
										brandName = "HH";
										envBranPass = brandName;

									} else if (comUrl.contains("cruisedeals")) {
										brandName = "CD";
										envBranPass = brandName;

									} else

									if (comUrl.contains("cruise")) {
										brandName = "CR";
										envBranPass = brandName;

									} else if ((comUrl.contains("tuiretailhish") || comUrl.contains("tuiprjuat"))
											&& comUrl.contains("/retail/headoffice/login")) {
										// tuiretailhish
										brandName = "TH-HO";
										envBranPass = brandName;
									} else if ((comUrl.contains("tuiretailhish") || comUrl.contains("tuiprjuat"))
											&& comUrl.contains("/retail/stockton/login")) {
										brandName = "TH-STK";
										envBranPass = brandName;
									} else if ((comUrl.contains("tuiretailhish") || comUrl.contains("tuiprjuat"))
											&& comUrl.contains("/retail/thirdparty/login")) {
										brandName = "TH-3RD";
										envBranPass = brandName;
									}
									//
									else if ((comUrl.contains("tuiretailhish") || comUrl.contains("tuiprjuat"))
											&& comUrl.contains("/retail/se/thirdparty/login")) {
										brandName = "SE-3RD";
										envBranPass = brandName;
									} else if ((comUrl.contains("tuiretailhish") || comUrl.contains("tuiprjuat"))
											&& comUrl.contains("/retail/no/thirdparty/login")) {
										brandName = "NO-3RD";
										envBranPass = brandName;
									}
									//
									else if ((comUrl.contains("falconretailhish")
											|| comUrl.contains("tuiholidaysprjuat"))
											&& comUrl.contains("/retail/f/headoffice/login")) {
										brandName = "IE-HO";
										envBranPass = brandName;
									} else if ((comUrl.contains("falconretailhish")
											|| comUrl.contains("tuiholidaysprjuat"))
											&& comUrl.contains("/retail/f/stockton/login")) {
										brandName = "IE-STK";
										envBranPass = brandName;
									} else if ((comUrl.contains("falconretailhish")
											|| comUrl.contains("tuiholidaysprjuat"))
											&& comUrl.contains("/retail/f/thirdparty/login")) {
										brandName = "IE-3RD";
										envBranPass = brandName;
									} else if ((comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")
											|| comUrl.contains("tui")) && !comUrl.contains("tuiholidays")
											&& (comUrl.contains("flight"))) {
										brandName = "THFO";
										envBranPass = brandName;

									} else if ((comUrl.contains("tuiholidaysprjuat") || comUrl.contains("tuiholidays"))
											&& (comUrl.contains("flight"))) {
										brandName = "IEFO";
										envBranPass = brandName;

									} else if ((comUrl.contains("firstchoiceprjuat") || comUrl.contains("firstchoice"))
											&& comUrl.contains("multi-centre")) {
										brandName = "FCMC";
										envBranPass = brandName;
									} else if (comUrl.contains("firstchoice")
											&& comUrl.contains("your-account/login")) {
										brandName = "FCCA";
										envBranPass = brandName;

									} else if (comUrl.contains("firstchoice")) {
										brandName = "FC";
										envBranPass = brandName;

									} else if (comUrl.contains("tuiholidaysprjuat.ie/f/your-account/login")) {
										brandName = "IECA";
										envBranPass = brandName;
									} else if ((comUrl.contains("tuiprjuat") || comUrl.contains("tui"))
											&& comUrl.contains("your-account/login")
											|| comUrl.contains("your-account/register")) {
										brandName = "CA";
										envBranPass = brandName;
									} else if (comUrl.contains("tuiholidaysprjuat")
											|| comUrl.contains("falconholidaysprjuat")
											|| comUrl.contains("tuiholidays")) {
										brandName = "IE";
										envBranPass = brandName;

									}

									else

										if (comUrl.contains(".tuiholidaysprjuat.ie/f/lakes-and-mountains")) {
											brandName = "IELM";
											envBranPass = brandName;
											System.out.println("ireland lake and mountais");
										}
									
									else if ((comUrl.contains("tuiprjuat") || comUrl.contains("tui"))
											&& comUrl.contains("lakes-and-mountains")) {
										brandName = "THLM";
										envBranPass = brandName;
										System.out.println("uk lake and mountais");
									} 
								
									
									
									
									else if ((comUrl.contains("tuiprjuat") || comUrl.contains("tui"))
											&& comUrl.contains("multi-centre")) {
										brandName = "THMC";
										envBranPass = brandName;
									}

									/*
									 * else if(comUrl.contains("www.tui.co.uk") &&
									 * (comUrl.contains("destinations"))){ brandName = "TH"; buildTH = "true";
									 * envtName = "PROD"; envBranPass = brandName; }
									 */
									else if ((comUrl.contains("tuiprjuat") || comUrl.contains("tui"))
											&& (comUrl.contains("destinations"))) {
										brandName = "TH";
										buildTH = "true";
										envBranPass = brandName;
									}

									// Nordics

									else if (comUrl.contains(".se") && (comUrl.contains("flyg"))) {
										brandName = "SEFO";
										buildTH = "true";
										envBranPass = brandName;
									} else if (comUrl.contains(".dk") && (comUrl.contains("kun-fly"))) {
										brandName = "DKFO";
										buildTH = "true";
										envBranPass = brandName;
									} else if (comUrl.contains(".no") && (comUrl.contains("fly"))) {
										brandName = "NOFO";
										buildTH = "true";
										envBranPass = brandName;
									} else if (comUrl.contains("lento")) {
										brandName = "FIFO";
										buildTH = "true";
										envBranPass = brandName;
									} else if (comUrl.contains(".se")) {
										brandName = "SE";
										buildTH = "true";
										envBranPass = brandName;
									}

									else if (comUrl.contains(".dk")) {
										brandName = "DK";
										buildTH = "true";
										envBranPass = brandName;
									}

									else if (comUrl.contains(".fi")) {
										brandName = "FI";
										buildTH = "true";
										envBranPass = brandName;
									}

									else if (comUrl.contains(".no")) {
										brandName = "NO";
										buildTH = "true";
										envBranPass = brandName;
									}

									else {
										brandName = "Not found";
										envBranPass = brandName;
									}

									// //System.out.println(comUrl+"====jdks\n\n");

									brandSet.add(envBranPass);

									eMap.put(envtName, brandSet);

								}
//end of code

// start of code for build

								if (!buildTH.equals("false") && line.contains("buildversion")) {

									buildName = line.replace("<td class='step-details' colspan='2'>", "");
									buildName = buildName
											.replace("OR_homepage_buildversion text is captured as <strong>", "");

									buildName = buildName.replace("</strong>", "");
									buildName = buildName.replaceAll("\\s+", "");
									buildName = buildName.replace("_", "-");

									//System.out.println(buildName + "sdf==\n\n");

									if (!buildName.contains("**********")) {
										buildName = buildName + "#" + envtName;
										envBrandMap.put(buildName, envtName);
									}

									// envBrandMap.put(buildName, envtName);

								}

//end of code for build	
							} // end of html reader while
						} else {
							//System.out.println("The html file doesn't have the content:");
						}

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}

		} // last if loop of the entire script

// writing all details in hash
		Set set = envBrandMap.entrySet();
		Iterator iterator = set.iterator();
		for (String key : envBrandMap.keySet()) {

			while (iterator.hasNext()) {
				Map.Entry mentry = (Map.Entry) iterator.next();
				for (String key2 : eMap.keySet()) {
					Set set1 = eMap.entrySet();
					Iterator iterator1 = set1.iterator();
					while (iterator1.hasNext()) {
						Map.Entry envtry = (Map.Entry) iterator1.next();
						if (mentry.getValue().equals(envtry.getKey())) {

							// //System.out.println(mentry.getKey() +" "+ envtry.getKey()+
							// envtry.getValue()+"===bsbsb\n\n");

							Map<String, String> brandEnv = new TreeMap<>();
							String envValue = envtry.getKey() + "_" + envtry.getValue();

							allMap.put((String) mentry.getKey(), envValue);
							// brandEnv.put(envtry.getKey(), envtry.getValue());
							// allMap.put((String) mentry.getKey(), (TreeMap<String, String>) brandEnv);

						}
					}
				}
			}

		}

		Set set2 = allMap.entrySet();
		Iterator iterator2 = set2.iterator();
		while (iterator2.hasNext()) {
			Map.Entry mentry2 = (Map.Entry) iterator2.next();
			System.out.print("Build deployed is: " + mentry2.getKey() + " Envt & Brands are ");
			//System.out.println(mentry2.getValue());

		}
// end of code

		return allMap;
	}
}
