package com.utilities;

public class Constants {

	// Config Path Details
	public static final String CONSTANTS_CONFIG_PATH = "\\src\\com\\conf\\config.conf";

	// Object Repository Path Details
	public static final String CONSTANTS_MOBILECHROMEDRIVER_PATH = "\\drivers\\slfdriver\\chromedriver.exe";
	public static String CONSTANTS_OR_PATH;

	// Browser and env details
	public static String CONSTANTS_AAPLNURL;
	public static String CONSTANTS_EXECUTIONCRITERIA;
	public static String CONSTANTS_IPDETAILS;

	// Driver Path Details
	public static String CONSTANTS_CHROME_DRIVER_PATH="Jars\\chromedriver-2.29.exe";
	public static String CONSTANTS_IE_DRIVER_PATH;
	public static String CONSTANTS_PHANTOM_DRIVER_PATH;
	public static String CONSTANTS_GECKO_DRIVER_PATH;

	// Log4J Properties File Path
	public static final String CONSTANTS_LOG_PROPERTIES_PATH = "\\src\\log4j.properties";

	// Test Script Path Details
	public static final String CONSTANTS_SCRIPTS_PATH = "\\conf\\testscript.properties";
	public static final String CONSTANTS_DRIVERS_PATH = "\\conf\\slf.properties";
	public static final String CONSTANTS_BROWSERENV_PATH = "\\conf\\browserenv.properties";

	public static final String CONSTANTS_SETVALUE_STORE_PROPERTY = "\\src\\com\\objectrepository\\IntegratedBookingValues.properties";
	public static final String CONSTANTS_REPORT_LOCATION = "\\Tomcat\\webapps\\Reports\\execution_reports\\";

	// Booking Properties
	public static final String CONSTANTS_BOOKING_REFERENCE_PROPERTY = "\\src\\com\\tui\\booking.properties";
	public static final String CONSTANTS_BOOKING_REFERENCE_STORE_PROPERTY = "\\src\\com\\objectrepository\\IntegratedRefundBookingReferences.properties";
	public static final String CONSTANTS_BOOKING_REFERENCE_STORE_PROPERTY_MMB = "\\src\\com\\utilities\\BookingDetailsForMMB.properties";


	// Driver Property Details
	public static final String CONSTANTS_CHROME_PROPERTY = "webdriver.chrome.driver";
	public static final String CONSTANTS_IE_PROPERTY = "webdriver.ie.driver";
	public static final String CONSTANTS_PHANTOM_PROPERTY = "phantomjs.binary.path";

	// Object Repository Property Details
	public static final String CONSTANTS_OBJECT_NAME = "Object_Name";
	public static final String CONSTANTS_OBJECT_TYPE = "Object_Type";
	public static final String CONSTANTS_OBJECT_LOCATOR = "Object_Locator_Type";
	public static final String CONSTANTS_OBJECT_LOCATOR_VALUE = "Object_Locator_Value";

	public static final String CONSTANTS_OBJECT_TYPE_TEXT = "TextBox";
	public static final String CONSTANTS_OBJECT_TYPE_BUTTON = "Button";
	public static final String CONSTANTS_OBJECT_TYPE_IMAGE = "Image";
	public static final String CONSTANTS_OBJECT_TYPE_DROPDOWN = "DropDown";
	public static final String CONSTANTS_OBJECT_TYPE_SELECT = "Select";
	public static final String CONSTANTS_OBJECT_TYPE_RADIOBUTTON = "RadioButton";
	public static final String CONSTANTS_OBJECT_TYPE_CHECKBOX = "CheckBox";
	public static final String CONSTANTS_OBJECT_TYPE_LINK = "Link";

	public static final String CONSTANTS_OBJECT_LOCATOR_ID = "id";
	public static final String CONSTANTS_OBJECT_LOCATOR_NAME = "name";
	public static final String CONSTANTS_OBJECT_LOCATOR_XPATH = "xpath";
	public static final String CONSTANTS_OBJECT_LOCATOR_CSS = "css";
	public static final String CONSTANTS_OBJECT_LOCATOR_REGEX = "regex";

	// Test Data Details 
	public static final String CONSTANTS_TEST_DATA_URL = "Application URL Under Test";

	// Reports and Screenshot Details for Extent Reporting
	public static final String CONSTANTS_EXTENTREPORT_LOCATION = "\\reports\\";
	public static final String CONSTANT_EXTENTREPORT_IMAGES = "images/";
	public static  String REPORT_NAME = null;

	public static final String CONSTANTS_OBJECT_TYPE_SELECT_VISIBLETEXT = "VisibleText";
	public static final String CONSTANTS_OBJECT_TYPE_SELECT_INDEX = "Index";
	public static final String CONSTANTS_OBJECT_TYPE_SELECT_VALUE = "Value";
	
}//end of class
