package com.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class ObjectRepositoryMerging {

	public static void mergeMethod(String[] filePathToRead, String filePathToWrite) throws IOException {
		String[] line1, line2;
		Map<String, String[]> mp1 = new LinkedHashMap<String, String[]>();
		CSVReader reader1, reader2, reader3;
		int index1 = 0, index2 = 0;
		CSVWriter writer = null;
		File filName1, filName2;

		try {
			if (filePathToRead[0].endsWith(".csv")) { // The condition to
														// evaluate the file
														// name ends with csv or
														// not
				writer = new CSVWriter(new FileWriter(filePathToWrite));
				filName1 = new File(filePathToRead[0]);
				reader1 = new CSVReader(new FileReader(filName1));
				writer.writeNext(reader1.readNext());
				while ((line1 = reader1.readNext()) != null) {
					mp1.put(line1[index1], line1); // First OR file value will
													// get store in Maps as Key
													// is OjbectComponen name
													// and value is entire row
				}

				for (int i = 1; i < filePathToRead.length && filePathToRead.length > 1; i++) // Loop
																								// to
																								// read
																								// the
																								// remaining
																								// OR
																								// file
																								// from
																								// array
				{
					if (filePathToRead[i].endsWith(".csv")) {
						filName2 = new File(filePathToRead[i]);
						reader2 = new CSVReader(new FileReader(filName2));
						reader2.readNext();
						while ((line2 = reader2.readNext()) != null) {
							if (!mp1.keySet().contains(line2[index2])) { // Condition
																			// is
																			// to
																			// check
																			// the
																			// whether
																			// the
																			// key
																			// already
																			// present
																			// in
																			// map
																			// is
																			// same
																			// as
																			// in
																			// next
																			// csv
																			// file
																			// or
																			// not
								mp1.put(line2[index2], line2);
							}
						}
					} else {
						System.out.println("The gieven file doesnt seems csv file");
					}
				}

				System.out.println("The map size afetr concatenating:" + mp1.size());

				if (mp1.size() != 0) {
					Iterator<String[]> it = mp1.values().iterator();
					while (it.hasNext()) {
						writer.writeNext(it.next()); // All the OR file data
														// will be write into
														// separate file
					}
				}
				writer.close();

				reader1 = new CSVReader(new FileReader(filePathToWrite));
				int updtFilSize = reader1.readAll().size();
				System.out.println(updtFilSize + "\t" + mp1.size());
				if (updtFilSize - 1 == mp1.size()) {
					System.out.println("All Object repository names are copied"
							+ " properly to new File \nThe rows from eachfile is " + mp1.size() + "\n"
							+ "The Updated fileSize is " + (updtFilSize - 1));
				} else {
					System.out.println("All Object repository names are not copied"
							+ " properly to new File \nThe rows from eachfile is-->" + mp1.size() + "\n"
							+ "The Updated fileSize is-->" + (updtFilSize - 1));
				}

			} else {
				System.out.println("The parent file doesn't seems csv file,Please pass the csv file in array");
			}
		} catch (FileNotFoundException e) {
			System.out.println("The file not found execption: " + e.getMessage());
		} catch (Exception e) {
			System.out.println("The execption occuerd while fetching the data from csv file: " + e.getMessage());
		} finally {
			if (writer != null) {
				writer.close();
			}
		}

	}

}
