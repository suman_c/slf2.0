package com.utilities;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class WebBAUSummaryGenerator_old {

    public String filename, htmlContent;

    /**
     * The method is used to extract the file name
     *
     * @return string file name
     * @author Anant Patil
     */

    @SuppressWarnings("static-access")
    public static Map<String, Map<String, ArrayList<Scenario>>> reportForamtion() throws ParseException, IOException {
        // ============================================================================
        String rptFlderPath, finalResultFldPath, prjPath;
        String line, scenarioName = null, line1 = "", testCaseName = null, flagStatus, iteratorVal, brandName = null, envtName = null, envBranPass = "FC", runEnvt = null, buildName = null, buildTH = "false", envName = null;
        String scenarioNameReg = null, branScenarioName = null, bookingRefNumber = null;
        String htmlFName = null, fStatus = "false", dateOfExecution;

        boolean failFound = true, passFound = true, noTechDiff = true, testStatus=false;
		String techDifficulties = "false";

        TreeSet<String> brandPassPer = new TreeSet<String>();

        Map<String, String> scenarioMap = new HashMap<>();

        TreeMap<String, TreeSet<String>> eMap = new TreeMap<String, TreeSet<String>>();
        TreeMap<String, String> envBrandMap = new TreeMap<String, String>();
        TreeSet<String> brandSet = new TreeSet<String>();
        TreeMap<String, String> allMap = new TreeMap<String, String>();

        ArrayList<String> failFileList= new ArrayList<String> ();
        Map<String, Map<String, String>> tableStructure = new TreeMap<>();
        Map<String, Object> requestBody = new HashMap<String, Object>();
        boolean isDirectryFlag;
        List<File> datFolder = new ArrayList<File>();

        Map<String, Map<String, ArrayList<Scenario>>> finalMap = new TreeMap<>();
        ArrayList<Scenario> scenarioList = new ArrayList<Scenario>();

        File readPrjPath = new File("");
        //prjPath = readPrjPath.getAbsolutePath();
        prjPath = "C:\\Tomcat\\webapps\\Reports";


        //rptFlderPath = prjPath + "//reports//";
        rptFlderPath = prjPath + "//execution_reports//";

        finalResultFldPath = prjPath + "//execution_reports//";


        WebBAUSummaryGenerator_old report = new WebBAUSummaryGenerator_old();
        SimpleDateFormat fmt = new SimpleDateFormat();
        Date sysDate = new Date();

        File readFlder = new File(rptFlderPath);
        File[] listOfFolder = readFlder.listFiles();

// Kirti's code for new summary report starts here

//code for getting a list of the files in the latest folder
        for (File lstFlder : listOfFolder) {
            isDirectryFlag = lstFlder.isDirectory();
            if (isDirectryFlag && !lstFlder.toString().contains("images")) {
                if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
                    fmt.applyPattern("dd-MM-yyyy");
                    datFolder.add(lstFlder);
                }
            }
        }

        if (datFolder.size() != 0) {
            Date strdDate;
            long small = 0, diff, diffDays;
            File filPath;
            String dateValue, dtVal;

            filPath = datFolder.get(0);
            dateValue = filPath.getAbsolutePath();
            try {
                strdDate = fmt.parse(filPath.getName().trim());
                diff = sysDate.getTime() - strdDate.getTime();
                diffDays = diff / (24 * 60 * 60 * 1000);
                small = diffDays;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (int j = 1; j < datFolder.size(); j++) {
                dtVal = datFolder.get(j).getName();
                try {
                    strdDate = fmt.parse(dtVal);
                    diff = sysDate.getTime() - strdDate.getTime();
                    diffDays = diff / (24 * 60 * 60 * 1000);
                    if (diffDays < small) {
                        small = diffDays;
                        dateValue = datFolder.get(j).getAbsolutePath();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            File amPmFilVal = new File(dateValue);
            long lastMod = Long.MIN_VALUE;
            File choice = null;
            if (amPmFilVal.exists()) {
                File[] subFolder = amPmFilVal.listFiles();
                for (File subFld : subFolder) {
                    if (subFld.isDirectory()) {
                        if (subFld.lastModified() > lastMod) {
                            lastMod = subFld.lastModified();
                            choice = subFld;
                            System.out.println(choice+"====sadh\n\n");

                        }
                    }
                }
            }
//end of code

//Capturing the date of execution

            String folderPath = choice.toString();
            folderPath = folderPath.replace("\\", "#");

            String[] pathArray = folderPath.split("#");
            dateOfExecution = pathArray[pathArray.length - 2];

            //System.out.println(pathArray[pathArray.length-2]);

//end of code		


            String[] htmlFlName = choice.list();
            List<String[]> finalList = new ArrayList<String[]>();
            fStatus = "false";
//code for reading the html reports one by one in a loop
            
            for (String htFlName : htmlFlName) {
            	//	System.out.println(htFlName+" ====folder\n");

                if (!htFlName.contains("images") && !htFlName.contains("AutomationSummary")) {

                    File htmlFileName = new File(choice.toString() + File.separator + htFlName);


   				 Scanner scanner=new Scanner(htmlFileName);
   				 
   				 while(scanner.hasNextLine()){
   					 final String lineFromFile = scanner.nextLine();
   					   if(lineFromFile.contains("title=\'FAIL\'")) { 
   						  
   						   String fileName = htmlFileName.toString();
   						failFileList.add(fileName);
   						 //  System.out.println(fStatus+"=====\n"+htmlFileName+"\n"+lineFromFile);
   						   
   						  break;

   				 }
   				 }
   				 
   				
                    BufferedReader reader;

                    try {
                        reader = new BufferedReader(new FileReader(htmlFileName));
                        scenarioName = htmlFileName.getName().replace(".html", "");

                        // code for getting the file name

                        htmlFName = htmlFileName.toString();
                        htmlFName = htmlFName.replace(choice.toString(), "").replace("\\", "");
                      //  System.out.println(htmlFName+"===adas\n\n");


                        if (htmlFileName.length() != 0) {

                            while ((line = reader.readLine()) != null) {
                            	
                           for(String fName :	failFileList) {
                        	   String onlyFileName = fName.replace(choice.toString(), "").replace("\\", "");
                        	   if(onlyFileName.equals(htmlFName)) {
                        		   fStatus = "true";
                        		   break;
                        	   }
                        	   else {
                        		   fStatus = "false";
                        	   }
                           }

                           //System.out.println(fStatus+htmlFileName+"outside status\n"); 	
                                line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
                                line1 = line1.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();

//Code capturing envt & brand details


                                if (line.contains("Launching URL:")) {
                                    envName = line.replace("http:", "").replace("https:", "").replace("Launching URL:", "").replace("Page Url:", "").trim();
                                    String comUrl = envName.replace("//", "");

                                    String[] allenv = envName.split("\\.");
                                    brandName = allenv[1];
                                    envtName = allenv[0].replace("/", "");
                                    envtName = envtName.toLowerCase();

                                    if (comUrl.contains("cruise")) {
                                        brandName = "CR";
                                        envBranPass = brandName;


                                    } else if (comUrl.contains("tuiretailagents.tuiprjuat.co.uk/retail/headoffice/login")) {
                                        brandName = "TH-HO";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiretailagents.tuiprjuat.co.uk/retail/stockton/login")) {
                                        brandName = "TH-STK";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiretailagents.tuiprjuat.co.uk/retail/thirdparty/login")) {
                                        brandName = "TH-3RD";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("falconretailagents.falconholidaysprjuat.ie/retail/f/headoffice/login")) {
                                        brandName = "IE-HO";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("falconretailagents.falconholidaysprjuat.ie/retail/f/stockton/login")) {
                                        brandName = "IE-STK";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("falconretailagents.falconholidaysprjuat.ie/retail/f/thirdparty/login")) {
                                        brandName = "IE-3RD";
                                        envBranPass = brandName;
                                    } else if ((comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")) && (comUrl.contains("flight"))) {
                                        brandName = "THFO";
                                        envBranPass = brandName;

                                    } else if (comUrl.contains("tuiholidaysprjuat") && (comUrl.contains("flight"))) {
                                        brandName = "IEFO";
                                        envBranPass = brandName;

                                    } else if (comUrl.contains("firstchoiceprjuat") && comUrl.contains("multi-centre")) {
                                        brandName = "FCMC";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("firstchoice") && comUrl.contains("your-account/login")) {
                                        brandName = "FCCA";
                                        envBranPass = brandName;

                                    } else if (comUrl.contains("firstchoice")) {
                                        brandName = "FC";
                                        envBranPass = brandName;

                                    } else if (comUrl.contains("tuiholidaysprjuat.ie/f/your-account/login")) {
                                        brandName = "IECA";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("your-account/login")) {
                                        brandName = "THCA";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiholidaysprjuat") || comUrl.contains("falconholidaysprjuat")) {
                                        brandName = "IE";
                                        envBranPass = brandName;

                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("lakes-and-mountains")) {
                                        brandName = "LM";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("multi-centre")) {
                                        brandName = "THMC";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("www.tui.co.uk")) {
                                        brandName = "TH";
                                        buildTH = "true";
                                        envtName = "PROD";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")) {
                                        brandName = "TH";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    } else {
                                        brandName = "Not found";
                                        envBranPass = brandName;
                                    }

                                    //System.out.println(comUrl+"====jdks\n\n");
                                }
//end of code

// code for capturing the scenario names

                                if (line.contains("ScenarioName:TestScenario:")) {


                                    scenarioNameReg = line.replace("ScenarioName:TestScenario:", "");
                                    //end of code
                                    scenarioMap.put(scenarioNameReg, envBranPass);
                                }

                                // code for capturing the booking ref number

                                if (line.contains("ReferenceNumber text")) {
                                    bookingRefNumber = line.replace("TH_RespConfirmation_ReferenceNumber text is captured as <strong>", "");
                                    bookingRefNumber = bookingRefNumber.replace("</strong>", "");
                                    //System.out.println(bookingRefNumber+"===\n\n");
                                }
                                //end of code


// code for tagging pass or fail
                                
                                failFound = true;
                                
                               
                                                                
//                                if(!line.contains("FAIL")) {
//                                	failFound = false;
//                                }
////end of code
//                                if(testStatus==(false)) {
//                                	if (line.contains("We're really sorry, we're having some technical problems")) {
//                                    techDifficulties = true;
//                                }
//                                else {
//                                	techDifficulties = false;
//                                }
//                                }
//end of code

                            } // end of html reader while
                        } else {
                            System.out.println("The html file doesn't have the content:");
                        }
						
						
/*
						Set<String> keyset=scenarioMap.keySet();
						 for(Iterator iterator = keyset.iterator(); iterator.hasNext();)
						{
						String string = (String) iterator.next();
						 System.out.println(string+"==="+scenarioMap.get(string));
						}
*/


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

             
                Scenario scenerio = new Scenario();
                scenerio.setEnv(envtName);
                scenerio.setBrand(envBranPass);
                scenerio.setScenarioNameReg(scenarioNameReg);
                scenerio.setBookingId(bookingRefNumber);
                scenerio.setStatus(fStatus);
                scenerio.setTechnicalDifficultes(techDifficulties);
                scenerio.setDateOfExecution(dateOfExecution);
                scenerio.setfileName(htmlFName);
                scenarioList.add(scenerio);
                failFound = true;


                if(envtName!=null) {
                if (!finalMap.containsKey(envtName)) {
                    finalMap.put(envtName, new TreeMap<String, ArrayList<Scenario>>());
                }

                if (!finalMap.get(envtName).containsKey(envBranPass)) {
                    finalMap.get(envtName).put(envBranPass, new ArrayList<Scenario>());
                }
                finalMap.get(envtName).get(envBranPass).add(scenerio);
                }

            
            }

        } //last if loop of the entire script
        
  /*      
        fStatus = "false";
        //displaying the hash content
        Set set2 = finalMap.entrySet();
        Iterator iterator2 = set2.iterator();
    while(iterator2.hasNext()){
    Map.Entry mentry2 = (Map.Entry)iterator2.next();
    System.out.print("key is: "+mentry2.getKey() + " value is ");
    System.out.println(mentry2.getValue());
    
    System.out.println(scenarioList.size()+"===ada\n\n");
    Iterator iterator;
    iterator = scenarioList.iterator();

    // displaying the Tree set data
    System.out.println("Tree set data in ascending order: ");     
    while (iterator.hasNext()) {
       System.out.println(((Scenario) iterator.next()).getStatus() + " ===\n\n");
       
    }
    
    }*/
        return finalMap;
    }
}
