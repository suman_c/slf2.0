package com.utilities;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.commonfunctions.CommonFunctions;
import com.driver.Driver;
import com.relevantcodes.extentreports.LogStatus;

public class DriverReusable extends CommonFunctions {

	Logger logger = Logger.getLogger(this.getClass().toString());
	static ArrayList<String> at1 = new ArrayList<String>();
	static ArrayList<String> at2 = new ArrayList<String>();
	static ArrayList<String> anal = new ArrayList<String>();
	static Map<String, String> prcStore = new HashMap<String, String>();
	ArrayList<String> at, ay2;
	// List<Integer> stEnd = new ArrayList<>();
	// Map<Integer, List<Integer>> tstCount = new HashMap<>();
	// ReadExcel excelSheet;
	ReadExcel excelSheet1 = new ReadExcel();
	ReadExcel excelObjFrComp = new ReadExcel();
	public static String filePath;
	boolean b = true, pageFuncFlag = false , pageCompFlag = false;
	int row, sr, tRows,sr1,tRows1;
	Class<?> cls;
	Object _instance;

	public static String pound = "\u00a3";
	public static String euro = "\u20ac";
	public static String plusPound = "\u002B\u00a3";
	public static String plusEuro = "\u002B\u20ac";
	public static String space = "\u0020";

	/**
	 * Method to Capture content from the TestscriptSheet and iterate
	 * 
	 * @date Sep 2015
	 * @param String
	 *            path
	 * @author Madan
	 * @return void
	 */

	/*
	 * public void captureContent(String path, String tsSheetName) throws
	 * InvocationTargetException, ClassNotFoundException, IOException { int j =
	 * 0, tot, rowCt; boolean flag = false; String[] brands; excelSheet = new
	 * ReadExcel(); excelSheet.openSheet(path, tsSheetName); rowCt =
	 * excelSheet.getRowCount(); tstCount = testCaseCount(excelSheet);
	 * 
	 * for (int t = 0; t < tstCount.size(); t++) { stEnd = tstCount.get(t); tot
	 * = 1; for (int brand = 0; brand < tot; brand++) { innerloop: for (row =
	 * stEnd.get(0); row < stEnd.get(1); row++) { String run =
	 * excelSheet.getValueFromCell(0, row); if (excelSheet.getValueFromCell(1,
	 * row).contains("TestCase") && run.equalsIgnoreCase("yes")) flag = true; if
	 * (excelSheet.getValueFromCell(1, row).contains("TestCase") &&
	 * run.equalsIgnoreCase("no")) flag = false; if (flag) { List<Object>
	 * myParamList = new ArrayList<Object>(); String methodName =
	 * excelSheet.getValueFromCell(2, row); for (int col = 3; col <
	 * excelSheet.getColumnCount(); col++){ if
	 * (!excelSheet.getValueFromCell(col, row).isEmpty()) {
	 * myParamList.add(excelSheet.getValueFromCell( col, row)); } } Object[]
	 * paramListObject = new String[myParamList.size()]; paramListObject =
	 * myParamList.toArray(paramListObject); if
	 * (!Arrays.toString(paramListObject).contains("^^") &&
	 * excelSheet.getValueFromCell(1, row).contains("NoOfBrands"))
	 * report.log(LogStatus.INFO,"<b><h3>ScenarioName:" +
	 * paramListObject[paramListObject.length - 1] + "_"+
	 * excelSheet.getValueFromCell(1,row - 1).replace("TestCase-",
	 * "")+"</h3></b>");
	 * 
	 * if (Arrays.toString(paramListObject).contains("^^")) { brands =
	 * paramListObject[paramListObject.length - 1].toString().split("\\^\\^");
	 * tot = brands.length; paramListObject[paramListObject.length - 1] =
	 * brands[brand]; if (paramListObject.length == 1&&
	 * !methodName.contains("launchApplication") && !methodName
	 * .equals("ValuesVerifyContent"))
	 * report.log(LogStatus.INFO,"<b><h3>ScenarioName:"+ brands[brand]+ "_"+
	 * excelSheet.getValueFromCell(1, row - 1).replace("TestCase-",
	 * "")+"</h3></b>"); }
	 * 
	 * if (!methodName.equals("")) {
	 * 
	 * System.out.println("the method name:" + methodName + "\t" +
	 * paramListObject.length);
	 * 
	 * boolean isExtras = runReflectionMethod( "com.utilities.DriverReusable",
	 * methodName, paramListObject); if (isExtras == false) break innerloop; } }
	 * } } } }
	 */

	/**
	 * The method use to create the class instance at run time
	 * 
	 * @param strClassName
	 */
	public void createClassInstance(String strClassName) {
		try {
			cls = Class.forName(strClassName);
			try {
				_instance = cls.newInstance();
			} catch (InstantiationException e) {
				logger.error(" Object not Instantiated :" + e.getMessage());
			} catch (IllegalAccessException e) {
				logger.error(" Illegal argument exception :" + e.getMessage());
			}
		} catch (ClassNotFoundException e) {
			logger.error("com.utilities.DriverReusable"
					+ ":-  Class not found%n");
		}
	}

	/**
	 * Method to Capture content from the TestscriptSheet and iterate
	 * 
	 * @date April 2016
	 * @param String
	 * @Autor Anant
	 * @return void
	 */
	public void captureContent(List<Integer> stEnd,String reportName)
			throws InvocationTargetException, ClassNotFoundException, IOException {
		int tot = 1, noOfRepeat = browserProp.size();
		boolean flag = false;
		String[] brands;
		String driverMethodName,  stepDesc;
		createClassInstance("com.utilities.DriverReusable");
	for (int rep = 0; rep < noOfRepeat; rep++) {
			List<String> brwsInfo = browserProp.get(rep);
			browserName = brwsInfo.get(0);
			browserVer = brwsInfo.get(1);
			osOrPlatform = brwsInfo.get(2);
			osVersion = brwsInfo.get(3);
			device = brwsInfo.get(4);
			udidName = brwsInfo.get(5);
			for (int brand = 0; brand < tot; brand++) {
				innerloop: 
					for (row = stEnd.get(0); row < stEnd.get(1); row++) {
						String run = Driver.excelSheet.getValueFromCell(0, row);
						if (Driver.excelSheet.getValueFromCell(2, row).contains("TestScenario")
								&& run.equalsIgnoreCase("yes")) {
							flag = true;						
							if(Driver.crtReport==0 && brand==0){
								String checkurl=	Driver.applicationexecutrurl;
								URL input = null;
								try {
									input = new URL(checkurl);
								} catch (MalformedURLException e) {
									e.printStackTrace();
								}
								UrlDetails = new String(input.getHost()).toUpperCase();
								String Env = null;
								if(UrlDetails.contains("RETAIL")) {
									if(UrlDetails.contains("-")) {
										Env=UrlDetails.split("-")[1].split("\\.")[0].trim();	
									}
									else {
										Env=UrlDetails.split("\\.")[0].trim();	
									}
									
								}
								else {
									if((UrlDetails.contains("WWW"))&&(UrlDetails.contains("PRJUAT"))) {
										Env="PAT";									
									}
									else if((UrlDetails.contains("WWW"))&&(!UrlDetails.contains("PRJUAT"))) {
										Env="LIVE";
									}								
									else {
										Env=UrlDetails.substring(0, UrlDetails.indexOf('.'));
									}
								}
								String scenarionamefromfile=Driver.excelSheet.getValueFromCell(2, row).replace("TestScenario:", "").replace("TestCase-", "").trim();
								setReportingDetails(Env+"_"+scenarionamefromfile+"_"+browserName);
								//		setReportingDetails(Driver.excelSheet.getValueFromCell(2, row)+"_"+browserName+"_"+(Driver.t+1));
							}
							report.log(LogStatus.INFO,
									"ScenarioName:" + Driver.excelSheet.getValueFromCell(2, row).replace("TestCase-", ""));
						}
						if (flag) {
							pageFuncFlag = false; pageCompFlag = false;
							driverMethodName = Driver.excelSheet.getValueFromCell(3, row);
							String step = Driver.excelSheet.getValueFromCell(1, row);
							if (!step.contains("NA")) {
								if (driverMethodName.toLowerCase().contains("increment_tc_count")) {
									stepDesc = Driver.excelSheet.getValueFromCell(2, row);
									testDescription(stepDesc);
								}   
								else {
									if (!driverMethodName.contains("^^") &&  Driver.excelSheet.getValueFromCell(2, row).contains("NoOfBrands"))
										report.log(LogStatus.INFO, "ScenarioName:"
												+ Driver.excelSheet.getValueFromCell(2, row - 1).replace("TestScenario", ""));							
									if (driverMethodName.contains("^^")) {
										brands = driverMethodName.split("\\^\\^");
										tot = brands.length;
										if (!driverMethodName.contains("launchApplication") && !driverMethodName.equals("ValuesVerifyContent"))
											report.log(LogStatus.INFO,
													"ScenarioName:" + brands[brand] + "_" + Driver.excelSheet
													.getValueFromCell(2, row - 1).replace("TestScenario", ""));
									}
									if (!driverMethodName.equals("")) {
										boolean isExtras = executePageFunction(driverMethodName,"PageFunctions");
										if (isExtras == false)
											break innerloop;
									}
								}
							}
						}
					}
			}
	}
	}// the capture content method ends here




	/**
	 * The method will write the test case name to html report
	 * 
	 * @param stepDesc
	 * @return void
	 */
	public void testDescription(String stepDesc) {
		if (stepDesc.length() == 0 && stepDesc.isEmpty()) {
			stepDesc = "Test case desription not specifed in the description column";
		}
		report.log(LogStatus.INFO, stepDesc);
	}

	/**
	 * The method used to execute the test steps
	 * @param excelObj
	 * @param actionMethod
	 * @param rowNo
	 * @return boolean
	 */
	public boolean executSteps(ReadExcel excelObj, String actionMethod,
			int rowNo) {
		boolean isFail = true;
		List<Object> myParamList = new ArrayList<Object>();
		try {
			for (int sc = 5; sc < excelObj.getColumnCount(); sc++) {
				if (!excelObj.getValueFromCell(sc, rowNo).isEmpty())
					myParamList.add(excelObj.getValueFromCell(sc, rowNo));
			}
			Object[] paramListObject = new String[myParamList.size()];
			paramListObject = myParamList.toArray(paramListObject);
			String endStatus = excelObj.getValueFromCell(2, rowNo);
			if (actionMethod.contains("BrowserSetup")
					|| actionMethod.contains("MobileBrowserSetup")) {
				actionMethod = getMethod(onMode);
			}
			if (!actionMethod.equals("") && !endStatus.contains("NA")) {
				isFail = runReflectionMethod("com.utilities.DriverReusable",
						actionMethod, paramListObject);
			}
			myParamList.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isFail;
	}

	/**
	 * The method will execute the steps present in page Function method
	 * 
	 * @param compFuncName
	 * @param shtName
	 * @return boolean
	 */
	public boolean executePageFunction(String driverFuncName, String shtName) {
		String pageFuncName, runStatus, actionMethod, stepDesc;
		String runMode = "yes";
		boolean isFail = true;
		try {
			excelSheet1.openSheet(Driver.scriptPath, shtName);
			tRows = excelSheet1.getRowCount();
			for (sr = 1; sr < tRows && isFail; sr++) {
				pageFuncName = excelSheet1.getValueFromCell(0, sr);
				if (driverFuncName.equals(pageFuncName)) {
					runStatus = excelSheet1.getValueFromCell(1, sr);
					if (runStatus.equalsIgnoreCase(runMode)
							|| (runStatus.contains("start") || runStatus
									.contains("end"))) {
						pageFuncFlag = true;
						actionMethod = excelSheet1.getValueFromCell(4, sr);
						String endStatus = excelSheet1.getValueFromCell(2, sr);
						if(!actionMethod.equals("")&&!endStatus.equals("NA")){
						 if (actionMethod.contains("FUNC")) {
							isFail = executePageComponent(
									excelSheet1.getValueFromCell(5, sr),
									"PageComponents");
							if (isFail == false)
								return isFail;
						 }
						}
						if (actionMethod.toLowerCase().contains(
								"increment_tc_count")) {
							stepDesc = excelSheet1.getValueFromCell(3, sr);
							testDescription(stepDesc);
						}
						if (!actionMethod.contains("FUNC")
								&& !actionMethod.toLowerCase().contains(
										"increment_tc_count")) {
							isFail = executSteps(excelSheet1, actionMethod, sr);
						}
						//String endStatus = excelSheet1.getValueFromCell(2, sr);
						if (endStatus.equals("END")) {
							break;
						}
						runMode = "";
					} else {
						break;
					}
					driverFuncName = "";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isFail;
	}
	
	/**
	 * The method will execute the steps present in page component method
	 * 
	 * @param compFuncName
	 * @param shtName
	 * @return boolean
	 */
	public boolean executePageComponent(String compFuncName, String shtName) {
		String pageFuncName, runStatus, actionMethod, stepDesc;
		String runMode = "yes";
		boolean isFail = true;
		try {
			excelObjFrComp.openSheet(Driver.scriptPath, shtName);
			tRows1 = excelObjFrComp.getRowCount();
			for (sr1 = 1; sr1 < tRows1 && isFail; sr1++) {
				pageFuncName = excelObjFrComp.getValueFromCell(0, sr1);
				if (compFuncName.equals(pageFuncName)) {
					runStatus = excelObjFrComp.getValueFromCell(1, sr1);
					if (runStatus.equalsIgnoreCase(runMode)
							|| (runStatus.contains("start") || runStatus
									.contains("end"))) {
						pageCompFlag = true;
						actionMethod = excelObjFrComp.getValueFromCell(4, sr1);
						if (actionMethod.toLowerCase().contains(
								"increment_tc_count")) {
							stepDesc = excelObjFrComp.getValueFromCell(3, sr1);
							testDescription(stepDesc);
						} else {
							isFail = executSteps(excelObjFrComp, actionMethod,
									sr1);
							String endStatus = excelObjFrComp.getValueFromCell(
									2, sr1);
							if (endStatus.equals("END")) {
								break;
							}
						}
						runMode = "";
					} else {
						break;
					}
					compFuncName = "";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isFail;
	}


	/**+
	 * The method used to get the total no scenario
	 * 
	 * @param rowCt
	 * @return Map
	 * @author Anant
	 */
	public Map<Integer, List<Integer>> testCaseCount(Integer rowCt) {
		int st, end;
		Map<Integer, List<Integer>> mp = new HashMap<>();
		List<Object> lt = new ArrayList<Object>();
		List<Integer> testCt;
		// Integer rowCt = excelSheet.getRowCount();
		for (int r = 1; r < rowCt; r++) {
			if (Driver.excelSheet.getValueFromCell(0, r)
					.equalsIgnoreCase("yes")
					|| Driver.excelSheet.getValueFromCell(0, r)
							.equalsIgnoreCase("no")) {
				lt.add(r + "^^" + Driver.excelSheet.getValueFromCell(0, r));
			}
		}
		if (lt.size() != 0) {
			int ct = 0;
			for (int i = 0; i < lt.size(); i++) {
				testCt = new ArrayList<Integer>();
				String[] spt = lt.get(i).toString().split("\\^\\^");
				st = Integer.parseInt(spt[0]);
				if (i == (lt.size() - 1)) {
					end = rowCt;
				} else {
					String[] ed = lt.get(i + 1).toString().split("\\^\\^");
					end = Integer.parseInt(ed[0]);
				}
				if (lt.get(i).toString().toLowerCase().contains("yes")) {
					testCt.add(st);
					testCt.add(end);
					mp.put(ct, testCt);
					ct++;
				}
			}
		}
		return mp;
	}

	/**
	 * Method to integrate Tescriptsheet and Capturecontent method
	 * 
	 * @date Sept 2015
	 * @param String
	 *            ClassName, String MethodName, String InputArgs
	 * @author Madan Updated By Anant
	 * @return boolean
	 * @throws ParseException 
	 */
	@SuppressWarnings("unchecked")
	public boolean runReflectionMethod(String strClassName,
			String strMethodName, Object... inputArgs)
			throws InvocationTargetException, ParseException {
		Object returntype = null;
		boolean isExtrasPage = true;
		boolean flag = true;
		try {
			if (strMethodName.toLowerCase().contains("verify")
					|| strMethodName.toLowerCase().contains("stored")
					|| strMethodName.toLowerCase().contains("verify1")) {

				Class<?> params[] = new Class[inputArgs.length + 1];
				params[0] = ArrayList.class;
				if (Arrays.toString(inputArgs).contains("ListInputsRequired")) {

					params[1] = ArrayList.class;
					Method myMethod = cls.getMethod(strMethodName, params);
					myMethod.invoke(_instance, at1, at2);
				} else {
					params[1] = String.class;
					Method myMethod = cls.getMethod(strMethodName, params);
					myMethod.invoke(_instance, ay2, inputArgs[0]);
				}

			} else {

				if (!strMethodName.toLowerCase().contains("BrowserSetup")
						|| !strMethodName.toLowerCase().contains(
								"launchApplication")) {
					closeFeedBackPopup();
				}
				Class<?> params[] = new Class[inputArgs.length];
				for (int i = 0; i < inputArgs.length; i++) {
					if (inputArgs[i] instanceof String) {
						params[i] = String.class;
					}
				}

				Method myMethod = cls.getMethod(strMethodName, params);
				returntype = myMethod.invoke(_instance, inputArgs);
				if (returntype instanceof String
						|| returntype instanceof Boolean) {
					if (pageFuncFlag == true) {
						if (returntype.toString().equals("false")
								&&  excelSheet1.getValueFromCell(1, sr)
										.toLowerCase().contains("start")) {
							String status = excelSheet1.getValueFromCell(1,sr).replace("start", "");
							if (!status.equals("") && status.matches("\\d*")) {
								while (sr < 120000) {
									String cc = excelSheet1.getValueFromCell(1, sr);
									String endStatus = excelSheet1.getValueFromCell(2, sr);
									if (cc.toLowerCase().contains("end") && cc.contains(status)) {
										if (endStatus.contains("END")) {
											tRows = 0;
										}
										flag = false;
										break;
									}
									if (endStatus.contains("END")) {
										flag = false;
										tRows = 0;
										break;
									}
									sr++;
								}
							} else {
								logger.info("Please specify the numeric value with start keywork to skip the methods");
								driver.quit();
							}
						}
					} if(pageCompFlag == true){
						if (returntype.toString().equals("false")
								&& excelObjFrComp.getValueFromCell(1, sr1)
										.toLowerCase().contains("start")) {
							String status = excelObjFrComp.getValueFromCell(1, sr1).replace("start", "");
							if (!status.equals("") && status.matches("\\d*")) {
								while (sr1 < 120000) {
									String cc = excelObjFrComp.getValueFromCell(1,sr1);
									String endStatus = excelObjFrComp.getValueFromCell(2, sr1);
									if (cc.toLowerCase().contains("end") && cc.contains(status)) {
										if (endStatus.contains("END")) {
											tRows1 = 0;
										}
										flag = false;
										break;
									}
									if (endStatus.contains("END")) {
										flag = false;
										tRows1 = 0;
										break;
									}
									sr1++;
								}
							} else {
								logger.info("Please specify the numeric value with start keywork to skip the methods");
								driver.quit();
							}
						}
					 }
				}

				if (flag) {
					if (returntype != null) {
						if (returntype instanceof String) {
							ArrayList<String> returntype2 = convertStringToArraylist(returntype
									.toString());
							ay2 = returntype2;
						} else if (returntype instanceof Boolean) {

						} else {
							ay2 = (ArrayList<String>) returntype;
						}
					}
				}
			}

		}
	
		catch (IllegalArgumentException e1) {
			logger.error("Method invoked with wrong number of arguments%n");
		} catch (NoSuchMethodException e1) {
			logger.error("In Class " + strClassName + "::" + strMethodName
					+ ":- method does not exists%n");
		} catch (IllegalAccessException e1) {
			logger.error("Can not access a member of class with modifiers private%n");

		} catch (InvocationTargetException e1) {
			logger.error(e1.getCause());
			if (e1.getCause().toString().toLowerCase()
					.contains("page not display")) {
				isExtrasPage = false;
				report.log(LogStatus.INFO, "Browser has closed successfully");
				createXMLSummery();
				createXmlSummaryForFail();
				driver.quit();
			}
		}
		return isExtrasPage;
	}

	/**
	 * Method to Convert String to ArrayList
	 * 
	 * @date Sept 2015
	 * @param String
	 *            str
	 * @author Madan
	 * @return ArrayList
	 */

	public ArrayList<String> convertStringToArraylist(String str) {
		ArrayList<String> charList = new ArrayList<String>();

		charList.add(str);

		return charList;
	}

	/**
	 * Method to Store one or more values metioned in the Testscript sheet
	 * 
	 * @date March 2016
	 * @param Store
	 * @author Madan
	 * @return boolean
	 */
	public ArrayList<String> ValuesStore(String Store) {
		ArrayList<String> at = new ArrayList<String>();
		String[] bb = Store.split(",");
		for (int j = 0; j < bb.length; j++) {
			try {
				String val = performActionGetText(bb[j]);
				if (val != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at.add(bb[j] + "^^" + val.replaceAll("[^0-9.]", ""));
					} else {
						at.add(bb[j] + "^^" + val.replace("*", ""));
					}
				} else {
					at.add(bb[j] + "^^" + val);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStore:"
						+ a.getMessage());
			} catch (Exception e) {
				logger.error("Exception occured in ValuesStore method:"
						+ e.getMessage());
			}
		}
		return at;
	}

	/**
	 * Method to Store one or more values of html attribue
	 * 
	 * @date March 2016
	 * @param Store
	 * @return boolean
	 */
	public ArrayList<String> ValuesStoreAttribute(String Store) {
		ArrayList<String> at = new ArrayList<String>();
		String[] bb = Store.split(",");
		for (int j = 1; j < bb.length; j++) {
			try {
				String val = performActionGetAttributeValue(bb[j], bb[0]);
				if (val != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at.add(bb[j] + "^^" + val.replaceAll("[^0-9.]", ""));
					} else {
						at.add(bb[j] + "^^" + val);
					}
				} else {
					at.add(bb[j] + "^^" + val);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStoreAttribute:"
						+ a.getMessage());
			} catch (Exception e) {
				logger.error("Exception occured in ValuesStoreAttribute method:"
						+ e.getMessage());
			}
		}
		return at;
	}

	/**
	 * Method to Store one or more values of url content
	 * 
	 * @date March 2016
	 * @param Store
	 * @return boolean
	 */
	public ArrayList<String> ValuesStoreUrlContent(String Store) {
		String val1, val2, url;
		String[] bb = Store.split(",");
		url = driver.getCurrentUrl();
		ArrayList<String> at = new ArrayList<String>();
		try {
			val1 = url.substring(url.indexOf(".") + 1, url.lastIndexOf("."))
					.replace("prjuat", "").replace(".co", "");
			val2 = url
					.substring(url.indexOf("ount") + 5, url.indexOf("/displ"));

			for (int j = 0; j < bb.length; j++) {
				if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
					at.add(bb[j] + "^^" + (val1 + "_" + val2));
				} else {
					at.add(bb[j] + "^^" + (val1 + "_" + val2));
				}
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("Array Index out of bound Exception in ValuesStoreUrlContent:"
					+ a.getMessage());
		} catch (Exception e) {
			logger.error("The execption occured in ValuesStoreUrlContent:"
					+ e.getMessage());
		}
		return at;
	}

	/**
	 * Method to Store one or more values of analytics from source page
	 * 
	 * @date March 2016
	 * @param Store
	 * @return boolean
	 */
	public ArrayList<String> ValuesStoreAnanlytic(String Store)
			throws ParseException {
		String[] bb = Store.split(",");
		SimpleDateFormat fmt = new SimpleDateFormat("MMMyyyy");
		for (int j = 0; j < bb.length; j++) {
			try {
				String retVal = performActionGetText(bb[j]);
				if (retVal != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at2.add(bb[j] + "^^" + retVal.replaceAll("[^0-9.]", ""));
					} else if (bb[j].contains("THOMSONANC_Flights_MonthYear")) {
						// String val = performActionGetText(bb[j]);

						// if (val != null) {
						Date dt = fmt.parse(retVal);
						fmt.applyPattern("MM/yyyy");
						at2.add(bb[j] + "^^" + fmt.format(dt));
						/*
						 * } else { at2.add(bb[j] + "^^" + retVal); }
						 */
					} else if (bb[j].contains("TotalAmount")
							|| bb[j].contains("AmountOutStand")) {
						// String prcVal = performActionGetText(bb[j]);
						// if (prcVal != null) {
						Long l = Math.round(Double.parseDouble(retVal.replace(
								pound, "").replace(euro, "")));
						at2.add(bb[j] + "^^" + l);
						/*
						 * } else { at2.add(bb[j] + "^^" + prcVal); }
						 */
					} else {
						at2.add(bb[j]
								+ "^^"
								+ retVal.replace(pound, "").replace(euro, "")
										.replace(".00", ""));
					}
				} else {
					at2.add(bb[j] + "^^" + retVal);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStoreAnanlytic:"
						+ a.getMessage());
			} catch (Exception e) {
				logger.error("The execption occured in ValuesStoreAnanlytic:"
						+ e);
			}
		}
		return at2;
	}

	/**
	 * Method to Store one or more values metioned in the Testscript sheet, this
	 * is specific to FO pagination
	 * 
	 * @date Sept 2015
	 * @param ClassName
	 *            ,MethodName,InputArgs
	 * @author Anant
	 * @return boolean
	 */
	public ArrayList<String> ValuesStoreFt(String Store) {
		ArrayList<String> at = new ArrayList<String>();
		List<String> appVal;
		int page = 1;
		String ftCount;
		String[] bb = Store.split(",");
		if (bb[0].equals("Thomson_Flights_Count")) {
			ftCount = performActionGetText(bb[0]).replace("flights", "").trim();
			int val = Integer.parseInt(ftCount);
			if (val <= 20) {
				page = 1;
			} else {
				int page1 = val / 20;
				if (val % 20 == 0)
					page = page1;
				else
					page = page1 + 1;
			}
		}
		for (int j = 2; j <= bb.length - 1; j++) {
			for (int p = 0; p < page; p++) {
				appVal = performActionMultipleGetText(bb[j]);
				if (page > 1) {
					performActionClick(bb[1]);
				}
				for (int p1 = 0; p1 < appVal.size(); p1++) { // System.out.println("The
																// valuee:"+bb[j]+"^^"+appVal.get(p1));
					at.add(bb[j] + "^^" + appVal.get(p1));
				}
			}
		}
		return at;
	}

	/**
	 * The method is used to verify the one or more values Which accepts two
	 * parameters one is List contains values of webElemet and other one is
	 * string which contain Object names
	 * 
	 * @param ay
	 * @param cc
	 *            UPdated on Nov 2016
	 * @author Anant
	 */
	public void ValuesVerify(ArrayList<String> ay, String cc) {
		String expected, actual, pageVal1, pageVal2;
		String[] bb = cc.split(",");
		ArrayList<String> ab = new ArrayList<String>();
		for (int j = 0; j < bb.length; j++) {
			String val = performActionGetText(bb[j]);
			ab.add(bb[j] + "^^" + val);
		}
		for (int i = 0; i < ab.size(); i++) {
			try {
				pageVal2 = ab.get(i).substring(0, ab.get(i).indexOf("^^"));
				pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				if (ab.get(i).toLowerCase().contains("price")
						|| ab.get(i).toLowerCase().contains("discount")) {
					actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();

					if ((actual != null && expected != null)
							&& (actual.length() > 0 && expected.length() > 0)) {
						priceComparisonForTwoPage(pageVal1, pageVal2, expected,
								actual);
					} else {
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);
					}
				} else {
					actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace(pound, "").trim();
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace(pound, "").trim();
					if (expected.contains(actual) && actual.length() > 0) {
						logger.info("trueeeeee");
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);

					} else {
						logger.info("falseeeeee");
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);
					}
				}
			} catch (Exception e) {
				logger.error("The Array index out of bound Exception ValuesVerify: "
						+ e.getMessage());
			}
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}
	
	public void ValuesVerifyWithPriceChange(ArrayList<String> ay, String cc) {

		String expected, actual, pageVal1, pageVal2;
		String[] bb = cc.split(",");
		ArrayList<String> ab = new ArrayList<String>();
		for (int j = 0; j < bb.length; j++) {
			String val = performActionGetText(bb[j]);
			ab.add(bb[j] + "^^" + val);
		}
		for (int i = 0; i < ab.size(); i++) {
			try {
				pageVal2 = ab.get(i).substring(0, ab.get(i).indexOf("^^"));
				pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				if (ab.get(i).toLowerCase().contains("price")
						|| ab.get(i).toLowerCase().contains("discount")) {
					actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();

					if ((actual != null && expected != null)
							&& (actual.length() > 0 && expected.length() > 0)) {
						priceComparisonForTwoPageWithPriceChange(pageVal1, pageVal2, expected,
								actual);
					} else {
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);
					}
				} else {
					actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace(pound, "").trim();
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace(pound, "").trim();
					if (expected.contains(actual) && actual.length() > 0) {
						logger.info("trueeeeee");
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);

					} else {
						logger.info("falseeeeee");
						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);
					}
				}
			} catch (Exception e) {
				logger.error("The Array index out of bound Exception ValuesVerify: "
						+ e.getMessage());
			}
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}

	/**
	 * The method is used to verify the one or more values Which accepts two
	 * parameters one is List contains values of webElemet and other one is
	 * string which contain Object names
	 * 
	 * @param ay
	 * @param cc
	 *            UPdated on March 2016
	 */
	/*
	 * public void ValuesVerify(ArrayList<String> ay, String cc) { String
	 * expected, actual, pageVal1, pageVal2; String[] bb = cc.split(",");
	 * ArrayList<String> ab = new ArrayList<String>(); for (int j = 0; j <
	 * bb.length; j++) { try{ if (bb[j].toLowerCase().contains("price") ||
	 * bb[j].toLowerCase().contains("discount")) { String val =
	 * performActionGetText(bb[j]); if(val!=null) { val =
	 * val.replaceAll("[^0-9.]", ""); if(bb[j].contains("FC_Flights_PerPrice")
	 * || bb[j].contains("FC_Flights_TotalPrice")||bb[j].contains(
	 * "FC_Flights_Discount")) { try{ Integer prVal = (int)
	 * Math.round(Double.parseDouble(val)); ab.add(bb[j]+ "^^"
	 * +prVal.toString()); }catch(Exception e){} }else{ ab.add(bb[j] + "^^" +
	 * val); } }else{ ab.add(bb[j] + "^^" + val); } } else{ ab.add(bb[j] + "^^"
	 * + performActionGetText(bb[j])); } }catch(Exception e){
	 * e.printStackTrace(); } } for (int i = 0; i < ab.size(); i++) { pageVal2 =
	 * ab.get(i).substring(0, ab.get(i).indexOf("^^")); pageVal1 =
	 * ay.get(i).substring(0, ay.get(i).indexOf("^^"));
	 * 
	 * actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
	 * .replace("per person", "").replace("discount", "") .replace("Includes",
	 * "").replace("discount", "") .toLowerCase().replace(, "").replace(" ", "")
	 * .replace("totaldiscount", "").replace("totalprice", "") .replace("pp",
	 * "").trim(); expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
	 * .replace("per person", "").replace("discount", "") .replace("Includes",
	 * "").replace("discount", "") .toLowerCase().replace(pound,
	 * "").replace(" ", "") .replace("totaldiscount", "").replace("totalprice",
	 * "") .replace("pp", "").trim(); if (expected.contains(actual) &&
	 * actual.length()>0) { System.out.println("trueeeeee");
	 * compareStringTwoPage(pageVal1, pageVal2, expected, actual);
	 * 
	 * } else { System.out.println("falseeeeee"); compareStringTwoPage(pageVal1,
	 * pageVal2, expected, actual); } } at1.clear(); at2.clear(); anal.clear();
	 * }
	 */

	/**
	 * The method is used to verify the one or more values of multiple page
	 * Which accepts two parameters one is List contains values of webElemet of
	 * one page and other one is List which contain values of webeElement of
	 * next
	 * 
	 * @param ay
	 * @param cc
	 *            Updated on March 2016
	 */
	@SuppressWarnings("unused")
	public void ValuesVerify1(ArrayList<String> ay, String cc) {
		String expected, actual, pageVal1, pageVal2;
		String[] bb = cc.split(",");
		ArrayList<String> ab = new ArrayList<String>();
		for (int j = 0; j < bb.length; j++) {
			try {
				String val = performActionGetText(bb[j]);
				if (val != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						ab.add(bb[j] + "^^" + val.replaceAll("[^0-9.]", ""));
					} else {
						ab.add(bb[j] + "^^" + val);
					}
				} else {
					ab.add(bb[j] + "^^" + val);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < ab.size(); i++) {
			pageVal2 = ab.get(i).substring(0, ab.get(i).indexOf("^^"));
			pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));

			actual = ab.get(i).substring(ab.get(i).indexOf("^^") + 2)
					.replace("per person", "").replace("discount", "")
					.replace("Includes", "").replace("discount", "")
					.toLowerCase().replace(pound, "").replace(" ", "")
					.replace("totaldiscount", "").replace("totalprice", "")
					.replace("pp", "").replace("+", "").replace("-", "").trim();
			expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
					.replace("per person", "").replace("discount", "")
					.replace("Includes", "").replace("discount", "")
					.toLowerCase().replace(pound, "").replace(" ", "")
					.replace("totaldiscount", "").replace("totalprice", "")
					.replace("pp", "").replace("+", "").replace("-", "").trim();
			if (expected.contains(actual) && actual.length() > 0) {
				logger.info("trueeeeee");
				// compareStringTwoPage(pageVal1, pageVal2, expected, actual);
				compareStringContent(pageVal1, expected, actual);
			} else {
				logger.info("falseeeeee");
				// compareStringTwoPage(pageVal1, pageVal2, expected, actual);
				compareStringContent(pageVal1, expected, actual);
			}
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}

	/**
	 * The method is used to compare the valuse of Map elements based on the key
	 * Which accepts one parameter as Key
	 * 
	 * @param keyVal
	 *            Upadetd on March 2016
	 */
	public void compareStringfromMap(String keyVal) {
		String Expected, Actual, pageValue1, pageValue2, compName1, pageVal1, pageVal2;
		if (mp.size() != 0) {
			try {

				String[] key = keyVal.split(",");
				for (int j = 0; j < key.length && key.length % 2 == 0; j++) {

					int incr = ++j;
					Actual = mp.get(key[j])
							.substring(mp.get(key[j]).indexOf("^^") + 2)
							.replace(pound, "").replace(euro, "");

					Expected = mp.get(key[incr])
							.substring(mp.get(key[incr]).indexOf("^^") + 2)
							.replace(pound, "").replace(euro, "");

					pageVal1 = mp.get(key[j]).substring(0,
							mp.get(key[j]).indexOf("^^"));

					pageVal2 = mp.get(key[incr]).substring(0,
							mp.get(key[incr]).indexOf("^^"));

					pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
							pageVal1.lastIndexOf("_"));
					compName1 = pageVal1
							.substring(pageVal1.lastIndexOf("_") + 1);
					pageValue2 = pageVal2.substring(pageVal2.indexOf("_") + 1,
							pageVal2.lastIndexOf("_"));

					boolean cmpresult = false;

					try {

						if (Expected.equalsIgnoreCase(Actual)) {

							cmpresult = true;
							
							report.log(LogStatus.PASS, compName1
									+ " Verifying condition is passed from "
									+ pageValue1 + " to " + pageValue2
									+ " page where Expected:" + Expected + " "
									+ "Actual:" + Actual);
							logger.info("Comparingsummary parameters" + "-"
									+ "Expected" + Expected + " " + "Actual"
									+ Actual + " " + "Result-" + cmpresult);
						} else {
							cmpresult = false;
							report.log(LogStatus.FAIL, compName1
									+ " Verifying condition is failed from "
									+ pageValue1 + " to " + pageValue2
									+ " page where Expected:" + Expected + " "
									+ "Actual:" + Actual);
							report.attachScreenshot(takeScreenShotExtentReports());
							logger.info("Comparing parameters" + "-"
									+ "Expected" + Expected + " "
									+ "ActualPrice" + Actual + " " + "Result-"
									+ cmpresult);
						}
					} catch (Exception e) {
						logger.info(Expected + " is not selected **********");
						logger.error(e);
						logger.error(Actual + "*******" + e.getMessage());

					}
				}
			} catch (Exception e) {
				logger.error("exception occured in compareStringTwoPage:"
						+ e.getMessage());
			}

		}
		mp.clear();
	}

	/**
	 * The method is used to verify the two List elements Updated on march 2016
	 * 
	 * @param ay
	 * @param cc
	 * updated on 08/02/2019 by Samson V due parameter verification failures when using ValuesStoreAll function
	 */
	public static void clearList() {
		if(at1!=null)
		  at1.clear();
		if(at2!=null)
		at2.clear();
		if(anal!=null)
		anal.clear();
		if(mp!=null)
		mp.clear();

	}

	/**
	 * The method use to compare two array list returns void
	 * 
	 * @param ay
	 * @param cc
	 * @author anant Updated on nov2016
	 */
	public void ValuesVerify(ArrayList<String> ay, ArrayList<String> cc) {
		String expected, actual, pageVal1, pageVal2;
		for (int i = 0; i < cc.size(); i++) {
			try {
				pageVal2 = cc.get(i).substring(0, cc.get(i).indexOf("^^"));
				pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				if (cc.get(i).trim().toLowerCase().contains("price")
						|| cc.get(i).trim().toLowerCase().contains("discount")) {
					actual = cc.get(i).substring(cc.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.replaceAll("[^0-9\\.]+", "").trim();
					if ((actual != null && expected != null)
							&& (actual.length() > 0 && expected.length() > 0)) {
						priceComparisonForTwoPage(pageVal1, pageVal2, expected,
								actual);
					} else {
						compareStringTwoPageAnite(pageVal1, pageVal2, expected,
								actual);
					}
				} else {
					actual = cc.get(i).substring(cc.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace("(", "").replace("-", "");
					expected = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
							.toLowerCase().replace(" ", "")
							.replaceAll("\\r", "").replaceAll("\\n", "")
							.replace("(", "").replace("-", "");
					actual = actual.replace("leadpassenger", "")
							.replace("dob:", "").replace(")", "")
							.replace("duration", "").replace("nights", "")
							.replace("room1:", "").replace(euro, "");
					expected = expected.replace(")", "");
					if (expected.contains(actual) && actual.length() > 0) {
						logger.info("trueeeeee");
						compareStringTwoPageAnite(pageVal1, pageVal2, expected,
								actual);

					} else {
						logger.info("falseeeeee");
						compareStringTwoPageAnite(pageVal1, pageVal2, expected,
								actual);
					}
				}
			} catch (Exception e) {
				logger.error("The array index out of bound execption in ValuesVerify with List parameter:"
						+ e.getMessage());
			}
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}

	/**
	 * The method use to compare two array list returns void
	 * 
	 * @param ay
	 * @param cc
	 */
	/*
	 * public void ValuesVerify(ArrayList<String> ay, ArrayList<String> cc) {
	 * 
	 * String expected, actual, pageVal1, pageVal2; for (int i = 0; i <
	 * cc.size(); i++) { try{ pageVal2 = cc.get(i).substring(0,
	 * cc.get(i).indexOf("^^")); pageVal1 = ay.get(i).substring(0,
	 * ay.get(i).indexOf("^^"));
	 * 
	 * actual = cc.get(i).substring(cc.get(i).indexOf("^^") + 2)
	 * .replace("per person", "").replace("discount", "") .replace("Includes",
	 * "").replace("discount", "") .toLowerCase().replace(pound,
	 * "").replace(" ", "") .replace("totaldiscount", "").replace("totalprice",
	 * "") .replace("pp", "").replace("(", "").trim().replace("-", ""); expected
	 * = ay.get(i).substring(ay.get(i).indexOf("^^") + 2) .replace("per person",
	 * "").replace("discount", "") .replace("Includes", "").replace("discount",
	 * "") .toLowerCase().replace(, "").replace(" ", "")
	 * .replace("totaldiscount", "").replace("totalprice", "") .replace("pp",
	 * "").replace("(", "").trim().replace("-", ""); actual =
	 * actual.replace("leadpassenger", "").replace("dob:", "") .replace(")",
	 * "").replace("duration", "") .replace("nights", "").replace("room1:", "")
	 * .replace(euro, ""); expected = expected.replace(")", ""); if
	 * (expected.contains(actual) && actual.length()>0) {
	 * System.out.println("trueeeeee"); compareStringTwoPageAnite(pageVal1,
	 * pageVal2, expected, actual);
	 * 
	 * } else { System.out.println("falseeeeee");
	 * compareStringTwoPageAnite(pageVal1, pageVal2, expected, actual); }
	 * }catch(Exception e){ logger. error(
	 * "The array index out of bound execption in ValuesVerify with List parameter:"
	 * +e.getMessage()); } }
	 * 
	 * at1.clear(); at2.clear(); anal.clear();
	 * 
	 * }
	 */
	// older one

	/**
	 * Method to Store All the list of parameters
	 * 
	 * Updated on March 2016
	 * 
	 * @param String
	 *            cc
	 * @author Madan
	 * @return void
	 */
	public ArrayList<String> ValuesStoreAll(String ss) {
		String[] bb = ss.split(",");
		for (int j = 0; j < bb.length; j++) {
			try {
				String val = performActionGetText(bb[j]);
				if (val != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at1.add(bb[j] + "^^" + val.replaceAll("[^0-9.]", ""));
					} else if (bb[j].endsWith("ChangeExtra")
							|| bb[j].endsWith("ChangeFees")
							|| bb[j].endsWith("ToPayToday")) {
						at1.add(bb[j]
								+ "^^"
								+ val.replaceAll("[^\\d\\.]+", "").replace("*",
										""));
					} else {
						at1.add(bb[j] + "^^" + val);
					}
				} else {
					at1.add(bb[j] + "^^" + val);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStoreAll1:"
						+ a.getMessage());
			} catch (Exception e) {
				logger.error("The execption occured in ValuesStoreAll:" + e);
			}
		}
		return at1;
	}

	/**
	 * Method to Store All the list of parameters to be verified Updated on
	 * march 2016
	 * 
	 * @param String
	 *            cc
	 * @author Madan
	 * @return void
	 */
	public ArrayList<String> ValuesStoreAll1(String ss) {
		String[] bb = ss.split(",");
		for (int j = 0; j < bb.length; j++) {
			try {
				String retVal = performActionGetText(bb[j]);
				if (retVal != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at2.add(bb[j]
								+ "^^"
								+ performActionGetText(bb[j]).replaceAll(
										"[^0-9.]", ""));
					} else if (bb[j].endsWith("ChangeExtra")
							|| bb[j].endsWith("ToPayToday")) {
						Iterator<String> itrVal = performActionMultipleGetText(
								bb[j]).iterator();
						Double sum = 0.0;
						while (itrVal.hasNext()) {
							String val = itrVal.next().replace(pound, "");
							if (val.contains("-")) {
								sum = sum
										- Double.parseDouble(val.replace("-",
												"").replace("*", ""));
							} else {
								sum = sum
										+ Double.parseDouble(val.replace("*",
												""));
							}
						}
						at2.add(bb[j] + "^^" + sum.toString());
					} else if (bb[j].endsWith("ChangeFees")) {
						at2.add(bb[j]
								+ "^^"
								+ retVal.replaceAll("[^\\d\\.]+", "").replace(
										"*", ""));
					} else {
						at2.add(bb[j] + "^^" + retVal);
					}
				} else {
					at2.add(bb[j] + "^^" + retVal);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStoreAll1:"
						+ a.getMessage());
			} catch (Exception e) {
				logger.error("Exception occured in ValuesStoreAll1:"
						+ e.getMessage());
			}
		}

		return at2;
	}

	/**
	 * The function is used to add the multiple Values Based on the sign of each
	 * component The value get store in map Specifically for amend and cancel
	 * 
	 * @param objcts
	 * @param keyVal
	 */
	public void priceAdder(String objcts, String keyVal) {
		double sum = 0;
		String val = "", retval;
		DecimalFormat dt = new DecimalFormat("#.00");
		String prc[] = objcts.split(",");
		for (int i = 0; i < prc.length; i++) {
			try {
				retval = performActionGetText(prc[i]);
				if (retval != null) {
					if (!retval.equals("")) {
						retval = retval.replace(pound, "").replace("", "")
								.trim();
						if (retval.contains("+")) {
							retval = retval.replace("+", "");
							sum = sum + Double.parseDouble(retval);
						} else {
							retval = retval.replace("-", "");
							sum = Double.parseDouble(retval) - sum;
						}
					}
				}
			} catch (Exception e) {
				logger.info(prc[i] + "Not cpatured from webpage");
				report.log(LogStatus.INFO,
						"Price addition not happend properly:");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		}

		val = dt.format(sum);
		prcStore.put(keyVal, val.replace("-", ""));
	}

	/**
	 * The method used to compare the price with added price added price will be
	 * getting from map by passing the key value Specifically for Amend and
	 * cancel
	 * 
	 * @param Object
	 * @param key
	 */

	public void priceComparison(String Object, String key) {
		String expected, actual, pageVal1;
		expected = prcStore.get(key);
		pageVal1 = Object;
		try {
			actual = performActionGetText(Object).replace(pound, "");
			if (expected.equalsIgnoreCase(actual)) {
				logger.info("trueeeeee");
				compareStringContent(pageVal1, expected, actual);
			} else {
				logger.info("falseeeeee");
				compareStringContent(pageVal1, expected, actual);
			}
		} catch (Exception e) {
			logger.error("The exception occured in the ValuesVerifyContent:"
					+ e.getMessage());
		}
		prcStore.clear();
	}

	/**
	 * The method is to validate the list of parameter with parameters from the
	 * CSV file which accepts two parameter,first once contains the data from
	 * CSV file and second one is page It used for A&C project
	 * 
	 * @param parameter
	 * @param pageVal
	 */
	public void compareParameters(String parameter, String pageVal) {
		String val = fetchTestDataFromMap(parameter), pageVal1, pageVal2, expected, actual;
		pageVal2 = pageVal;
		if (val != null) {
			try {

				String[] cmpP = val.split("\\|");
				for (int i = 0; i < at1.size(); i++) {
					try {
						pageVal1 = at1.get(i).substring(0,
								at1.get(i).indexOf("^^"));
						actual = at1.get(i)
								.substring(at1.get(i).indexOf("^^") + 2)
								.replace("ROOM 1", "").toLowerCase()
								.replace(" ", "").replaceAll("\\(.+?\\)", "");

						if (actual.matches("\\w{2,4}\\d{2}\\w*\\d*")
								&& !actual.matches("\\w{2,4}\\d{2}\\w*\\d{4}")) {
							actual = actual.replaceAll("[a-zA-Z]+", "");
						}

						String[] tstVal = cmpP[i].split("=");
						expected = tstVal[1].toLowerCase().replace(" ", "");

						compareStringTwoPage(pageVal1, pageVal2, expected,
								actual);
						/*
						 * if(expected.contains(actual)){
						 * System.out.println("trueeeeeeee");
						 * compareStringTwoPage(pageVal1, pageVal2, expected,
						 * actual);
						 * 
						 * }else{ System.out.println("falseeeeeee");
						 * compareStringTwoPage(pageVal1, pageVal2, expected,
						 * actual); }
						 */
					} catch (ArrayIndexOutOfBoundsException a) {
						logger.error("The ArrayIndexOutOfBoundsException "
								+ "in compareParameters method " + a);
					} catch (Exception e) {
						logger.error("Exception occured while splitting by "
								+ "cap or equal operator in compareParameters method "
								+ e);
					}
				}

			} catch (Exception e) {
				logger.error("Exception occured while splitting by "
						+ "pipe in compareParameters method " + e);
			}
		} else {
			logger.info("No test data for comparison");
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}

	/**
	 * Method to Verify the stored values with the expected values passing from
	 * TestScript sheet, this is specific to FO validation
	 * 
	 * @date Sept 2015
	 * @param ArrayList
	 *            ay,String cc
	 * @author Madan
	 * @return void
	 */

	@SuppressWarnings("unused")
	public void ValuesVerifyFt(ArrayList<String> ay, String cc) {
		String comp, Value;
		String[] bb = cc.split(",");
		report.log(LogStatus.INFO, "ScenarioName:" + bb[0]);

		if (bb[1].contains("Ascending")) {
			Integer[] arr = new Integer[ay.size()];
			for (int s = 0; s < ay.size(); s++) {
				Value = ay.get(s).substring(ay.get(s).indexOf("^^") + 2).trim();
				arr[s] = Integer.parseInt(Value.replace(pound, "").trim());
			}
			Arrays.sort(arr);
			for (int k = 0; k < ay.size(); k++) {
				comp = ay.get(k).substring(0, ay.get(k).indexOf("^^"));
				Value = ay.get(k).substring(ay.get(k).indexOf("^^") + 2).trim();
				if (Integer.parseInt(Value.replace(pound, "").trim()) == arr[k]) {
					compareString(comp, arr[k].toString(), Value);
				} else {
					compareString(comp, arr[k].toString(), Value);
				}
			}
		} else {
			ArrayList<String> ab = new ArrayList<String>();
			for (int i = 0; i < ay.size(); i++) {
				boolean flag = false;
				comp = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				Value = ay.get(i).substring(ay.get(i).indexOf("^^") + 2).trim();
				if (Integer.parseInt(Value) >= Integer.parseInt(bb[1])
						&& Integer.parseInt(Value) <= Integer.parseInt(bb[2])) {
					flag = true;
					logger.info("trueeeeee");
					compareStringOnePage(comp, flag, bb[1] + "-" + bb[2], Value);
				} else {
					logger.info("falseeeeee");
					compareStringOnePage(comp, flag, bb[2] + "-" + bb[2], Value);
				}
			}
		}
	}

	/**
	 * The method is used to compare the paramter captured from web page with
	 * content specified in the test script sheet file
	 * 
	 * @param ay
	 * @param cc
	 */
	public void ValuesVerifyContent(ArrayList<String> ay, String cc) {
		String expected, actual, pageVal1;
		String[] bb = cc.split(",");
		for (int i = 0; i < bb.length; i++) {
			try {
				pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				expected = bb[i].replace("per person", "")
						.replace("Includes", "").replace("discount", "")
						.replace("Miss", "").replace("Mstr", "")
						.replace("Mr", "").toLowerCase().replace(pound, "")
						.replace(" ", "").replace("totaldiscount", "")
						.replace("totalprice", "").replace("pp", "")
						.replace(euro, "").trim();
				actual = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
						.replace("per person", "").replace("Includes", "")
						.replace("discount", "").replace("Miss", "")
						.replace("Mstr", "").replace("Mr", "").toLowerCase()
						.replace(pound, "").replace(" ", "")
						.replace("totaldiscount", "").replace("totalprice", "")
						.replace(euro, "").replace("pp", "").trim()
						.replace(",", "");

				if (expected.equalsIgnoreCase(actual) && actual.length() > 0) {
					logger.info("trueeeeee");
					compareStringContent(pageVal1, expected, actual);

				} else {
					logger.info("falseeeeee");
					compareStringContent(pageVal1, expected, actual);
				}
			} catch (Exception e) {
				logger.error("The exception occured in the ValuesVerifyContent:"
						+ e.getMessage());
			}
		}
		at1.clear();
		at2.clear();
		anal.clear();
	}

	/**
	 * The method is used to compare the parameters captured from web page with
	 * content specified in the test script sheet file using 'contains'
	 * condition.
	 * 
	 * @param ay
	 * @param cc
	 * @author Chitra S.
	 * @updated by Anant
	 */

	public void ValuesVerifyContent_Contains(ArrayList<String> ay, String cc) {
		String expected, actual, pageVal1;
		boolean cmpResult;
		String[] bb = cc.split(",");
		for (int i = 0; i < bb.length; i++) {
			try {
				pageVal1 = ay.get(i).substring(0, ay.get(i).indexOf("^^"));
				expected = bb[i].replace("Includes", "")
						.replace("discount", "").replace("Miss", "")
						.replace("Mstr", "").replace("Mr", "").toLowerCase()
						.replace(pound, "").replace(" ", "")
						.replace("totaldiscount", "").replace("totalprice", "")
						.replace("pp", "").replace(euro, "").trim();
				actual = ay.get(i).substring(ay.get(i).indexOf("^^") + 2)
						.replace("per person", "").replace("Includes", "")
						.replace("discount", "").replace("Miss", "")
						.replace("Mstr", "").replace("Mr", "").toLowerCase()
						.replace(pound, "").replace(" ", "")
						.replace("totaldiscount", "").replace("totalprice", "")
						.replace(euro, "").replace("pp", "").trim()
						.replace(",", "");

				if (expected.toLowerCase().contains(actual.toLowerCase())
						|| actual.toLowerCase()
								.contains(expected.toLowerCase())) {
					logger.info("trueeeeee");
					cmpResult = true;
					compareStringOnePage(pageVal1, cmpResult, expected, actual);
					// compareStringContent(pageVal1, expected, actual);

				} else {
					cmpResult = false;
					logger.info("falseeeeee");
					compareStringOnePage(pageVal1, cmpResult, expected, actual);
					// compareStringContent(pageVal1, expected, actual);
				}
			} catch (Exception e) {
				logger.error("The exception occured in the ValuesVerifyContent:"
						+ e.getMessage());
			}
		}
		if (at1.size() != 0 || at2.size() != 0) {
			at1.clear();
			at2.clear();
		}
	}

	/**
	 * The following method is used to replace multiple value
	 * 
	 * @param str
	 * @param string
	 * @return replaced string
	 * @Author-Anant
	 */
	@SuppressWarnings("unused")
	public String multiplSubStringReplacer(String str, String string) {

		StringBuffer build = new StringBuffer();
		@SuppressWarnings("serial")
		Map<String, String> keyVal = new HashMap<String, String>() {
			{
				put("per person", "");
				put("includes", "");
				put("discount", "");
				put("totaldiscount", "");
				put("pp", "");
				put(pound, "");
				put(euro, "");
				put(pound, "");
				put("mstr", "");
				put("miss", "");
				put("mr", "");
				put(",", "");

			}
		};

		string = string.toLowerCase();
		str = str.toLowerCase();
		Pattern pat = Pattern.compile(string);
		Matcher mat = pat.matcher(str);
		while (mat.find()) {
			try {
				str = str.replace(mat.group(), keyVal.get(mat.group()));
			} catch (IndexOutOfBoundsException e) {
			} catch (Exception e1) {
			}
		}
		return str;
	}

	public void ValuesVerifyStringConcat(ArrayList<String> ay, String cc) {
		String expected = "", actual, pageVal1;
		try {
			String[] bb = cc.split(",");
			pageVal1 = ay.get(0).substring(0, ay.get(0).indexOf("^^"));
			actual = ay.get(0).substring(ay.get(0).indexOf("^^") + 2).trim()
					.replace(",", "");
			for (int i = 0; i < bb.length; i++) {
				if (bb[i].contains("_")) {
					expected = expected + " " + performActionGetText(bb[i]);
				} else {
					expected = bb[i].trim();
				}
			}

			if (expected.equalsIgnoreCase(actual)) {
				logger.info("trueeeeee");
				compareStringContent(pageVal1, expected, actual);

			} else {
				logger.info("falseeeeee");
				compareStringContent(pageVal1, expected, actual);
			}

		} catch (Exception e) {
			logger.error("The exception occured in the ValuesVerifyContent:"
					+ e.getMessage());
		}
	}

	/**
	 * Method to compare the expected and actual paramaters
	 * 
	 * @date Sept 2015
	 * @param String
	 *            ComponentName,String Expected, String Actual
	 * @author Madan
	 * @return void
	 */
	public void compareString(String ComponentName, String Expected,
			String Actual) {
		if (Expected.equalsIgnoreCase(Actual)) {

			/*
			 * System.out.println(ComponentName + " " +
			 * "Verifying condition is passed Expected:" + Expected + " " +
			 * "Actual:" + Actual);
			 */
			report.log(LogStatus.PASS, ComponentName + " "
					+ "Verifying condition is passed from " + " Expected:"
					+ Expected + " " + "Actual:" + Actual);
			logger.info(ComponentName + " " + "-" + "Expected" + Expected + " "
					+ "Actual" + Actual + " ");

		} else {

			/*
			 * System.out.println(ComponentName + " " +
			 * "Verifying condition is failed  where Expected:" + Expected + " "
			 * + "Actual:" + Actual);
			 */
			report.log(LogStatus.FAIL, ComponentName + " "
					+ " Verifying condition is failed where Expected:"
					+ Expected + " " + "ActualPrice" + Actual);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(ComponentName + " " + "-" + "Expected" + Expected + " "
					+ "ActualPrice" + Actual + " ");
		}
	}

}
