package com.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class WebBAUCalculatePercentagePass {
	
	@SuppressWarnings("static-access")
	public static TreeMap<String, String> percentCalculation() throws ParseException, IOException {
		
		TreeMap<String, String> allPassVals = new TreeMap<String, String>();
		TreeMap<String, String> eMap = new TreeMap<String, String>();
		TreeMap<String, String> envBrandMap = new TreeMap<String, String>();
		TreeSet<String> brandSet = new TreeSet<String>();
		TreeMap<String, String> allMap = new TreeMap<String, String>();
		
		String rptFlderPath, finalResultFldPath, prjPath;
		String line,sPerPass = null,scenarioName = null,line1="",testCaseName = null,flagStatus,iteratorVal,brandName = null,envtName = null,envBranPassVal = null, envBranPass = null,runEnvt = null,buildName = null,buildTH = "false",envName=null ;
		String htmlFName, fStatus = "false";
		
		float total=0,passed = 0 ,failed = 0 ,errored = 0, allFail = 0, perPass = 0,perFail;
		int lineCt=0,failCt,failCt1,intPerPass;
		boolean failFound = true,repeatSteps = false,testCaseId = false,testCaseIdFound = false;
		List<String[]> scenarioList = null;
		
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();
		

		//rptFlderPath = prjPath + "//reports//";
		rptFlderPath = "C:\\Tomcat\\webapps\\Reports\\execution_reports";

		finalResultFldPath = prjPath + "//reports//";
		
		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();

		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		
		for(File lstFlder : listOfFolder)
		{ 
			   isDirectryFlag = lstFlder.isDirectory();
		 if(isDirectryFlag && !lstFlder.toString().contains("images"))
		 {
			if(lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}"))
			{
				   fmt.applyPattern("dd-MM-yyyy");
				   datFolder.add(lstFlder);	  
		    }  
		  }
		}
		
		if(datFolder.size()!=0)
		{ 
		         Date strdDate;
			     long small = 0,diff,diffDays;
			     File filPath;
			     String dateValue,dtVal;
			  
			     filPath = datFolder.get(0);
			     dateValue = filPath.getAbsolutePath();
		 try{
		         strdDate = fmt.parse(filPath.getName().trim());
			     diff = sysDate.getTime()-strdDate.getTime();
			     diffDays = diff / (24 * 60 * 60 * 1000);
			     small  = diffDays;
			}
		 catch (ParseException e) {
				 e.printStackTrace();
			}  
			  
		  for(int j = 1; j < datFolder.size(); j++)
		  {
				      dtVal = datFolder.get(j).getName();
			try{
					  strdDate = fmt.parse(dtVal);
					  diff = sysDate.getTime()-strdDate.getTime();
					  diffDays = diff / (24 * 60 * 60 * 1000);
					  if(diffDays < small)
				      {
					      small = diffDays;
					      dateValue = datFolder.get(j).getAbsolutePath();
					  }
				}
			catch (ParseException e) {
					    e.printStackTrace();
			 }
		  }

		 
		File amPmFilVal = new File(dateValue);
		long lastMod = Long.MIN_VALUE;
		File choice = null;
		if(amPmFilVal.exists()){ 
			        File[] subFolder = amPmFilVal.listFiles();
		for(File subFld : subFolder)
		{
			    if(subFld.isDirectory())
				{
					if(subFld.lastModified() > lastMod){
						lastMod = subFld.lastModified();
						choice = subFld;
						System.out.println(choice+"====sadh\n\n");
						
					}
				}	
		 }
		}
		
		String[] htmlFlName  = choice.list();
		List<String[]> finalList = new ArrayList<String[]>();
		
		for(String htFlName : htmlFlName)
		 {   

			
			 if(!htFlName.contains("images") && !htFlName.contains("AutomationSummary"))
			 {
				 failed = 0; 
				 errored =0;
				 passed = 0;
				 total = 0;
				 
				 File htmlFileName = new File(choice.toString()+File.separator+htFlName);
				 BufferedReader reader;
				 try{
						reader = new BufferedReader(new FileReader(htmlFileName));
						scenarioName = htmlFileName.getName().replace(".html", "");
						
						// code for getting the file name
						
						htmlFName = htmlFileName.toString();
						htmlFName = htmlFName.replace(choice.toString(),"").replace("\\","");
						
						
						if(htmlFileName.length()!=0)
						 { 
							
						     while((line=reader.readLine())!=null)
						    {   
						    	 
						    	 
						    	line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
						    	line1 = line1.replace("<td class='step-details' colspan='2'>","").replace("</td>","").trim();
						    	
						    	if(line.contains("Launching URL:")){
						    		System.out.println(htmlFileName+"===sdfc\n\n");
						    		envName =  line.replace("http:", "").replace("https:", "").replace("Launching URL:", "").replace("Page Url:", "").trim();
							    	  String comUrl = envName.replace("//",""); 
							    	  System.out.println(comUrl);
							    	  String[] allenv = envName.split("\\.");
							    	  brandName = allenv[1];
							    	  envtName = allenv[0].replace("/","");
							    	  envtName = envtName.toLowerCase();
							    	  
							    	  
							    	  if(envtName.contains("-")) {
							    		  String[] envArray = envtName.split("-");
							    		  envtName = envArray[1];
							    	  }
							    	  else {
							    		  envtName = envtName;
							    	  }
							    	  
								    	 System.out.println(envtName);
								    	 System.out.println(htmlFName+"===\n\n");
								    	 /*
								    	 if(envtName.equals("www")){
								    		 envtName="PAT";
								    	 }
								    	 */
										 
										 								    	  if(comUrl.contains("holidayhypermarket")){
								    		  brandName = "HH";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  
								    					    		  
								    	  }
								    	  else
								    	  if(comUrl.contains("cruisedeals")){
								    		  brandName = "CD";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  
								    					    		  
								    	  }
								    	  else
										 
										 
								    	 if(comUrl.contains("cruise")){
								    		  brandName = "CR";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  
								    					    		  
								    	  }
								    	 else
								    		 if(comUrl.contains("tuiretailhish") && comUrl.contains("/retail/headoffice/login")){
								    			  brandName = "TH-HO";
									    		  envBranPass = brandName; 
									    		  envBranPassVal = envtName+"#"+envBranPass;
								    			  System.out.println(envBranPass+"====jdks\n\n");
								    		  }
								    		  else
								    			  if(comUrl.contains("tuiretailhish") && comUrl.contains("/retail/stockton/login")){
									    			  brandName = "TH-STK";
										    		  envBranPass = brandName;
										    		  envBranPassVal = envtName+"#"+envBranPass;
									    			  System.out.println(envBranPass+"====jdks\n\n");
									    		  }
									    		  else
									    			  if(comUrl.contains("tuiretailhish") && comUrl.contains("/retail/thirdparty/login")){
										    			  brandName = "TH-3RD";
											    		  envBranPass = brandName; 
											    		  envBranPassVal = envtName+"#"+envBranPass;
										    			  System.out.println(envBranPass+"====jdks\n\n");
										    		  }
											    	  else
											    		  if(comUrl.contains("falconretailhish") && comUrl.contains("/retail/f/headoffice/login")) {
											    			  brandName = "IE-HO";
												    		  envBranPass = brandName; 
												    		  envBranPassVal = envtName+"#"+envBranPass;
											    			  System.out.println(envBranPass+"====jdks\n\n");
											    		  }
											    		  else
											    			  if(comUrl.contains("falconretailhish") && comUrl.contains("/retail/f/stockton/login")) {
												    			  brandName = "IE-STK";
													    		  envBranPass = brandName; 
													    		  envBranPassVal = envtName+"#"+envBranPass;
												    			  System.out.println(envBranPass+"====jdks\n\n");
												    		  }
												    		  else
												    			  if(comUrl.contains("falconretailhish") && comUrl.contains("/retail/f/thirdparty/login")) {
													    			  brandName = "IE-3RD";
														    		  envBranPass = brandName; 
														    		  envBranPassVal = envtName+"#"+envBranPass;
													    			  System.out.println(envBranPass+"====jdks\n\n");
													    		  }
								    	  else
								    	  if((comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")) && (comUrl.contains("flight"))){
								    		  brandName = "THFO";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  
								    	  }
								    	  else
								    	  if(comUrl.contains("tuiholidaysprjuat") && (comUrl.contains("flight"))){
								    		  brandName = "IEFO";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  
								    	  }
								    	  else
									    		if(comUrl.contains("firstchoiceprjuat") && comUrl.contains("multi-centre")){
								    				  brandName = "FCMC";
								    				  envBranPass = brandName;
								    				  envBranPassVal = envtName+"#"+envBranPass;
								    			  }
								    	  else
								    		  if(comUrl.contains("firstchoice") && comUrl.contains("your-account/login")) {
								    			  brandName = "FCCA";
									    		  envBranPass = brandName; 
									    		  envBranPassVal = envtName+"#"+envBranPass;
									    		  System.out.println(envBranPassVal+"===123\n\n");
								    		  }
								    	  else
								    	  if(comUrl.contains("firstchoice")){
								    		  brandName = "FC";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    	  }

								    	  else
								    		  if(comUrl.contains("tuiholidaysprjuat.ie/f/your-account/login") ) {
								    			  brandName = "IECA";
									    		  envBranPass = brandName; 
									    		  envBranPassVal = envtName+"#"+envBranPass;
								    		  }
									    	  else
									    		  if(comUrl.contains("tuiprjuat") && comUrl.contains("your-account/register")  || comUrl.contains("your-account/login")) {
									    			  brandName = "CA";
										    		  envBranPass = brandName;
										    		  envBranPassVal = envtName+"#"+envBranPass;
									    		  }

								    	  else
									        if(comUrl.contains("holidays") && comUrl.contains("lakes-and-mountains")){
								    		  
									    			  brandName = "LM";
									    			  envBranPass = brandName;
									    			  envBranPassVal = envtName+"#"+envBranPass;
									    			  System.out.println(envBranPass+"====jdks\n\n");
									    		}
									    	  else
										    	  if(comUrl.contains("tuiholidaysprjuat") || comUrl.contains("falconholidaysprjuat")){
										    		  brandName = "IE";
										    		  envBranPass = brandName;
										    		  envBranPassVal = envtName+"#"+envBranPass;
										    		  
										    	  }
									    	
									    		else
									    			
									    		if(comUrl.contains("tuiprjuat") && comUrl.contains("multi-centre")){
								    				  brandName = "THMC";
								    				  envBranPass = brandName;
								    				  envBranPassVal = envtName+"#"+envBranPass;
								    			  }
									    		else
											    	  if(comUrl.contains("www.tui.co.uk")){
											    		  brandName = "TH";
											    		  buildTH = "true";
											    		  envtName = "PROD";
											    		  envBranPass = brandName;
											    		  
											    		  envBranPassVal = envtName+"#"+envBranPass;
											    	  }
								    	  else
								    	  if(comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat") && !comUrl.contains("lakes-and-mountains") || !comUrl.contains("multi-centre")){
								    		  brandName = "TH";
								    		  buildTH = "true";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    	  }
								    	  
								    	  
								    	  else{
								    		  brandName = "Not found";
								    		  envBranPass = brandName;
								    		  envBranPassVal = envtName+"#"+envBranPass;
								    	  }
							    	  
						    	}
						    	
						    	/*
						    	if(!buildTH.equals("false") && line.contains("buildversion")){
							    	
						    		buildName = line.replace("<td class='step-details' colspan='2'>","");
						    		buildName = buildName.replace("OR_homepage_buildversion text is captured as <strong>","");
						    		
						    		buildName = buildName.replace("</strong>","");
						    		buildName = buildName.replaceAll("\\s+","");
						    		buildName = buildName.replace("_","-");
						    		
						    		envBrandMap.put(buildName, envtName);
						    		
						   	}
						    	*/
						    	
						    	if(line.contains("title='FAIL'")){
						    		failed++;
						    		//System.out.println(line+htmlFName);
						    		
						    	}
						    	if(line.contains("title='ERROR'")){
						    		errored++;
						    		//System.out.println(line+htmlFName);
						    	}
						    	if(line.contains("title='PASS'")){
						    		passed++;
						    		//System.out.println(line+htmlFName);
						    	
						    	}
						    	allFail = failed+errored;
						    	
						    	total = allFail+passed;
						    	
						    	if(total!=0){
						    	
						    	perPass = (passed/total)*100;
						    	perPass = (float) Math.ceil(perPass);
						    	sPerPass = Float.toString(perPass);
						    	
						    	sPerPass = sPerPass+"#"+htmlFileName;
						    	
						    	
						    	
						    	

						    }
						    	
						    	
						    }
						     System.out.println(sPerPass+"%===4567\n\n");
						     System.out.println(envBranPassVal+"====jdks\n\n");
						    	eMap.put(envBranPassVal, sPerPass);
						 }
						else{
							  System.out.println("The html file doesn't have the content:");
						  }
				 }catch (FileNotFoundException e){
					   e.printStackTrace();
				  } catch(IOException e){
					   e.printStackTrace();
				  }
				 
			 }
			 
		 }
		
		
		
		
		
		
		
		
		} // last if loop. Everything needs to be inside this
		
		// writing all details in hash
				Set set = envBrandMap.entrySet();
		        Iterator iterator = set.iterator();
		        for (String key : envBrandMap.keySet()) {
		        	 
		        while(iterator.hasNext()) {
		           Map.Entry mentry = (Map.Entry)iterator.next();
		           for (String key2 : eMap.keySet()) {
		        	   Set set1 = eMap.entrySet();
		               Iterator iterator1 = set1.iterator();
		           while(iterator1.hasNext()){
		           Map.Entry envtry = (Map.Entry)iterator1.next();
		           if(mentry.getValue().equals(envtry.getKey())){
		        	   
		        	  // System.out.println(mentry.getKey() +" "+ envtry.getKey()+ envtry.getValue()+"===bsbsb\n\n");
		        	   
		        	   Map<String, String> brandEnv = new TreeMap<>();
		        	   String envValue = envtry.getKey()+"_"+envtry.getValue();
		        	   
		        	   allMap.put((String) mentry.getKey(), envValue);
		        	   //brandEnv.put(envtry.getKey(), envtry.getValue());
		        	   //allMap.put((String) mentry.getKey(), (TreeMap<String, String>) brandEnv);
		        	   
		           }
		        }
		        }
		        }
		        
		        }
		
		
		
        Set set2 = eMap.entrySet();
        Iterator iterator2 = set2.iterator();
    while(iterator2.hasNext()){
    Map.Entry mentry2 = (Map.Entry)iterator2.next();
    System.out.print("Build deployed is: "+mentry2.getKey() + " Envt & Brands are ");
    System.out.println(mentry2.getValue());
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
    }
		
		return eMap;
	}

}
	
