package com.utilities;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

public class WebBAUSummaryGenerator {

    public String filename, htmlContent;

    /**
     * The method is used to extract the file name
     *
     * @return string file name
     * @author Anant Patil
     */

    @SuppressWarnings("static-access")
    public static Map<String, Map<String, ArrayList<Scenario>>> reportForamtion() throws ParseException, IOException {
        // ============================================================================
        String rptFlderPath, finalResultFldPath, prjPath;
        String line, scenarioName = null, line1 = "", testCaseName = null, flagStatus, iteratorVal, brandName = null, envtName = null, envBranPass = "FC", runEnvt = null, buildName = null, buildTH = "false", envName = null;
        String scenarioNameReg = null, branScenarioName = null, bookingRefNumber = "-";
        String htmlFName = null, fStatus = "false", dateOfExecution, techIssues = "false";

        boolean failFound = true, passFound = true, noTechDiff = true, testStatus=false;
		String techDifficulties = null;

        TreeSet<String> brandPassPer = new TreeSet<String>();

        Map<String, String> scenarioMap = new HashMap<>();

        TreeMap<String, TreeSet<String>> eMap = new TreeMap<String, TreeSet<String>>();
        TreeMap<String, String> envBrandMap = new TreeMap<String, String>();
        TreeSet<String> brandSet = new TreeSet<String>();
        TreeMap<String, String> allMap = new TreeMap<String, String>();

        ArrayList<String> failFileList= new ArrayList<String> ();
        Map<String, String> BookFileList= new HashMap<String, String> ();
        Map<String, Map<String, String>> tableStructure = new TreeMap<>();
        Map<String, Object> requestBody = new HashMap<String, Object>();
        boolean isDirectryFlag;
        List<File> datFolder = new ArrayList<File>();

        Map<String, Map<String, ArrayList<Scenario>>> finalMap = new TreeMap<>();
        ArrayList<Scenario> scenarioList = new ArrayList<Scenario>();

        File readPrjPath = new File("");
        //prjPath = readPrjPath.getAbsolutePath();
        prjPath = readPrjPath.getAbsolutePath();


        //rptFlderPath = prjPath + "//reports//";
        rptFlderPath = prjPath + "\\Tomcat\\webapps\\Reports\\execution_reports\\";

        finalResultFldPath = prjPath + "\\Tomcat\\webapps\\Reports\\execution_reports\\";


        WebBAUSummaryGenerator report = new WebBAUSummaryGenerator();
        SimpleDateFormat fmt = new SimpleDateFormat();
        Date sysDate = new Date();

        File readFlder = new File(rptFlderPath);
        File[] listOfFolder = readFlder.listFiles();



//code for getting a list of the files in the latest folder
        for (File lstFlder : listOfFolder) {
            isDirectryFlag = lstFlder.isDirectory();
            if (isDirectryFlag && !lstFlder.toString().contains("images")) {
                if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
                    fmt.applyPattern("dd-MM-yyyy");
                    datFolder.add(lstFlder);
                }
            }
        }

        if (datFolder.size() != 0) {
            Date strdDate;
            long small = 0, diff, diffDays;
            File filPath;
            String dateValue, dtVal;

            filPath = datFolder.get(0);
            dateValue = filPath.getAbsolutePath();
            try {
                strdDate = fmt.parse(filPath.getName().trim());
                diff = sysDate.getTime() - strdDate.getTime();
                diffDays = diff / (24 * 60 * 60 * 1000);
                small = diffDays;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (int j = 1; j < datFolder.size(); j++) {
                dtVal = datFolder.get(j).getName();
                try {
                    strdDate = fmt.parse(dtVal);
                    diff = sysDate.getTime() - strdDate.getTime();
                    diffDays = diff / (24 * 60 * 60 * 1000);
                    if (diffDays < small) {
                        small = diffDays;
                        dateValue = datFolder.get(j).getAbsolutePath();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            File amPmFilVal = new File(dateValue);
            long lastMod = Long.MIN_VALUE;
            File choice = null;
            if (amPmFilVal.exists()) {
                File[] subFolder = amPmFilVal.listFiles();
                for (File subFld : subFolder) {
                    if (subFld.isDirectory()) {
                        if (subFld.lastModified() > lastMod) {
                            lastMod = subFld.lastModified();
                            choice = subFld;
                            System.out.println(choice+"====sadh\n\n");

                        }
                    }
                }
            }
//end of code

//Capturing the date of execution

            String folderPath = choice.toString();
            folderPath = folderPath.replace("\\", "#");

            String[] pathArray = folderPath.split("#");
            dateOfExecution = pathArray[pathArray.length - 2];

            //System.out.println(pathArray[pathArray.length-2]);

//end of code		


            String[] htmlFlName = choice.list();
            List<String[]> finalList = new ArrayList<String[]>();
            fStatus = "false";
//code for reading the html reports one by one in a loop
            for (String htFlName : htmlFlName) {
            	System.out.println(htFlName);

                if (!htFlName.contains("images") && !htFlName.contains("AutomationSummary")) {

                    File htmlFileName = new File(choice.toString() + File.separator + htFlName);


                    
                    
   				 Scanner scanner=new Scanner(htmlFileName);
   				 
  				 while(scanner.hasNextLine()){
   					 final String lineFromFile = scanner.nextLine();
   					   if(lineFromFile.contains("title=\'FAIL\'")) { 
   						  
   						   String fileName = htmlFileName.toString();
   						failFileList.add(fileName);
   						 //  System.out.println(fStatus+"=====\n"+htmlFileName+"\n"+lineFromFile);
   						   
   						   break;

   				 }
   				 }
  				 
  				 Scanner bookingRefScan = new Scanner(htmlFileName);
  				 while(bookingRefScan.hasNextLine()){
  					 final String lineFromFileBook = bookingRefScan.nextLine();
  					   if(lineFromFileBook.contains("ReferenceNumber") && !lineFromFileBook.contains("could not get") ) { 
  						  
  						   String[] bookRefLine = lineFromFileBook.split("<strong>");
  						   String bkNum = bookRefLine[1].replace("</strong></td>", "");
  						   System.out.println(bkNum+"===\n\n");
  						   String fileNameBook = htmlFileName.toString();
  						 BookFileList.put(fileNameBook, bkNum);
  						//  System.out.println(fStatus+"=====\n"+htmlFileName+"\n"+lineFromFileBook);
  						   
  						   break;

  				 }
  				 }
   				
                    BufferedReader reader;
                    int BUFFER_SIZE = 1000;

                    try {
                        reader = new BufferedReader(new FileReader(htmlFileName));
                        scenarioName = htmlFileName.getName().replace(".html", "");

                        // code for getting the file name

                        htmlFName = htmlFileName.toString();
                        htmlFName = htmlFName.replace(choice.toString(), "").replace("\\", "");
                      //  System.out.println(htmlFName+"===adas\n\n");

                        
                        if (htmlFileName.length() != 0) {

                            while ((line = reader.readLine()) != null) {
                            	
                           for(String fName :	failFileList) {
                        	   String onlyFileName = fName.replace(choice.toString(), "").replace("\\", "");
                        	   if(onlyFileName.equals(htmlFName)) {
                        		   fStatus = "true";
                        		   break;
                        	   }
                        	   else {
                        		   fStatus = "false";
                        		   techIssues = "false";
                        		   techDifficulties = null;
                        	   }
                           }

                           
                           for (Entry<String, String> entry : BookFileList.entrySet()) {
                           
                        	   String bName = entry.getKey();
                        	   String onlyFileName2 = bName.replace(choice.toString(), "").replace("\\", "");
                        	   if(onlyFileName2.equals(htmlFName)) {
                        		   bookingRefNumber = entry.getValue();
                        		   break;
                        	   }
                        	   else {
                        		   bookingRefNumber = "-";
                        	   }

                           }
                          

    
                           
                           
                           //System.out.println(fStatus+htmlFileName+"outside status\n"); 	
                                line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
                                line1 = line1.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();

                                
                                if(fStatus.equals("true")) {
                             	   if(line.contains("the element value of page:You can go back to viewing your holiday by clicking here")) {
                             		reader.mark(BUFFER_SIZE);
                             		String a= reader.readLine();
                             		a = a+" "+reader.readLine();
                             		a = a+" "+reader.readLine();
                             		a = a.replace("</td>", "");
                             		techDifficulties = a;
                             		techIssues = "true";
                             		break;
                             		
                             	   }
                             	   
                             	   
                             	   
                                }
                                
                                
                                
                                
                                
//Code capturing envt & brand details


                                if (line.contains("Launching URL:")) {
                                    envName = line.replace("http:", "").replace("https:", "").replace("Launching URL:", "").replace("Page Url:", "").trim();
                                    String comUrl = envName.replace("//", "");

                                    String[] allenv = envName.split("\\.");
                                    brandName = allenv[1];
                                    envtName = allenv[0].replace("/", "");
                                    envtName = envtName.toLowerCase();
                                    
							    	  if(envtName.contains("-")) {
							    		  String[] envArray = envtName.split("-");
							    		  envtName = envArray[1];
							    	  }
							    	  else {
							    		  envtName = envtName;
							    	  }

                                    if (comUrl.contains("cruise")) {
                                        brandName = "CR";
                                        envBranPass = brandName;
                                    } else if(comUrl.contains("/retail/headoffice/login") || comUrl.contains("/retail/f/headoffice/login")){
                                        brandName = "AL-HO";
                                        envBranPass = brandName;
                                    } else if(comUrl.contains("/retail/stockton/login")  || comUrl.contains("/retail/f/stockton/login")){
                                        brandName = "AL-STK";
                                        envBranPass = brandName;
                                    } else if(comUrl.contains("/retail/thirdparty/login") || comUrl.contains("/retail/f/thirdparty/login")){
                                        brandName = "AL-3RD";
                                        envBranPass = brandName;
                                    } else if ((comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")) && (comUrl.contains("flight"))) {
                                        brandName = "THFO";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiholidaysprjuat") && (comUrl.contains("flight"))) {
                                        brandName = "IEFO";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("firstchoiceprjuat") && comUrl.contains("multi-centre")) {
                                        brandName = "FCMC";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("firstchoice") && comUrl.contains("your-account/login")) {
                                        brandName = "FCCA";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("firstchoice")) {
                                        brandName = "FC";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiholidaysprjuat.ie/f/your-account/login")) {
                                        brandName = "IECA";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("your-account/login")) {
                                        brandName = "THCA";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiholidaysprjuat") || comUrl.contains("falconholidaysprjuat")) {
                                        brandName = "IE";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("lakes-and-mountains")) {
                                        brandName = "LM";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat") && comUrl.contains("multi-centre")) {
                                        brandName = "THMC";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("www.tui.co.uk")) {
                                        brandName = "TH";
                                        buildTH = "true";
                                        envtName = "PROD";
                                        envBranPass = brandName;
                                    } else if (comUrl.contains("tuiprjuat.se/boka-flyg") || comUrl.contains("tuiprjuat.co.uk/boka-flyg")) {
                                        brandName = "SEFO";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    }     
                                    else if (comUrl.contains("tuiprjuat.fi/varaa-lento") || comUrl.contains("tuiprjuat.co.uk/varaa-lento")) {
                                        brandName = "FIFO";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    } 
                                    else if (comUrl.contains("tuiprjuat.dk/bestil-fly") || comUrl.contains("tuiprjuat.co.uk/bestil-fly")) {
                                        brandName = "DKFO";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    } 
                                    else if (comUrl.contains("tuiprjuat.no/bestill-fly") || comUrl.contains("tuiprjuat.co.uk/bestill-fly")) {
                                        brandName = "NOFO";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    } 
                                    else if (comUrl.contains("tuiprjuat") || comUrl.contains("thomsonprjuat")) {
                                        brandName = "TH";
                                        buildTH = "true";
                                        envBranPass = brandName;
                                    } else {
                                        brandName = "Not found";
                                        envBranPass = brandName;
                                    }

                                    //System.out.println(comUrl+"====jdks\n\n");
                                }
//end of code

// code for capturing the scenario names

                                if (line.contains("ScenarioName:TestScenario:")) {


                                    scenarioNameReg = line.replace("ScenarioName:TestScenario:", "");
                                    //end of code
                                    scenarioMap.put(scenarioNameReg, envBranPass);
                                }


                            } // end of html reader while
                        } else {
                            System.out.println("The html file doesn't have the content:");
                        }
						
						
/*
						Set<String> keyset=scenarioMap.keySet();
						 for(Iterator iterator = keyset.iterator(); iterator.hasNext();)
						{
						String string = (String) iterator.next();
						 System.out.println(string+"==="+scenarioMap.get(string));
						}
*/


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                Scenario scenerio = new Scenario();
                scenerio.setEnv(envtName);
                scenerio.setBrand(envBranPass);
                scenerio.setScenarioNameReg(scenarioNameReg);
                scenerio.setBookingId(bookingRefNumber);
                scenerio.setStatus(fStatus);
                scenerio.setTechnicalDifficultes(techIssues);
                scenerio.setTechnicalDifficultesMessage(techDifficulties);
                scenerio.setDateOfExecution(dateOfExecution);
                scenerio.setfileName(htmlFName);
                scenarioList.add(scenerio);
                failFound = true;


                if(envtName!=(null)) {
                if (!finalMap.containsKey(envtName)) {
                    finalMap.put(envtName, new TreeMap<String, ArrayList<Scenario>>());
                }

                if (!finalMap.get(envtName).containsKey(envBranPass)) {
                    finalMap.get(envtName).put(envBranPass, new ArrayList<Scenario>());
                }
                finalMap.get(envtName).get(envBranPass).add(scenerio);

            }
            }

        } //last if loop of the entire script
        
        
        fStatus = "false";
        //displaying the hash content
        Set set2 = finalMap.entrySet();
        Iterator iterator2 = set2.iterator();
    while(iterator2.hasNext()){
    Map.Entry mentry2 = (Map.Entry)iterator2.next();
    System.out.print("key is: "+mentry2.getKey() + " value is ");
    System.out.println(mentry2.getValue());
    
    System.out.println(scenarioList.size()+"===ada\n\n");
    Iterator iterator;
    iterator = scenarioList.iterator();

    // displaying the Tree set data
    System.out.println("Tree set data in ascending order: ");     
    while (iterator.hasNext()) {
       System.out.println(((Scenario) iterator.next()).getTechnicalDifficultes() + " ===\n\n");
       
    }
    
    }
        return finalMap;
    }
}
