package com.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ParsePOM {

	public static void main(String[] args) {

		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			List<String> dependencies = new ArrayList<String>();
			String serverURL = null;
			String currentDirectory = System.getProperty("user.dir");
			fw = new FileWriter(currentDirectory + File.separator + "version.properties");
			bw = new BufferedWriter(fw);
			File pomFile = new File(currentDirectory + File.separator + "pom.xml");
			String value = null;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder;
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc;
			doc = dBuilder.parse(pomFile);
			doc.getDocumentElement().normalize();
			NodeList server = doc.getElementsByTagName("repo");
			if (((Element) server.item(0) != null)) {

				if (((Element) server.item(0)).getElementsByTagName("url").item(0) != null) {
					serverURL = ((Element) server.item(0)).getElementsByTagName("url").item(0).getTextContent();
				}
			}

			if (serverURL != null) {
				NodeList listOfDependency = doc.getElementsByTagName("dependency");
				for (int temp = 0; temp < listOfDependency.getLength(); temp++) {
					Node nNode = listOfDependency.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						if (stringNotNull(eElement) && stringNotEmpty(eElement) && serverURL != null) {

							value = serverURL + "/"
									+ (eElement.getElementsByTagName("groupId").item(0).getTextContent()).replace(".",
											"/")
									+ "/" + eElement.getElementsByTagName("artifactId").item(0).getTextContent() + "/"
									+ eElement.getElementsByTagName("version").item(0).getTextContent() + "/"
									+ eElement.getElementsByTagName("artifactId").item(0).getTextContent() + "-"
									+ eElement.getElementsByTagName("version").item(0).getTextContent() + "."
									+ eElement.getElementsByTagName("type").item(0).getTextContent();
							dependencies.add(value);
						} else {
							dependencies.clear();
							break;
						}
					}
				}
				if (!dependencies.isEmpty()) {
					for (String str : dependencies) {
						if (str != null && !str.isEmpty()) {
							bw.append(str);
							bw.newLine();
						}
					}
				}
			}

		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null) {
					bw.close();
				}
				if (fw != null) {
					fw.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	private static boolean stringNotEmpty(Element eElement) {
		if (checkifTagAvailable(eElement)) {
			return ifNotEmpty(eElement.getElementsByTagName("groupId").item(0).getTextContent())
					&& ifNotEmpty(eElement.getElementsByTagName("artifactId").item(0).getTextContent())
					&& ifNotEmpty(eElement.getElementsByTagName("version").item(0).getTextContent());
		}
		return false;
	}

	private static boolean checkifTagAvailable(Element eElement) {
		return eElement.getElementsByTagName("groupId") != null && eElement.getElementsByTagName("artifactId") != null
				&& eElement.getElementsByTagName("version") != null;

	}

	private static boolean ifNotEmpty(String textContent) {
		return !textContent.isEmpty();
	}

	private static boolean stringNotNull(Element eElement) {
		if (checkifTagAvailable(eElement)) {
			return ifNotNull(eElement.getElementsByTagName("groupId").item(0))
					&& ifNotNull(eElement.getElementsByTagName("artifactId").item(0))
					&& ifNotNull(eElement.getElementsByTagName("version").item(0));
		}
		return false;
	}

	private static boolean ifNotNull(Node node) {
		return node != null;
	}

}
