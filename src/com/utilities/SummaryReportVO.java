package com.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SummaryReportVO {


	private List<String> brands,lt;	
	private List<List<String>> ltVal;
	private HashMap<String,TestCaseCount> testCaseCount;
	private String brandName;
	private String env;
	
	

	
	
	/**
	 * @ return List
	 */
	public List<String> getListObject(){
		lt = new ArrayList<String>();
		return lt;
	}
	
	/**
	 * @ return List
	 */
	public List<List<String>> getListOfListTypeObject(){
		ltVal = new ArrayList<List<String>>();
		return ltVal;
	}

	/**
	 * @return the brands
	 */
	public List<String> getBrands() {
		List<String> brands = new ArrayList<String>();
		brands.add("Pkg Holidays- (TH, FC, Falcon,CA and A&C)");
		brands.add("Cruise (with CA and A&C)");
		brands.add("FO (with CA and A&C)");
		brands.add("Hybris Retail");
		return brands;
	}




	




	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}




	/**
	 * @param brandName the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}




	/**
	 * @param brands the brands to set
	 */
	public void setBrands(List<String> brands) {
		this.brands = brands;
	}




	/**
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}




	/**
	 * @param env the env to set
	 */
	public void setEnv(String env) {
		this.env = env;
	}









	/**
	 * @return the testCaseCount
	 */
	public HashMap<String, TestCaseCount> getTestCaseCount() {
		return testCaseCount;
	}









	/**
	 * @param testCaseCount the testCaseCount to set
	 */
	public void setTestCaseCount(HashMap<String, TestCaseCount> testCaseCount) {
		this.testCaseCount = testCaseCount;
	}









	










	

	

}
