package com.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class GridMasterSummaryReport {

	public String filename, htmlContent;

	/**
	 * The method is used to extract the file name
	 * 
	 * @return string file name
	 * @author Anant Patil
	 */
	public String getFileName() {
		return filename;
	}

	/**
	 * Method is used to set the file name return void
	 * 
	 * @author Anant Patil
	 */
	public void setFileName(String filename) {
		this.filename = filename;
	}

	/**
	 * The following method is used to create the xml file It returns the void
	 * 
	 * @author Anant Patil
	 * @param filename
	 */
	public void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("MasterSummaryResult");
			doc.appendChild(root);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(getFileName()));
			trans.transform(src, result);
			System.out.println("File created!");
		} catch (ParserConfigurationException e1) {
			System.out.println("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			System.out.println("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			System.out.println("TransfomerException:" + e3);
		}
	}

	public void addSummary(String Envt, String TH, String FC, String FL, String CR, String FO, String FLFO,
			String color) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(getFileName());
			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element status = doc1.createElement("Status");
				root1.appendChild(status);

				Element tot = doc1.createElement("Envt");
				tot.appendChild(doc1.createTextNode(String.valueOf(Envt)));
				status.appendChild(tot);

				Element pass = doc1.createElement("TH");
				pass.appendChild(doc1.createTextNode(String.valueOf(TH)));
				status.appendChild(pass);

				Element fC = doc1.createElement("FC");
				pass.appendChild(doc1.createTextNode(String.valueOf(FC)));
				status.appendChild(fC);

				Element fL = doc1.createElement("FL");
				pass.appendChild(doc1.createTextNode(String.valueOf(FL)));
				status.appendChild(fL);

				Element cR = doc1.createElement("CR");
				pass.appendChild(doc1.createTextNode(String.valueOf(CR)));
				status.appendChild(cR);

				Element fO = doc1.createElement("FO");
				pass.appendChild(doc1.createTextNode(String.valueOf(FO)));
				status.appendChild(fO);

				Element fLFo = doc1.createElement("FLFO");
				pass.appendChild(doc1.createTextNode(String.valueOf(FLFO)));
				status.appendChild(fLFo);

				Element col = doc1.createElement("color");
				pass.appendChild(doc1.createTextNode(String.valueOf(color)));
				status.appendChild(col);

				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(getFileName()));
				trans.transform(src, result);
				System.out.println("file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			System.out.println("The Sax error:" + e1);
		} catch (IOException e2) {
			System.out.println("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			System.out.println("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			System.out.println("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	public static void transormXmlToHtml(String path, String rsltHtmlFile, String xslFile) {
		try {
			System.out.println("the file:" + xslFile);
			File dltHtmlFile = new File(rsltHtmlFile + ".html");
			if (dltHtmlFile.exists()) {
				if (dltHtmlFile.getName().contains(".html")) {
					dltHtmlFile.delete();
				}
			}
			TransformerFactory tfactory = TransformerFactory.newInstance();
			StreamSource xmlfile = new StreamSource(path + ".xml");
			StreamSource xslfile = new StreamSource(xslFile);
			FileOutputStream htmlfile = new FileOutputStream(rsltHtmlFile + ".html");
			Transformer trans = tfactory.newTransformer(xslfile);
			trans.transform(xmlfile, new StreamResult(htmlfile));

		} catch (TransformerConfigurationException e1) {
			System.out.println("The TransformerConfigurationException:" + e1);
		} catch (FileNotFoundException e2) {
			System.out.println("The FileNotFoundException:" + e2);
		} catch (TransformerException e3) {
			System.out.println("The TransformerException:" + e3);
		}
	}

	/**
	 * @author anant Patil
	 * @param args
	 * @return
	 * @throws ParseException
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public static Map<String, Map<String, ArrayList<String>>> reportForamtion() throws ParseException, IOException {
		// ============================================================================
		String rptFlderPath, finalResultFldPath, prjPath;
		Map<String, Map<String, ArrayList<String>>> tableStructure = new TreeMap<>();
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();

		rptFlderPath = prjPath + "//reports//";

		finalResultFldPath = prjPath + "//reports//";

		// GridMasterSummaryReport report = new GridMasterSummaryReport();
		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();

		File readFlder = new File(rptFlderPath);
		
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				String lowercaseName = name.toLowerCase();
				if (lowercaseName.startsWith(".")) {
					return false;
				} else {
					return true;
				}
			}
		};
		
		File[] listOfFolder = readFlder.listFiles(filter);

		final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		
		Arrays.sort(listOfFolder, new Comparator<File>() {
			public int compare(File f1, File f2) {
				try {
					if(f1.isDirectory() && f2.isDirectory()) {
						System.out.println("Date Value of Folder : " + dateFormatter.parse(f1.getName()));
					return -Integer.valueOf(dateFormatter.parse(f1.getName()).compareTo(dateFormatter.parse(f2.getName())));
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});

		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);					
				}
			}
			break;
		}
		String environment;
		String brand;
		String url;
		
		boolean valueExists = false;
		for (File directory : datFolder) {
			File[] timeFolders = directory.listFiles();
			Arrays.sort(timeFolders, new Comparator<File>() {
				public int compare(File f1, File f2) {
					if(f1.isDirectory() && f2.isDirectory())
					{
						System.out.println("Integer Value Of Time : " + Integer.valueOf(f1.getName()));
						return -Integer.valueOf(Integer.valueOf(f1.getName())).compareTo(Integer.valueOf(f2.getName()));
					}
					return 0;
				}
			});
			for (File file : timeFolders) {
				if (file.isDirectory()) {
					for (File reportFile : file.listFiles()) {
						if (reportFile.getName().contains("AutomationSummary")) {
							valueExists = false;
							String[] nameEach = reportFile.getName().split("_");
							environment = nameEach[0];
							brand = nameEach[1];
							url = finalResultFldPath + directory.getName() + "//" + file.getName() + "//"
									+ reportFile.getName();
							for (String existingEnvironment : tableStructure.keySet()) {
								if(existingEnvironment.equalsIgnoreCase(environment))
								{
									if (null != tableStructure.get(existingEnvironment)) {
										if (tableStructure.get(environment).containsKey(brand)) {
											valueExists = true;
											break;
										}
									}
							   }
							}
							if (!valueExists) {
								if (null == tableStructure.get(environment)) {
									Map<String, ArrayList<String>> brandUrl = new TreeMap<String, ArrayList<String>>();
									ArrayList<String> list=new ArrayList<String>();
									list.add(url);
									brandUrl.put(brand, list);
									tableStructure.put(environment, brandUrl);
							}
						}
							else{
							
								tableStructure.get(environment).get(brand).add(url);
							}
					}
				}
			}
		}

		if (datFolder.size() != 0)

		{
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);

			dateValue = filPath.getAbsolutePath();

			try {
				strdDate = fmt.parse(filPath.getName().trim());

				diff = sysDate.getTime() - strdDate.getTime();

				diffDays = diff / (24 * 60 * 60 * 1000);

				small = diffDays;

			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();

				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);

					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();

					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);

			long lastMod = Long.MIN_VALUE;

			File choice = null;
			String curPath = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();

				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
							curPath = choice.toString();
						}
					}
				}
			}

			File sumReports = new File(curPath);
			String[] files = sumReports.list();
			List<String> filesContainingSubstring = new ArrayList<String>();

			for (String fileName : files) {
				if (fileName.contains("AutomationSummaryReport")) {
					filesContainingSubstring.add(fileName);

				}
			}

			// ======================================================================================

		}
		for (Entry<String, Map<String, ArrayList<String>>> entry : tableStructure.entrySet()) {
			for (Entry<String, ArrayList<String>> entry2 : entry.getValue().entrySet()) {
				System.out.println(
						"Env : " + entry.getKey() + " Brand : " + entry2.getKey() + " URL : " + entry2.getValue());
				
			}
		}
		}
		return tableStructure;
	}
	
}
