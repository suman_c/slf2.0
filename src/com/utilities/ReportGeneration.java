package com.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class ReportGeneration {

	public String filename;

	/**
	 * The method is used to extract the file name
	 *
	 * @return string file name
	 * @author Anant Patil
	 */
	public String getFileName() {
		return filename;
	}

	/**
	 * Method is used to set the file name return void
	 *
	 * @author Anant Patil
	 */
	public void setFileName(String filename) {
		this.filename = filename;
	}

	/**
	 * The following method is used to create the xml file It returns the void
	 *
	 * @param filename
	 * @author Anant Patil
	 */
	public void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("SummaryResult");
			doc.appendChild(root);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(getFileName()));
			trans.transform(src, result);
			System.out.println("File created!");
		} catch (ParserConfigurationException e1) {
			System.out.println("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			System.out.println("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			System.out.println("TransfomerException:" + e3);
		}
	}

	/**
	 * The following method is used to add the node to xml file It returns void
	 *
	 * @param ScenarioName
	 * @param Passed
	 * @param Failed
	 * @param percentagePass
	 * @param perFailed
	 * @param defects
	 * @author Anant Patil
	 */
	public void addSummary(float Total, float Passed, float Failed, float percentagePass, float perFailed) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(getFileName());
			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element status = doc1.createElement("Status");
				root1.appendChild(status);

				Element tot = doc1.createElement("Total");
				tot.appendChild(doc1.createTextNode(String.valueOf(Total)));
				status.appendChild(tot);

				Element pass = doc1.createElement("Passed");
				pass.appendChild(doc1.createTextNode(String.valueOf(Passed)));
				status.appendChild(pass);

				Element fail = doc1.createElement("Failed");
				fail.appendChild(doc1.createTextNode(String.valueOf(Failed)));
				status.appendChild(fail);

				Element perpass = doc1.createElement("PercentagePassed");
				perpass.appendChild(doc1.createTextNode(String.valueOf(percentagePass)));
				status.appendChild(perpass);

				Element perfail = doc1.createElement("PercentageFailed");
				perfail.appendChild(doc1.createTextNode(String.valueOf(perFailed)));
				status.appendChild(perfail);

				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(getFileName()));
				trans.transform(src, result);
				System.out.println("file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			System.out.println("The Sax error:" + e1);
		} catch (IOException e2) {
			System.out.println("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			System.out.println("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			System.out.println("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The following method is used to add the scnario and status details It
	 * returns void
	 *
	 * @param scenStatus
	 * @author Anant Patil
	 */
	public void addDetails(List<String[]> scenStatus) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File fin = new File(getFileName());
			if (fin.exists()) {
				Document doc = builder.parse(fin);
				Element root1 = doc.getDocumentElement();
				Element details = doc.createElement("Scenarios");
				root1.appendChild(details);
				for (int i = 0; i < scenStatus.size(); i++) {
					Element snSt = doc.createElement("ScenDescStatus");
					details.appendChild(snSt);

					String[] arr = scenStatus.get(i);
					Element secn = doc.createElement("ScenarioName");
					secn.appendChild(doc.createTextNode(arr[0]));
					snSt.appendChild(secn);

					Element sctatus = doc.createElement("ScenStatus");
					sctatus.appendChild(doc.createTextNode(arr[1]));
					snSt.appendChild(sctatus);

				}
				TransformerFactory tfactory = TransformerFactory.newInstance();
				Transformer trans = tfactory.newTransformer();
				DOMSource src = new DOMSource(doc);
				StreamResult result = new StreamResult(fin);
				trans.transform(src, result);
			}
		} catch (ParserConfigurationException e) {
			System.out.println("ParseConfiguration Error:" + e);
		} catch (SAXException e1) {
			System.out.println("SAX error:" + e1);
		} catch (IOException e2) {
			System.out.println("The I/O error:" + e2);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e3) {
			System.out.println("Transformer Exception:" + e3);
		}
	}

	/**
	 * The method is used to convert the XML to HTML It returns void
	 *
	 * @param path
	 * @param xmlPath
	 * @param xslFile
	 * @author Anant
	 */
	public static void transormXmlToHtml(String path, String rsltHtmlFile, String xslFile) {
		try {
			File dltHtmlFile = new File(rsltHtmlFile + ".html");
			if (dltHtmlFile.exists()) {
				if (dltHtmlFile.getName().contains(".html")) {
					dltHtmlFile.delete();
				}
			}
			TransformerFactory tfactory = TransformerFactory.newInstance();
			StreamSource xmlfile = new StreamSource(path + ".xml");
			StreamSource xslfile = new StreamSource(xslFile);
			FileOutputStream htmlfile = new FileOutputStream(rsltHtmlFile + ".html");
			Transformer trans = tfactory.newTransformer(xslfile);
			trans.transform(xmlfile, new StreamResult(htmlfile));

		} catch (TransformerConfigurationException e1) {
			System.out.println("The TransformerConfigurationException:" + e1);
		} catch (FileNotFoundException e2) {
			System.out.println("The FileNotFoundException:" + e2);
		} catch (TransformerException e3) {
			System.out.println("The TransformerException:" + e3);
		}
	}

	/**
	 * @param args
	 * @throws ParseException
	 * @throws IOException
	 * @author anant Patil
	 */
	@SuppressWarnings("static-access")
	public static void reportForamtion() throws ParseException, IOException {

		String rptFlderPath, finalResultFldPath, prjPath;
		boolean isDirectryFlag;
		List<File> datVal = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();
		System.out.println(prjPath);

		rptFlderPath = prjPath + "//reports//";
		finalResultFldPath = prjPath + "//reports//";

		ReportGeneration report = new ReportGeneration();
		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();

		// Presentation ppt = new Presentation();

		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datVal.add(lstFlder);
				}
			}
		}

		if (datVal.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datVal.get(0);
			dateValue = filPath.getAbsolutePath();
			try {
				strdDate = fmt.parse(filPath.getName().trim());
				diff = sysDate.getTime() - strdDate.getTime();
				diffDays = diff / (24 * 60 * 60 * 1000);
				small = diffDays;
			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datVal.size(); j++) {
				dtVal = datVal.get(j).getName();
				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);
					if (diffDays < small) {
						small = diffDays;
						dateValue = datVal.get(j).getAbsolutePath();
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			// fmt.applyPattern("a");
			// String amPmVal = fmt.format(sysDate);
			// List<File> subFolderPath = new ArrayList<File>();

			File amPamFilVal = new File(dateValue);
			long lastMod = Long.MIN_VALUE;
			File choice = null;
			if (amPamFilVal.exists()) {
				File[] subFolder = amPamFilVal.listFiles();
				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
						}
					}
				}

				String line, scenarioName = null, flagStatus;
				float total, passed = 0, failed = 0, perPass, perFail;
				int totCt = 0;
				boolean errStatus;
				List<String[]> finalList = new ArrayList<String[]>();
				if (choice != null) {
					String[] htmlFlName = choice.list();
					total = (float) htmlFlName.length;
					// System.out.println("total:"+total+"\t"+choice);
					if (total != 1) {
						for (String htFlName : htmlFlName) {
							if (!htFlName.contains("images")) {
								totCt++;
								flagStatus = "PASS";
								errStatus = true;
								File htmlFileName = new File(choice + "\\" + htFlName);
								BufferedReader reader;
								try {
									reader = new BufferedReader(new FileReader(htmlFileName));
									scenarioName = htmlFileName.getName().replace(".html", "");
									if (htmlFileName.length() != 0) {
										while ((line = reader.readLine()) != null) {
											if (line.contains("ScenarioName:")) {
												scenarioName = line.replace("<td class='step-details' colspan='2'>", "")
														.replace("</td>", "").replace("ScenarioName", "")
														.replace(":", "").trim();
											}
											if (line.contains("title='FAIL'")) {
												String[] arr = new String[2];
												flagStatus = "FAIL";
												errStatus = false;
												arr[0] = scenarioName;
												arr[1] = flagStatus;
												finalList.add(arr);
												failed++;
												break;
											}
										}

										if (errStatus == true) {
											String[] arr = new String[2];
											flagStatus = "PASS";
											arr[0] = scenarioName;
											arr[1] = flagStatus;
											finalList.add(arr);
											passed++;
										}
									} else {
										System.out.println("The html file doesn't have the content:");
									}

								} catch (FileNotFoundException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
						// System.out.println(passed+"\t"+totCt);
						perPass = (passed) / (totCt) * 100;
						perFail = (failed) / (total) * 100;

						report.setFileName("src\\AutomationSummaryReport.xml");
						report.createXmlFile();
						report.addSummary(totCt, passed, failed, perPass, perFail);
						report.addDetails(finalList);
						report.transormXmlToHtml("src\\" + "AutomationSummaryReport",
								finalResultFldPath + "AutomationSummaryReport", "src\\reportInterface.xsl");

					}

				}
			}

		}

	}

}
