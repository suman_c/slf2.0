package com.utilities;

public class Scenario {
	
	String brand;
	String bookingId;
	String fStatus;
	String technicalDifficultes;
	String technicalDifficultesMessage;
	String env;
	String dateOfExecution;
	String fileName;
	public String getScenarioNameReg() {
		return scenarioNameReg;
	}
	public void setScenarioNameReg(String scenarioNameReg) {
		this.scenarioNameReg = scenarioNameReg;
	}
	String scenarioNameReg;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}


	public String getStatus() {
		return fStatus;
	}
	public void setStatus(String fStatus) {
		this.fStatus = fStatus;
	}
	public String getTechnicalDifficultes() {
		return technicalDifficultes;
	}
	public void setTechnicalDifficultes(String technicalDifficultes) {
		this.technicalDifficultes = technicalDifficultes;
	}
	public String getTechnicalDifficultesMessage() {
		return technicalDifficultesMessage;
	}
	public void setTechnicalDifficultesMessage(String technicalDifficultesMessage) {
		this.technicalDifficultesMessage = technicalDifficultesMessage;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public String getDateOfExecution() {
		return dateOfExecution;
	}
	public void setDateOfExecution(String dateOfExecution) {
		this.dateOfExecution = dateOfExecution;
	}
	public String getfileName() {
		return fileName;
	}
	public void setfileName(String fileName) {
		this.fileName = fileName;
	}



}
