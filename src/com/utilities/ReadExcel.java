package com.utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

//import org.apache.poi.hssf.usermodel.HSSFSheet;
//import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.ss.usermodel.Cell;
//import jxl.Cell;

public class ReadExcel {

	Workbook wbWorkbook;

	Sheet shSheet;

	public void openSheet(String filePath, String shtName) {
		FileInputStream fs;

		try {
			WorkbookSettings ws = new WorkbookSettings();
			ws.setSuppressWarnings(true);
			fs = new FileInputStream(filePath);
			wbWorkbook = Workbook.getWorkbook(fs, ws);
			shSheet = wbWorkbook.getSheet(shtName);

			/*
			 * fs1 = new FileOutputStream(filePath);
			 * 
			 * wbWorkbook1= Workbook.getWorkbook(fs1);
			 */

			/*
			 * Workbook workbook = Workbook.getWorkbook(new
			 * File(""D:\\0077\\my2.xls"")); WritableWorkbook copy =
			 * Workbook.createWorkbook(new File("output.xls"), workbook);
			 * WritableSheet sheet2 = copy.getSheet(1); Label label = new
			 * Label(5,2,"ssssssssss"); sheet2.addCell(label);
			 */

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public void UpdateExcel(String filePath,int setrow,String updateVal)
	 * throws IOException { FileInputStream fsIP= new FileInputStream(new
	 * File(filePath)); //Read the spreadsheet that needs to be updated
	 * 
	 * HSSFWorkbook wb = new HSSFWorkbook(fsIP); //Access the workbook
	 * 
	 * HSSFSheet worksheet = wb.getSheetAt(0); //Access the worksheet, so that
	 * we can update / modify it.
	 * 
	 * 
	 * Cell cell = null; // declare a Cell object
	 * 
	 * cell = worksheet.getRow(setrow+1).getCell(2); // Access the second cell
	 * in second row to update the value
	 * 
	 * cell.setCellValue(updateVal); // Get current cell value value and
	 * overwrite the value
	 * 
	 * fsIP.close(); //Close the InputStream
	 * 
	 * FileOutputStream output_file =new FileOutputStream(new File(filePath));
	 * //Open FileOutputStream to write updates
	 * 
	 * wb.write(output_file); //write changes
	 * 
	 * output_file.close(); //close the stream fsIP.close(); }
	 */
	public String getValueFromCell(int iColNumber, int iRowNumber) {
		return shSheet.getCell(iColNumber, iRowNumber).getContents();
	}

	public int getRowCount() {
		return shSheet.getRows();
	}

	public int getColumnCount() {
		return shSheet.getColumns();
	}
}
