package com.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class ORMergingAndShowingConflict {

	Logger logger = Logger.getLogger(this.getClass().toString());

	/**
	 * The method is used to verify the string empty or not
	 *
	 * @param str
	 * @return array
	 */
	public String[] checkEmptyString(String[] str) {
		String[] arr = new String[4];
		if (str.length >= 5) {
			if (str[4].length() > 1) {
				arr[0] = str[0];
				arr[1] = str[1];
				arr[2] = str[2];
				arr[3] = str[3];
			} else {
				arr = str;
			}
		} else {
			arr = str;
		}
		return arr;
	}

	/**
	 * The method is used to copy from one array to other and adding one element
	 * by adding one extra index
	 *
	 * @param str
	 * @param flName
	 * @return
	 */
	public String[] copyArrayandAddIndex(String[] str, String flName) {
		String[] arr = new String[5];
		if (str.length >= 4) {
			arr[0] = str[0];
			arr[1] = str[1];
			arr[2] = str[2];
			arr[3] = str[3];
			arr[4] = flName;
		}
		return arr;
	}

	/**
	 * The method is used to read all the OR file and will create the updated OR
	 * file with merged all OR name and its conflicts
	 *
	 * @param filePathToRead
	 * @param filePathToWrite
	 * @throws IOException
	 * @author Anant
	 */
	public void mergeMethod(String[] filePathToRead, String filePathToWrite) throws IOException {
		String[] line1;
		List<String[]> lstVal = new ArrayList<String[]>();
		CSVReader reader1 = null, reader2 = null;
		int index1 = 0, index2 = 0;
		CSVWriter writer = null;
		boolean flag;
		String parentFilName = null;
		File filName1 = null;
		String[] labelNames = { "Object_Name", "Object_Type", "Object_Locator_Type", "Object_Locator_Value",
				"FileName" };
		ORMergingAndShowingConflict merge = new ORMergingAndShowingConflict();

		writer = new CSVWriter(new FileWriter(filePathToWrite));
		writer.writeNext(labelNames);
		for (int f = 0; f < filePathToRead.length; f++) {
			++index2;
			filName1 = new File(filePathToRead[f]);
			if (filName1.getName().endsWith(".csv") && filName1.exists()) {
				parentFilName = filName1.getName();
				reader1 = new CSVReader(new FileReader(filName1));
				reader1.readNext();
				while ((line1 = reader1.readNext()) != null) {
					String[] arr = merge.checkEmptyString(line1);
					lstVal.add(arr);
				}
				break;
			} else {
				logger.info("The file specified in the array doesn't seems csv file: " + filName1);
			}
		}

		for (int i = index2; i < filePathToRead.length; i++) // Loop to read the
																// remaining OR
																// file from
																// array
		{
			try {
				File filNames = new File(filePathToRead[i]);
				if (filePathToRead[i].endsWith(".csv") && filNames.exists()) {
					reader2 = new CSVReader(new FileReader(filNames));
					reader2.readNext();
					while ((line1 = reader2.readNext()) != null) {
						flag = false;
						for (int j = 0; j < lstVal.size(); j++) {
							String[] str = lstVal.get(j);
							if (str[0].trim().equals(line1[index1].trim())) {
								if (!str[3].trim().equals(line1[3].trim())) {
									String dupORFilName = filNames.getName();
									String[] str1 = merge.copyArrayandAddIndex(str, parentFilName);
									lstVal.set(j, str1);
									String[] str2 = merge.copyArrayandAddIndex(line1, dupORFilName);
									lstVal.add(j + 1, str2);
									flag = true;
									break;
								}
								flag = true;
								break;
							}
						}

						if (flag == false) {
							String[] retArr = merge.checkEmptyString(line1);
							lstVal.add(retArr);
						}
					}
					reader2.close();
				} else {
					logger.info(
							"The gieven file doesn't seems csv file/it doesnt exist in the local drive: " + filName1);
				}
			} catch (FileNotFoundException e) {
				logger.error("The file not found execption: " + e.getMessage());
			} catch (Exception e) {
				logger.error("The execption occuerd while fetching the data from csv file: " + e);
			} finally {
				if (reader2 != null)
					reader2.close();
			}
		} // Main for loop ends here

		if (lstVal.size() != 0) {
			Iterator<String[]> it = lstVal.iterator();
			while (it.hasNext()) {
				writer.writeNext(it.next());
			}
			logger.info("========The new file created by copying all the rows from different csv file========");
		}
		reader1.close();
		writer.close();

	}
}