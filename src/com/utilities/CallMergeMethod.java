package com.utilities;

import java.io.File;
import java.io.IOException;

public class CallMergeMethod {

	public static void main(String[] args) throws IOException {

		File fi = new File("");
		String path = fi.getAbsolutePath();
		ORMergingAndShowingConflict conflct = new ORMergingAndShowingConflict();

		String[] filePathToRead = { path + "/src/com/objectrepository/Object_Repo_St3_Anant.csv",
				path + "/src/com/objectrepository/FC_BookflowObjects_Samson.csv" };

		String filePathToWrite = path + "/src/com/objectrepository/Object_Repo_ST3_v1.csv";
		String filePathToWrite1 = path + "/src/com/objectrepository/Object_Repo_ST3_v2.csv";
		ObjectRepositoryMerging.mergeMethod(filePathToRead, filePathToWrite1);
		conflct.mergeMethod(filePathToRead, filePathToWrite);

	}

}
