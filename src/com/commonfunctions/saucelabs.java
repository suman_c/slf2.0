package com.commonfunctions;
 
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
 
import io.appium.java_client.ios.IOSDriver;
 
import java.net.URL;
 
public class saucelabs {
 
//  public static final String USERNAME = "tui_master";
  //public static final String ACCESS_KEY = "B27B6A3D90044A9BBFAD76E16F276EA4";
//  public static final String URL = "https://"  + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
 
  public static void main(String[] args) throws Exception {
 
    DesiredCapabilities caps = new DesiredCapabilities();
    caps.setCapability("testobjectApiKey","B27B6A3D90044A9BBFAD76E16F276EA4");
    
    caps.setCapability("platformName", "iOS");
    caps.setCapability("platformVersion", "10.3");
    
 //   caps.setCapability("platform", "Android");
  //  caps.setCapability("version", "7");
 caps.setCapability("privateDevicesOnly   ", true);
 
 caps.setCapability("testobject_app_id", "1");
 caps.setCapability("name", "My Test 1!");
 
 IOSDriver driver = new IOSDriver(new URL("https://eu1.appium.testobject.com/wd/hub"), caps);
 
 
    /**
     * Goes to Sauce Lab's guinea-pig page and prints title
     */
 
    driver.get("https://www.tui.co.uk");
   
    System.out.println("title of page is: " + driver.getTitle());
 
    driver.quit();
  }
}
