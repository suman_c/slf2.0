﻿package com.summaryreportgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.utilities.WebBAUCalculatePercentagePass;
import com.utilities.WebBAUCalculatePercentagePassAnalysis;
import com.utilities.ReportGenerationNew;
import com.utilities.WebBAUGridGenerator;

public class WebBAUMasterSummaryReport__SmokeAnalysis {

	public static WebBAUCalculatePercentagePassAnalysis WebBAPercentagePassAnalysis = new WebBAUCalculatePercentagePassAnalysis();
	public static String finalStringdatereport;
	public static Map<String, List<String>> finalMap = new HashMap<String, List<String>>();
	public static File fin1;
	public static FileWriter writer1;

	public static void Storedetails() {
		Date date1 = new Date();
		SimpleDateFormat newFormat = new SimpleDateFormat("ddMMMyyyy");
		SimpleDateFormat newFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
		finalStringdatereport = newFormat1.format(date1);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) throws ParseException, IOException {
		Storedetails();
		ReportGenerationNew pas = new ReportGenerationNew();

		String rptFlderPath, finalResultFldPath, prjPath;
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();

		// rptFlderPath = prjPath + "//reports//";
		rptFlderPath = prjPath + "\\Tomcat\\webapps\\Reports\\execution_reports";
		System.out.println("Report Folder Path : " + rptFlderPath);

		finalResultFldPath = prjPath + "\\Tomcat\\webapps\\Reports\\Smoke_Report\\";

		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();
		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		for (File lstFlder : listOfFolder) {

			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);

				}
			}
		}

		if (datFolder.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);
			System.out.println(filPath);
			dateValue = filPath.getAbsolutePath();

			try {
				strdDate = fmt.parse(filPath.getName().trim());

				diff = sysDate.getTime() - strdDate.getTime();

				diffDays = diff / (24 * 60 * 60 * 1000);

				small = diffDays;

			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();

				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);

					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();

					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);

			long lastMod = Long.MIN_VALUE;

			File choice = null;
			String curPath = null;
			String failPath = null;
			String buildDetails = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();

				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
							curPath = choice.toString();
							failPath = choice.toString();

						}
					}
				}
			}

			// choice = path for the latest folder
			// ================================================================================
			curPath = curPath.replace("\\", "#");
			String[] exeDate = curPath.split("#");

			String lastDate = exeDate[exeDate.length - 2];
			String exeTime = exeDate[exeDate.length - 1];
			// System.out.println(lastDate+"====djksgd\n\n");
			// String executionDate = null;

			// Kirti's codes for new master summary report

			Map<String, String> allMap = WebBAUGridGenerator.reportForamtion();

			Map<String, String> eMap = WebBAUCalculatePercentagePassAnalysis.percentCalculation();
			List<String> teamAssign = new ArrayList<String>();

			String buildNum, brandEnvt, env, brand, bran, envPrin, branStat, passPercen, failBrand, oriFPath,
					finalPath = null, failTeamAssign = null, failTeamAssign1 = null, passBran, enBran, pass,
					passPath = null;
			Float fsta;
			int failB = 0;

			// Path p1 =
			// Paths.get("C:\\Users\\kirti.vm\\Documents\\apache-jmeter-2.11\\bin\\AnCTestdata");

			File fin = new File(prjPath + "\\Tomcat\\webapps\\Reports\\Smoke_Report\\Smoke_Report.html");

			FileWriter writer = new FileWriter(fin);

			writer.write("<html lang=\"en-US\"><head><meta charset=\"UTF-8\">");
			writer.write("<title>TUI Automation Report</title>");
			writer.write(
					" <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"></head>");
			writer.write(
					"<body style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"structure\"><header style=\"background: #70cbf4;padding: 24px 48px;padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"logo-container\">");
			writer.write(
					"<div class=\"logo\" style=\"padding: 25px;display: inline-block;vertical-align: middle;\"><img style=\"width: 80px;\" src=\"https://www.tui.co.uk/static-images/_ui/mobile/th/images/logo/TUI-Logo.svg\"></div>");
			writer.write(
					"<div class=\"center-title\" style=\"display: inline-block;vertical-align: middle; width: calc(100% - 220px);text-align: center;\"><h1 style=\"font-size: 20px;color: #092a5e;\">Smoke Summary Reports</h1> </div>");
			writer.write(
					"<div class=\"logo\" style=\"display: inline-block;vertical-align: middle;\"><img style=\"width: 70px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAC7CAMAAABIKDvmAAAA21BMVEX/////AAAAAABWVlb/UVGRkZGNjY3/gYF2dnb/kZF5eXlaWlr/cHDe3t6cnJzBwcHIyMigoKD5+flpaWn/vLz/1dX/Z2e0tLT/ysqnp6fs7Ozm5uaAgIDz8/MoKCg4ODhDQ0NKSkr/+PgbGxv/nJz/qKj/7e3S0tL/sbH/4eH/GBgPDw//5eX/MDD/Xl7/Rkb/k5P/xsb/iYn/JSX/Pj7/bW3/g4NkZGT/sLAtLS3/KioWFhb/YWH/EhL/eHj/ODjWAAD/Tk5senqFkpLHgoLkLi6/YmLegYHbYGCbnrD6AAAQUklEQVR4nO2daWMiuRGGm7bBjDkNxpgbmtvj+xjssY13kmyS//+LIql0lKTqBmc32U1G74cdo1ZLpUel0tENG0Wgo9zPrC+RrUAj0FAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaw9aXxsb2fz2e32Kru0R5Ftdn/98PtbevTx2+6/PtudZx8aH6tEX09Or1NK+j5roXIWp27lrfV63RJa+0VcXF7qq+vLNVH8Inq2Pq9Od+juwsr/HC1SIWjtQeOOp6+f5/NzgNKiyjkTKBZ38/l8tZbgZlaO7exUQ/3u3X90NJe3tWZHhBHXrDz8+T3aKZs5Szj87TRueBNacoRsRXv8Un7wlixudW+UpZvcO/m2kgfdTUf80hNt5zm7VEafH0UXzco33y+YclBfjv/98Hh2f+5V/o0lnNJFI+2iccUTV+bzgn30BvCKZ7I94XoBHf1u53yH5OicNIYxvaPNvHCdssw+fzMfJQ2tH+zT3Cma6kZHu2hc8i5wzLqxs7zzPMmjW7IYYFHiopM45m52rucouqXNnImbUGy+t/rIo5Hb2mB/iOsIH60dNITznjk3vFlZrrj3J3bIElpBiQ6mB1nRljDmNnWgwAhD7sfw4PnNo8HuwP53Kq5T0dnSDhrcwRI7yWnIe0I0GXQO3uEMFlXTjX/DkUNe6wluQeFmZscen8apNbAismc87aDh2MB1btPgw8SLllILqkdUTYmfP5WGmrvNNDGzY6JP4xumoVppT9K+smnc8KRL9w48tldZHngNZdqsdFWXXv40Gh/MLnGLCQUzu1SfxiMeKYtofpnSA5ayaTwRRdxcojyH4iY7kHhG2rGctUmuLLwZL40GXznBPXrUfYl++BXZ9Rh0rFc+IA6Xc5nag0aK9wqtiaGEBLDsyZdPr7KyL072NBq8hHvHzexoRNBAOmfeK+ZoeuVolE1DrDYyWvvmN9aW8E/buzgNGRVdp0qhwZIv5AjzRxcok8aDmFshCzH3Ie0RRVOXRKqGjEh9CwXgRbJYesnZN7J3gSk0LkUIgEmSmImMISlGzMWV7a6ey+2kASakLB3lZiErNIFzWbEcSgOncdyOpnEDqxAIySmr60waCdyVZPs51w4aHzI5IXeuR1moQAvPweEGtQizBjJN41TyXmS0OIvGVnrUynNTT7tW5sqlozviyAI8h1xkK8H6HNsp8Z1FvtvQNFQNM9JAUBaNluyMG2hHlrU797ALfclvNVxLWXqBZOBAw10507NfI0njVkWXB9+ZtDJofGgLYXS+k7lAO2lcoJMet91widpxaJXhVjR56KElVx1o4UDSWGgA0GTyCCyDxkqnQ1vdaR1r92nPhfGOaGGNuu9eS309Ri4xTeNCQda9RdF4M0umcoqL5jJpoMgrMqVN0lz7nASu0HU87B73oCGbjPbSJuyq0KHto2icozlL+CI5haXTuEdnXhDEUiZprr1OiR/RiWdiztNu9qCRy6DBFwIWYoLGBXYG6BZq059O4xI5AywWM47A9jwzP1wTWaRvZK/9vTx4SlalyjURQWOOz3hgaFIzeiqNQ6snsiZprr2fpzwZHqpz5GIkc06RNK5xgmmOCh0SAkGDDY7zRSIOlJKE2gSCUmmwwdFaJ7wA9p/1IruNn3m69KRnF7nYf7c6lpZsL1q8W52rQgdMoj6NbUSIqDCVBnV/+hHYp561yXW6HueJ9YnUjdedtqur0CFio0+DOeTq2xlMqldvR/O01XUaDbZiO78t/xDd93C9ncGSI/U51eeePB7JXA/Yhsyl/9brDWfgqwHYyhE0Ht0oAUs2/8FIGo2FkwibndTV8yefw0occgUjOzZrdTfzqncaqEPHiqCxctd24Gr+rJBC48zLmz5Jc2XTuKYfnajBIU/6siYVOCnGU7zb3W+q6nufht/CS7rZKTRa3i4NOjDlZD6bRouYLyzPT1K6ykjksJZ/3hSpQkf0Y+vQuPfLvpXgHNE0rvxhDKuCtG13Jo1zoqEikqo65Co1peyc6njLeN8WtbZL7h0al36IeLd6wynCTX0mQgTEqZQjsB00/Ai5xTTkYU7KA7KcZGcPU6JnzMbQonFIRWgYeu5xG00jcg/Xcjl5vpqyKsikMScW9WKFoLfV8vEiXbbqSTsQEjSuaRp3lNWwdVs5qSSNI2ppASalzIOZNL4QMeHJMkbOCGlTlhhJTuOpUatDB6ZxQXVtyqxA0liT62RwLvoILJPGGzHChKeZjZpcLNKvRtxQhpMxTG8LEY0ZfbSzIvxNRgO/dmLyh+UvHfkzaVz4PSvGBvYzeYTlv5+Sk5skt3vpiK5Cx5mVRG2BYMnhcKJonKbMHQnlR6Ds9cbCSxFDw1pgwHI9IXxaPNJ2A88D3eMqdBga5TSTFwR+gsa7Z7sU9B+518ymceq1veV7mfQOdw32wK1eeGfL5ESRU4tWRGOdtr2CJYcdR6G/rR6Z0QNFLTlII7JpwBhDMfKOGnLy7PPcmvZE64hn4hwwefrUsmnw+ETTkKEKFyJPWvCR51XqcJD7WmqS3bFPAebJXFT9/m1Bu9i73Nyuv8BK/v1MhLqW/xDuQlxIyKetiaHxUYYSW3PnZcur7excmTrfCquuDsu3iU46/OD+8PAGs9Ti+f7JcpiLs9s7lfnu26E7Rezatc3UhcVCjNdT+k3Q78+JyQcD+9TzgLsEnb/7RK41Db2TEzlxHsfaN7Ot1uJDaI0TcJPv7LzuymD3HvZpdakaepe1wz2ct9TpetJ6pg5Lr7dlI+JF3FmSJHAfyle2qD3hK+XtA/eWsi3eCdc4wTLSz/s5GlwPjzc35Bzq6P3hhmXM2uH/uRXeM8cKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAPLpbHz6z3/17oNvoHk+sY//2iD/lD9I9BAcmn8/ZdfD39W/frL3xwaB/Ffo59Vf4mLTkopLvwhlvwZVIyPnZRAAyvQwAo0sGgajW7XSRl0kdzsfopMbtDJforRYN9C3IzZuankTBrjGMvJ18y4VrFuzOv0OK6pP6tWlopTAH27V0haPUINfK2fUUFc2ovGQRaN4t40qsiGuvrzZG8aVe+aptFLqcen4fq7dWMz0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAINi8bk96GB36zWNP4Db1b/2zQ2e9Ho5KtKRa/FNX5xEw/5VddgZmVB35rXBDCNuik779MQF6fxxr7dozEwhVRfPCcSF1/jJi+k5hVybOpv70UDqe7REOrEYyqZ0SC/EIBoIDV8GkKjuEMX4jZNaOLREBrGbSo5jpdE6r40ajSNk1Qa5FcyaBqDQAMp0MAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaxAA+vPTcM95eT63ElgL/UkkLKhEcc9KncqDQopo3FCJX+KxjFB4yDvqxnHRGq+FA+rRHI/jgtU9jguEqmFOO5TuafxhkitxnGTyj2MS1TyK1lIiiVjj4b9EOVnk/t7X/2Dn1lUlAgKCgoKCgoKCgoKCgoKCgr639WgUqn0nB8w7i3rFfpXl38/9Xi9dlKjUq9XyJ9Y/i+pOoXzsGFfH8T25DvDr1VlGH8neqouf0W/f9yP5QFuwz1fg1eXN3HK4XjlWOabVDWRmnyReaIPyEeowHHHraJ3bM6aY32k3TVvEWvrhMZw17RUVf08wKWx9lW+os/yuDovPsCFuqGhT+vxr0Hz9A1JY6Orc387WheoBMwH6KXuycCn8WK/ps4x17TNy1ifiLfN2bi2wy2sQ9CIOce4uBxEjV6tqPJwoHmGvLHcaBzMkKF+QoB8g9U8leltIdZ/B/BX3dznw+Dd3GGjc1DpjBUNVtCwzYoatNkt04ZswGtbqVaBf5lVJ/DXoKEby19hl+ebTV0hWKcH3ij+Wq/Vam3+O+RtRaOky29Hhdg8v2gUxcOJWmzcr606jrWqrg+XEY1JPGS9gh5qNOwj6CFjQz0ZitGjqMpQVLFB3VhSvzc+MuNTK4/unaiWT+JX9ZjH/Fb5JJ5i6xgN+KOmhtXAdtyx33FT/BCmILMzGpw5RABDo8eHz9f4xdw9sGgseScwb3HrqMTeQwyWNDSfhtKIFBo60lRlPtYJHdlxFd3BrnWaRvQi/dmhMfJoLNHPwEc6ODEag65qlqEhuqmA8dk0xLcYmv5jtQr6fohU33KhmsS1i0ZFjo96/KqeTnb0Zdc6Q6Mp3cuhUfS+xZG3HbsIhXEavGARDAwNFu5EFxgANg3xYel/3YSHXGemGaIBLu4cQgOyabB8Y/5PgVU0AbNK+hbXOkNjSo8UHopH1Tp6gFqyHy53IOAIGspETaMO5MaoIRaNDqD86g9HPoVv2kvTfsZnhDOMoEzWgEIfpP3PolGEfC/MyiqMiTjVOk2jrlyTWfuyUWqamW5SkHPpxH64XIeJFWi0YUhqGhtoZjs2T4EtGmOglyeeKcvZdFiqgo908VQIRXcjalJ0aECM7vLc8JLAUvu2Z52k0eAT2kBZi2dYNvLM//eiJs3EHmvR4NGnYWh0ZTfgfsU0KjKe95yWQjP06mLSjZzhFumRzWjInjvQ49eiAb5eE9hFs/PKft86Vtj09VUsXXr67nFBS1pdyzeHCr/jGzWLxlLUoGicKLOOTRTANPrKsBL5MkNj2SkAkq64z5p5SorGjrjBcgy5AQVBsMj9cRLZ1vWVddrRdBgb0EtD1jlN8JWmHd+q4DKSBhipaDCCkzHXyBSKabD1rLg+eYnp9yy46kMxfbAefMHJcs22m0YeLOJDsc1GwkAvmX3r2EhZLpcdtNZJpSHCTU9EPmz5AVStaPT4/CtpLN1B59Co4ctWUy11ITaPrejdlXPFbhp8SdMFJ2TG9eqqMwnrZNw40WEjiwZ4VNdaBfXkoFM0eKblFIrgS3mpog5diAYbBW15fUO/VgOC/WDVmoersk920+CzeFsuQ1/jdkH1i21dG9HgBQwbFA38t4wYJfwl04ksSNPgMWkoimig2GhGvaHRQzVVnBhZR28idWG510AbAvEN4MaeNJrxcCNB9uPiSM6dlHV6hi2qwOrQiF87agbJy0mYb+QkjkZJ3aZpiP2WKKKD580DZaGhYX3j1Vlb1eID5SuNsSyHFywTl3pC3YMG30vJG8XYbCszzSJSWmdWXweSj0uDqVRl+7fCVO8buS3Taq1e45tZ6VKGBl/HiSKsRX1N+fkA7e1QrDiJrRfThNnFDttPFmNtEK/uoF2vtw9M2h40BiZqDWIdISnrDA0epZpww6jYlCoKC5SaqvcqIy+tamiwOMUngYqHVRnUVBagt/sGdhyt4O+n62xtk6bQ+fsosZO09j0vZnIeqWpI61BhjanoNW/1tawWJ6PhuHmCa1gWDqbT4cacStULfe3p1UKBGVsr9C1/lR8b/UJbJeA1xon9MRrUCs3Ry8vkuI1HULs4mk5HxTaqy38pq+YUVSuodTT/s6by+Nbhwrr9AlvrM2uN8v8CTcPkuxwMpt0AAAAASUVORK5CYII=\"></div></div></header>");
			writer.write("<nav style=\"padding: 0px 48px;background: #336799;\">");
			http: // www.sonata-software.com/sites/all/themes/sonata/logo.png
			writer.write("<div class=\"content-width\" style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\">");

			writer.write(
					"<ul style=\"list-style:none;display: table;table-layout: fixed;width: 100%;height: 35px;margin: 0\">");
			writer.write("<Header style=\"background:#336799;padding: 2px;\">");
			/*
			 * String[] labelArray= {"Versions","Smoke","Stability","Regression"};
			 * 
			 * for(int l=0;l<labelArray.length;l++) { writer.
			 * write("<li style=\"display: table-cell;vertical-align: middle;text-align: center;\">"
			 * ); writer.
			 * write("<a href=\"\" style=\"color: #FFFFFF;text-decoration: none;font-size: 20px;text-decoration: underline;\">"
			 * +labelArray[l]+"</a></li>"); }
			 */

			writer.write("</ul></div></nav>");

			writer.write("<body>");

			writer.write("<div class=\"content\" style=\"min-height: 300px;\">");
			InetAddress IP = InetAddress.getLocalHost();
			String ipaddress = IP.getHostAddress();
			writer.write(
					"<p style=\"font-size:130%\"><a href=\"http://" + ipaddress + ":8082" + "/Reports/\">Back</a>");

			// writer.write("<div class=\"center-title\" style=\"display:
			// inline-block;vertical-align: center; width: calc(100% - 20px);text-align:
			// center;\"><h1 style=\"font-size: 18px;color: #092a5e;\">Smoke Test</h1>
			// </div>");
			writer.write("<br><br><table align=\"center\"border=\"2\" style=\"width:1205px;\">");
			writer.write("<tr><th bgcolor=\"  #b7e5f9  \" style=\"text-align:center;\"><b>ENVIRONMENT</th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>BUILD<b></th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>BRANDS</b></th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>DATE OF EXECUTION</b></th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \" style=\"text-align:center;\"><b>FAILURES ASSIGNED TO</th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>SMOKE ANALYSIS</b></th></tr>");
			for (Map.Entry<String, String> entry : allMap.entrySet()) {
				buildNum = entry.getKey();
				brandEnvt = entry.getValue();

				// code for environment
				String[] brandEnvSplit = brandEnvt.split("\\_");
				env = brandEnvSplit[0];
				envPrin = env;
				if (env.equals("www")) {
					envPrin = "LIVE";
				} else if (env.equals("pat")) {
					envPrin = "pat";
				}

				writer.write("<tr><td align=\"center\">" + envPrin.toUpperCase() + "</td>");
				String[] build = buildNum.split("#");
			

				if (build[0].equals("null"))

				{
					build[0] = "Deployment in progress";
					// System.out.println(build[0]+"===sjkdjgf\n\n");
				}
				writer.write("<td align=\"center\">" + build[0] + "</td>");

				// code for brand
				bran = brandEnvSplit[1].replace("\\[", "");
				String[] brandsAll = bran.split("\\,");
				writer.write("<td><table align=\"center\" style=\"width:500px;\">");

				failTeamAssign = null;
				failTeamAssign1 = null;
				teamAssign.removeAll(teamAssign);
				for (int i = 0; i < brandsAll.length; i++) {

					branStat = brandsAll[i].replace("\\[", "");
					branStat = brandsAll[i].replace("]", "");
					String[] brandOnly = branStat.split("\\#");
					brand = brandOnly[0].replace("[", "");
					brand = brand.replaceAll("\\s", "");

					enBran = env + "#" + brand;
					// System.out.println(enBran+"===\n\n");

					for (Map.Entry<String, String> passEntry : eMap.entrySet()) {
						passBran = passEntry.getKey();
						passPath = passEntry.getValue();
						String[] pPath = passPath.split("#");
						pass = pPath[0];
						oriFPath = pPath[1];
						String[] ori = oriFPath.split("reports");
						finalPath = "http://" + ipaddress + ":8082/Reports/execution_reports/" + ori[1];
						// finalPath = "C:\\Tomcat\\webapps\\Reports\\execution_reports" +ori[1];
						// System.out.println(ori[1]);
						// finalPath = oriFPath;

						if (passBran.equals(enBran)) {

							fsta = Float.valueOf(pass);
							fsta = (float) Math.ceil(fsta);

							// System.out.println(enBran);
							if (fsta >= 95) {
								writer.write("<td bgcolor=\"#00FF00\" align=\"center\"><a href=\"" + finalPath
										+ "\" target=\"_blank\">" + brand.toUpperCase() + "</b><br>" + fsta + "%</td>");

							}

							else if (fsta <= 94 && fsta > 85) {
								writer.write("<td bgcolor=\"#FFBF00\" align=\"center\"><a href=\"" + finalPath
										+ "\" target=\"_blank\">" + brand.toUpperCase() + "</b><br>" + fsta + "%</td>");

							}

							else {
								writer.write("<td bgcolor=\"#FF0000\" align=\"center\"><a href=\"" + finalPath
										+ "\" target=\"_blank\">" + brand.toUpperCase() + "</b><br>" + fsta + "%</td>");
								// System.out.println(brand+"==iuy\n\n");
								failBrand = brand.toUpperCase();
								failBrand = failBrand.replaceAll("\\s", "");
								// code for failure assignment
								// System.out.println(failBrand+"==iuy\n\n");
								if (failBrand.equals("TH") || failBrand.equalsIgnoreCase("FC") || failBrand.equals("IE")
										|| failBrand.equals("MC") || failBrand.equals("LM") || failBrand.equals("CR")) {

									failTeamAssign1 = "Book Team";
									teamAssign.add(failTeamAssign1);
								} else if (failBrand.equals("THFO") || failBrand.equals("IEFO")) {
									failTeamAssign1 = "Cruise & FO Team ";
									teamAssign.add(failTeamAssign1);

								} else if (failBrand.equals("FI") || failBrand.equals("DK") || failBrand.equals("NO")
										|| failBrand.equals("SE")) {
									failTeamAssign1 = "Accelerate Team";
									teamAssign.add(failTeamAssign1);
								} else if (failBrand.equals("THCA") || failBrand.equals("CA") || failBrand.equals("CDM")
										|| failBrand.equals("IECA") || failBrand.equals("FCCA")) {
									failTeamAssign1 = "Service Team";
									teamAssign.add(failTeamAssign1);
								}

							}
						}
					}

				}
				if (teamAssign.size() == 0) {
					teamAssign.add("==NA==");
				}
				writer.write("</table></td>");
				writer.write("<td align=\"center\">" + lastDate + "<br><br>" + exeTime + "</td>");
				Set<String> uniqueTeam = new HashSet<String>(teamAssign);
				System.out.println(uniqueTeam.size());
				failTeamAssign = null;
				for (String value : uniqueTeam) {
					failTeamAssign = failTeamAssign + ",<br>" + value;
				}
				// failTeamAssign = "Book Team";
				writer.write("<td align=\"center\">" + failTeamAssign.replace("null,", "") + "</td>");
				writer.write(
						"<td align=\"center\"><a href=\"\\Reports\\Smoke_Report\\Smoke_Reportanalysis.html\" target=\"_blank\">Click here</a></td>");
			} // end of for loop for envt & brand

			writer.write("</table><br><br>");
			writer.write("</div>");
			writer.write("<footer style=\"background:#336799;padding: 5px;\">");
			writer.write("<div style=\"text-align:center;\">");
			writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n"
					+ "<span style=\"background: green;padding: 1px 10px; \"></span>&nbsp; Passed </div>  ");
			writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n"
					+ "<span style=\"background: red;padding: 1px 10px;\"></span>&nbsp;Failed</div>");
			writer.write("</footer></div></body>");
			writer.write("</body>");
			writer.write("</html>");
			writer.close();
			////////////////////////////////

			AnalysisFilestart();
			samefilestart();

			try {
				for (String list : WebBAPercentagePassAnalysis.scenarionameslist) {
					String key = list.split("_")[0].toUpperCase();
					List<String> list1;
					if (finalMap.get(key) == null) {
						list1 = new ArrayList<>();
					} else {
						list1 = finalMap.get(key);
					}
					list1.add(list);
					finalMap.put(key, list1);
				}
				List<String> list1new = new ArrayList<>();
				list1new.addAll(finalMap.keySet());
				List<String> list1new11 = null;

				writer1.write(
						"<nav style=\"padding: 0px 48px;background: #336799;\"><div class=\"content-width\" style=\"padding: 0;text-align:right;color:white;margin: 0;font-family:"
								+ " Arial,sans-serif;\"><ul style=\"list-style:none;display: table;table-layout: fixed;width: 100%;margin: 0\">"
								+ "<header style=\"background:#336799;padding: 2px;\">" + "</header></ul>");
				writer1.write("Date : " + finalStringdatereport + "</div></nav>");
				for (String Buttons : list1new) {
					writer1.write("<button class=button2 onclick=myFunction('" + Buttons + "')><b>" + Buttons
							+ "</b></button>");
				}
				commonsinh();
				for (int s = 0; s < list1new.size(); s++) {
					writer1.write("<div class='" + list1new.get(s) + " report hide'>");
					writer1.write("<table cellpadding=\"15\">");
					templateforBookingswithANC();
					list1new11 = new ArrayList<>();
					list1new11.addAll(finalMap.get(list1new.get(s)));
					BookingswithANCforloop(list1new.get(s), list1new11);

					// OnlyBookingsforloop(list1new.get(s),list1new11);
					writer1.write("</table>");
					// SpaceBetweenTables();
					// writer1.write("<table cellpadding=\"15\">");
					// templateforonlyBookings();
					// OnlyBookingsforloop(list1new.get(s),list1new11);
					// writer1.write("</table>");
					SpaceBetweenTables();
					writer1.write("<table cellpadding=\"15\">");
					// templateforCA();
					OnlyCustomerAccountforloop(list1new.get(s), list1new11);
					writer1.write("</table>");
					writer1.write("</div>");
				}

			} catch (Exception e) {
				System.out.println(e);
				e.printStackTrace();
			}
			AnalysisFileEnd();

		}
	}

	private static void commonsinh() {

		try {

			writer1.write("<script>");
			writer1.write("var prevClickedTeam = '';");
			writer1.write(
					"function myFunction(team){  var displayingReports = document.getElementsByClassName('report show');if(displayingReports.length > 0){"
							+ "var i = displayingReports.length-1;while (i >= 0) {displayingReports[i].classList.add('hide'); displayingReports[i].classList.remove('show'); i--;}}"
							+ "  if(prevClickedTeam != team){	var seletedTeam = document.getElementsByClassName(team);"
							+ "  if(seletedTeam.length > 0){var i = seletedTeam.length-1;"
							+ "while (i >= 0) {seletedTeam[i].classList.remove('hide'); seletedTeam[i].classList.add('show');	i--;}}"
							+ "  prevClickedTeam = team;}  else {   prevClickedTeam = '';}      }");
			writer1.write("</script>");

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void AnalysisFilestart() {

		String Prjpath;
		File readPrjPath = new File("");
		Prjpath = readPrjPath.getAbsolutePath();

		try {
			fin1 = new File(Prjpath + "\\Tomcat\\webapps\\Reports\\Smoke_Report" + "\\Smoke_Reportanalysis.html");
			writer1 = new FileWriter(fin1);
		} catch (Exception e) {

		}
	}

	public static void SpaceBetweenTables() {
		try {
			writer1.write("<div style=\"padding: 15px 0px;background: #fff;\">");
			writer1.write("</div>");
		} catch (Exception e) {

		}
	}

	public static void AnalysisFileEnd() {
		try {
			writer1.write("</body>");
			writer1.write("</html>");
			writer1.close();

		} catch (Exception e) {

		}
	}

	public static void samefilestart() {
		try {
			writer1.write("<!DOCTYPE html>");
			writer1.write("<html>");
			writer1.write("<head>");
			writer1.write("<title>TUI Automation Report Analysis</title>");
			writer1.write(
					"<meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"></head>");
			writer1.write("<style>");
			writer1.write(
					"table { font-family: arial, sans-serif;border-collapse: collapse; width: 94%;white-space:�nowrap;margin: auto; }");
			writer1.write("td, th { border: 1px solid black;text-align: left;padding: 8px; font-size: 15px;}");
			writer1.write(".show {display:block;}");
			writer1.write(".hide {display:none;}");
			writer1.write(
					".button2 { background-color: #4CAF50; /* Green */border: none; color: WHITE;padding: 10px 10px;text-align: center;text-decoration: none; display: inline-block;font-size: 18px;margin: 2px 2px;cursor: pointer;}");
			writer1.write("</style>");
			writer1.write("</head>");
			writer1.write("<body style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;margin-bottom: 30px;\">");
			writer1.write(
					"<header style=\"background: #70cbf4;padding: 24px 48px;padding: 0;margin: 0;font-family: Arial,sans-serif;\">");
			writer1.write(
					"<div class=\"logo\" style=\"padding: 30px;display: inline-block;vertical-align: middle;\"><img style=\"width: 70px;\" src=\"https://www.tui.co.uk/static-images/_ui/mobile/th/images/logo/TUI-Logo.svg\"></div>");
			writer1.write(
					"<div class=\"center-title\" style=\"display: inline-block;vertical-align: middle; width: calc(100% - 220px);text-align: center;\"><h1 style=\"font-size: 20px;color: #092a5e;\">Smoke Report Analysis</h1>");
			/*
			 * writer1.write("<table cellpadding=\"15\">"); writer1.write("<tr>"); writer1.
			 * write("<th colspan=\"7\" style=\"text-align:center;font-size: 13.5px;\">Date : "
			 * +finalStringdatereport+"</th>"); // writer1.
			 * write("<th colspan=\"3\" style=\"text-align:center;font-size: 13.5px;\">Build No : "
			 * +WebBAPercentagePassAnalysis.BuildNo+"</th>"); writer1.write("</tr>");
			 * writer1.write("</table>");
			 */
			writer1.write("</div>");
			writer1.write(
					"<div class=\"logo\" style=\"display: inline-block;vertical-align: middle;\"><img style=\"width: 70px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAC7CAMAAABIKDvmAAAA21BMVEX/////AAAAAABWVlb/UVGRkZGNjY3/gYF2dnb/kZF5eXlaWlr/cHDe3t6cnJzBwcHIyMigoKD5+flpaWn/vLz/1dX/Z2e0tLT/ysqnp6fs7Ozm5uaAgIDz8/MoKCg4ODhDQ0NKSkr/+PgbGxv/nJz/qKj/7e3S0tL/sbH/4eH/GBgPDw//5eX/MDD/Xl7/Rkb/k5P/xsb/iYn/JSX/Pj7/bW3/g4NkZGT/sLAtLS3/KioWFhb/YWH/EhL/eHj/ODjWAAD/Tk5senqFkpLHgoLkLi6/YmLegYHbYGCbnrD6AAAQUklEQVR4nO2daWMiuRGGm7bBjDkNxpgbmtvj+xjssY13kmyS//+LIql0lKTqBmc32U1G74cdo1ZLpUel0tENG0Wgo9zPrC+RrUAj0FAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaw9aXxsb2fz2e32Kru0R5Ftdn/98PtbevTx2+6/PtudZx8aH6tEX09Or1NK+j5roXIWp27lrfV63RJa+0VcXF7qq+vLNVH8Inq2Pq9Od+juwsr/HC1SIWjtQeOOp6+f5/NzgNKiyjkTKBZ38/l8tZbgZlaO7exUQ/3u3X90NJe3tWZHhBHXrDz8+T3aKZs5Szj87TRueBNacoRsRXv8Un7wlixudW+UpZvcO/m2kgfdTUf80hNt5zm7VEafH0UXzco33y+YclBfjv/98Hh2f+5V/o0lnNJFI+2iccUTV+bzgn30BvCKZ7I94XoBHf1u53yH5OicNIYxvaPNvHCdssw+fzMfJQ2tH+zT3Cma6kZHu2hc8i5wzLqxs7zzPMmjW7IYYFHiopM45m52rucouqXNnImbUGy+t/rIo5Hb2mB/iOsIH60dNITznjk3vFlZrrj3J3bIElpBiQ6mB1nRljDmNnWgwAhD7sfw4PnNo8HuwP53Kq5T0dnSDhrcwRI7yWnIe0I0GXQO3uEMFlXTjX/DkUNe6wluQeFmZscen8apNbAismc87aDh2MB1btPgw8SLllILqkdUTYmfP5WGmrvNNDGzY6JP4xumoVppT9K+smnc8KRL9w48tldZHngNZdqsdFWXXv40Gh/MLnGLCQUzu1SfxiMeKYtofpnSA5ayaTwRRdxcojyH4iY7kHhG2rGctUmuLLwZL40GXznBPXrUfYl++BXZ9Rh0rFc+IA6Xc5nag0aK9wqtiaGEBLDsyZdPr7KyL072NBq8hHvHzexoRNBAOmfeK+ZoeuVolE1DrDYyWvvmN9aW8E/buzgNGRVdp0qhwZIv5AjzRxcok8aDmFshCzH3Ie0RRVOXRKqGjEh9CwXgRbJYesnZN7J3gSk0LkUIgEmSmImMISlGzMWV7a6ey+2kASakLB3lZiErNIFzWbEcSgOncdyOpnEDqxAIySmr60waCdyVZPs51w4aHzI5IXeuR1moQAvPweEGtQizBjJN41TyXmS0OIvGVnrUynNTT7tW5sqlozviyAI8h1xkK8H6HNsp8Z1FvtvQNFQNM9JAUBaNluyMG2hHlrU797ALfclvNVxLWXqBZOBAw10507NfI0njVkWXB9+ZtDJofGgLYXS+k7lAO2lcoJMet91widpxaJXhVjR56KElVx1o4UDSWGgA0GTyCCyDxkqnQ1vdaR1r92nPhfGOaGGNuu9eS309Ri4xTeNCQda9RdF4M0umcoqL5jJpoMgrMqVN0lz7nASu0HU87B73oCGbjPbSJuyq0KHto2icozlL+CI5haXTuEdnXhDEUiZprr1OiR/RiWdiztNu9qCRy6DBFwIWYoLGBXYG6BZq059O4xI5AywWM47A9jwzP1wTWaRvZK/9vTx4SlalyjURQWOOz3hgaFIzeiqNQ6snsiZprr2fpzwZHqpz5GIkc06RNK5xgmmOCh0SAkGDDY7zRSIOlJKE2gSCUmmwwdFaJ7wA9p/1IruNn3m69KRnF7nYf7c6lpZsL1q8W52rQgdMoj6NbUSIqDCVBnV/+hHYp561yXW6HueJ9YnUjdedtqur0CFio0+DOeTq2xlMqldvR/O01XUaDbZiO78t/xDd93C9ncGSI/U51eeePB7JXA/Yhsyl/9brDWfgqwHYyhE0Ht0oAUs2/8FIGo2FkwibndTV8yefw0occgUjOzZrdTfzqncaqEPHiqCxctd24Gr+rJBC48zLmz5Jc2XTuKYfnajBIU/6siYVOCnGU7zb3W+q6nufht/CS7rZKTRa3i4NOjDlZD6bRouYLyzPT1K6ykjksJZ/3hSpQkf0Y+vQuPfLvpXgHNE0rvxhDKuCtG13Jo1zoqEikqo65Co1peyc6njLeN8WtbZL7h0al36IeLd6wynCTX0mQgTEqZQjsB00/Ai5xTTkYU7KA7KcZGcPU6JnzMbQonFIRWgYeu5xG00jcg/Xcjl5vpqyKsikMScW9WKFoLfV8vEiXbbqSTsQEjSuaRp3lNWwdVs5qSSNI2ppASalzIOZNL4QMeHJMkbOCGlTlhhJTuOpUatDB6ZxQXVtyqxA0liT62RwLvoILJPGGzHChKeZjZpcLNKvRtxQhpMxTG8LEY0ZfbSzIvxNRgO/dmLyh+UvHfkzaVz4PSvGBvYzeYTlv5+Sk5skt3vpiK5Cx5mVRG2BYMnhcKJonKbMHQnlR6Ds9cbCSxFDw1pgwHI9IXxaPNJ2A88D3eMqdBga5TSTFwR+gsa7Z7sU9B+518ymceq1veV7mfQOdw32wK1eeGfL5ESRU4tWRGOdtr2CJYcdR6G/rR6Z0QNFLTlII7JpwBhDMfKOGnLy7PPcmvZE64hn4hwwefrUsmnw+ETTkKEKFyJPWvCR51XqcJD7WmqS3bFPAebJXFT9/m1Bu9i73Nyuv8BK/v1MhLqW/xDuQlxIyKetiaHxUYYSW3PnZcur7excmTrfCquuDsu3iU46/OD+8PAGs9Ti+f7JcpiLs9s7lfnu26E7Rezatc3UhcVCjNdT+k3Q78+JyQcD+9TzgLsEnb/7RK41Db2TEzlxHsfaN7Ot1uJDaI0TcJPv7LzuymD3HvZpdakaepe1wz2ct9TpetJ6pg5Lr7dlI+JF3FmSJHAfyle2qD3hK+XtA/eWsi3eCdc4wTLSz/s5GlwPjzc35Bzq6P3hhmXM2uH/uRXeM8cKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAPLpbHz6z3/17oNvoHk+sY//2iD/lD9I9BAcmn8/ZdfD39W/frL3xwaB/Ffo59Vf4mLTkopLvwhlvwZVIyPnZRAAyvQwAo0sGgajW7XSRl0kdzsfopMbtDJforRYN9C3IzZuankTBrjGMvJ18y4VrFuzOv0OK6pP6tWlopTAH27V0haPUINfK2fUUFc2ovGQRaN4t40qsiGuvrzZG8aVe+aptFLqcen4fq7dWMz0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAINi8bk96GB36zWNP4Db1b/2zQ2e9Ho5KtKRa/FNX5xEw/5VddgZmVB35rXBDCNuik779MQF6fxxr7dozEwhVRfPCcSF1/jJi+k5hVybOpv70UDqe7REOrEYyqZ0SC/EIBoIDV8GkKjuEMX4jZNaOLREBrGbSo5jpdE6r40ajSNk1Qa5FcyaBqDQAMp0MAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaxAA+vPTcM95eT63ElgL/UkkLKhEcc9KncqDQopo3FCJX+KxjFB4yDvqxnHRGq+FA+rRHI/jgtU9jguEqmFOO5TuafxhkitxnGTyj2MS1TyK1lIiiVjj4b9EOVnk/t7X/2Dn1lUlAgKCgoKCgoKCgoKCgoKCgr639WgUqn0nB8w7i3rFfpXl38/9Xi9dlKjUq9XyJ9Y/i+pOoXzsGFfH8T25DvDr1VlGH8neqouf0W/f9yP5QFuwz1fg1eXN3HK4XjlWOabVDWRmnyReaIPyEeowHHHraJ3bM6aY32k3TVvEWvrhMZw17RUVf08wKWx9lW+os/yuDovPsCFuqGhT+vxr0Hz9A1JY6Orc387WheoBMwH6KXuycCn8WK/ps4x17TNy1ifiLfN2bi2wy2sQ9CIOce4uBxEjV6tqPJwoHmGvLHcaBzMkKF+QoB8g9U8leltIdZ/B/BX3dznw+Dd3GGjc1DpjBUNVtCwzYoatNkt04ZswGtbqVaBf5lVJ/DXoKEby19hl+ebTV0hWKcH3ij+Wq/Vam3+O+RtRaOky29Hhdg8v2gUxcOJWmzcr606jrWqrg+XEY1JPGS9gh5qNOwj6CFjQz0ZitGjqMpQVLFB3VhSvzc+MuNTK4/unaiWT+JX9ZjH/Fb5JJ5i6xgN+KOmhtXAdtyx33FT/BCmILMzGpw5RABDo8eHz9f4xdw9sGgseScwb3HrqMTeQwyWNDSfhtKIFBo60lRlPtYJHdlxFd3BrnWaRvQi/dmhMfJoLNHPwEc6ODEag65qlqEhuqmA8dk0xLcYmv5jtQr6fohU33KhmsS1i0ZFjo96/KqeTnb0Zdc6Q6Mp3cuhUfS+xZG3HbsIhXEavGARDAwNFu5EFxgANg3xYel/3YSHXGemGaIBLu4cQgOyabB8Y/5PgVU0AbNK+hbXOkNjSo8UHopH1Tp6gFqyHy53IOAIGspETaMO5MaoIRaNDqD86g9HPoVv2kvTfsZnhDOMoEzWgEIfpP3PolGEfC/MyiqMiTjVOk2jrlyTWfuyUWqamW5SkHPpxH64XIeJFWi0YUhqGhtoZjs2T4EtGmOglyeeKcvZdFiqgo908VQIRXcjalJ0aECM7vLc8JLAUvu2Z52k0eAT2kBZi2dYNvLM//eiJs3EHmvR4NGnYWh0ZTfgfsU0KjKe95yWQjP06mLSjZzhFumRzWjInjvQ49eiAb5eE9hFs/PKft86Vtj09VUsXXr67nFBS1pdyzeHCr/jGzWLxlLUoGicKLOOTRTANPrKsBL5MkNj2SkAkq64z5p5SorGjrjBcgy5AQVBsMj9cRLZ1vWVddrRdBgb0EtD1jlN8JWmHd+q4DKSBhipaDCCkzHXyBSKabD1rLg+eYnp9yy46kMxfbAefMHJcs22m0YeLOJDsc1GwkAvmX3r2EhZLpcdtNZJpSHCTU9EPmz5AVStaPT4/CtpLN1B59Co4ctWUy11ITaPrejdlXPFbhp8SdMFJ2TG9eqqMwnrZNw40WEjiwZ4VNdaBfXkoFM0eKblFIrgS3mpog5diAYbBW15fUO/VgOC/WDVmoersk920+CzeFsuQ1/jdkH1i21dG9HgBQwbFA38t4wYJfwl04ksSNPgMWkoimig2GhGvaHRQzVVnBhZR28idWG510AbAvEN4MaeNJrxcCNB9uPiSM6dlHV6hi2qwOrQiF87agbJy0mYb+QkjkZJ3aZpiP2WKKKD580DZaGhYX3j1Vlb1eID5SuNsSyHFywTl3pC3YMG30vJG8XYbCszzSJSWmdWXweSj0uDqVRl+7fCVO8buS3Taq1e45tZ6VKGBl/HiSKsRX1N+fkA7e1QrDiJrRfThNnFDttPFmNtEK/uoF2vtw9M2h40BiZqDWIdISnrDA0epZpww6jYlCoKC5SaqvcqIy+tamiwOMUngYqHVRnUVBagt/sGdhyt4O+n62xtk6bQ+fsosZO09j0vZnIeqWpI61BhjanoNW/1tawWJ6PhuHmCa1gWDqbT4cacStULfe3p1UKBGVsr9C1/lR8b/UJbJeA1xon9MRrUCs3Ry8vkuI1HULs4mk5HxTaqy38pq+YUVSuodTT/s6by+Nbhwrr9AlvrM2uN8v8CTcPkuxwMpt0AAAAASUVORK5CYII=\"></div></div></header>");
			writer1.write("</header>");
			writer1.write("</div>");

			// writer1.write("<div class=\"content\" style=\"min-height: 2px;\">");
			InetAddress IP = InetAddress.getLocalHost();
			String ipaddress = IP.getHostAddress();
			writer1.write("<p style=\"font-size:130%\"><a href=\"http://" + ipaddress + ":8082"
					+ "/Reports/Smoke_Report/Smoke_Report.html\">Back</a>");

		} catch (Exception e) {

		}
	}

	public static void templateforBookingswithANC() {
		try {
			writer1.write(" <tr style=\"background:#1cbaff\">");
			writer1.write(" <th colspan=\"7\" style=\"text-align:center\">Smoke report detail analysis</th>");
			writer1.write(" </tr>");
			writer1.write("<tr style=\"background:#b7e5f9\">");
			writer1.write(" <th width ='15%'>Scenarios</th>");
			writer1.write(" <th style=\\\"text-align:center\\\" width ='10%'>Percentage %</th>");
			writer1.write(" <th width ='10%'>Booking No</th>");
			// writer1.write(" <th width ='3%'>Status</th>");
			writer1.write(" <th width ='12%'>Failure Count</th>");
			writer1.write(" <th width ='10%'>Error Count</th>");
			writer1.write(" <th width ='77%'>Issue</th>");
			writer1.write(" </tr>");
		} catch (Exception E) {

		}
	}

	public static void templateforonlyBookings() {
		try {
			writer1.write(" <tr style=\"background:#1cbaff\">");
			writer1.write(" <th colspan=\"7\" style=\"text-align:center\">BOOKFLOWS</th>");
			writer1.write(" </tr>");
			writer1.write("<tr style=\"background:#b7e5f9\">");
			writer1.write(" <th width ='15%'>FlowName</th>");
			writer1.write(" <th width ='3%'>Percent%</th>");
			writer1.write(" <th width ='5%'>Booking RefNo</th>");
			writer1.write(" <th width ='5%'>Total No Of Failures</th>");
			writer1.write(" <th width ='5%'>Total No Of Errors</th>");
			writer1.write(" <th width ='75%'>Issue</th>");
			writer1.write(" </tr>");
		} catch (Exception E) {

		}
	}

	/*
	 * public static void templateforCA() { try {
	 * writer1.write(" <tr style=\"background:#1cbaff\">"); writer1.
	 * write(" <th colspan=\"8\" style=\"text-align:center\">CUSTOMERACCOUNT</th>");
	 * writer1.write(" </tr>"); writer1.write("<tr style=\"background:#b7e5f9\">");
	 * writer1.write(" <th width ='13%'>FlowName</th>");
	 * writer1.write(" <th width ='3%'>Percent%</th>");
	 * writer1.write(" <th width ='12%'>EmailId</th>");
	 * writer1.write(" <th width ='10%'>Password</th>");
	 * writer1.write(" <th width ='8%'>Status</th>");
	 * writer1.write(" <th width ='5%'>Total No Of Failures</th>");
	 * writer1.write(" <th width ='5%'>Total No Of Errors</th>");
	 * writer1.write(" <th width ='75%'>Issue</th>"); writer1.write(" </tr>"); }
	 * catch(Exception E) {
	 * 
	 * } }
	 */
	public static void regexforFilePicking(String ClearName) {
		try {
			String printreportname = null;
			String checkname = ClearName;

			String pattern33 = "(.+?)_chrome.html$";
			Pattern p = Pattern.compile(pattern33);
			Matcher m = p.matcher(ClearName.replace("_1", ""));
			if (m.find()) {
				printreportname = m.group(1).trim();
			}

			writer1.write("<td><a href=\"" + WebBAPercentagePassAnalysis.scenarionameslistlink + ClearName
					+ "\" target=\"_blank\">" + printreportname + "</a></td>");
			writer1.write("<td   style=\"text-align:center\">"
					+ WebBAPercentagePassAnalysis.Percentageeach.get(checkname) + "%</td>");

		} catch (Exception E) {
		}
	}

	public static void BookingswithANCforloop(String environ, List<String> list1new111) {
		try {

			String sssssss = environ;
			for (int i = 0; i < list1new111.size(); i++) {
				if (list1new111.get(i).contains("") || list1new111.get(i).contains("")
						|| list1new111.get(i).contains("") || StringUtils.containsIgnoreCase(list1new111.get(i), "")
						|| list1new111.get(i).contains("")) {

					if (i % 2 == 0) {
						writer1.write("<tr style=\"background:#CAE2E2\">");
					} else {
						writer1.write("<tr>");
					}

					regexforFilePicking(list1new111.get(i));

					if (WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).length() > 5) {
						writer1.write(" <td style=\"color:#035c03;font-weight:bold;text-align:center\">");
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)));
						writer1.write(" </td>");
						/*
						 * writer1.write(" <td >");
						 * //writer1.write(WebBAPercentagePassAnalysis.MMBCHECK.get(list1new111.get(i)))
						 * ; writer1.write("PASS"); writer1.write(" </td>");
						 */
					} else {
						writer1.write(" <td style=\"color:#FF0000;font-weight:bold;text-align:center\">");
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)));
						writer1.write(" </td>");
						/*
						 * writer1.write(" <td >"); writer1.write("NA"); writer1.write(" </td>");
						 */
					}
					writer1.write(" <td  style=\"text-align:center\">");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i))));
					writer1.write(" </td>");
					writer1.write(" <td  style=\"text-align:center\">");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i))));
					writer1.write(" </td>");

					int k = 0;

					for (int f = 0; f < WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
							.size(); f++) {
						if ((WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).contains("FAIL"))
								&& (WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i)) == 0)
								&& (WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i)) == 0)) {
							// writer1.write("<li>Script did not execute fully</li>");
						} else {
							String statusItem = WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
									.get(f);
							if (!statusItem.contains("Error :")) {
								k = 1;
								// writer1.write("<li>"+WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i)).get(f)+"</li>");
							}
						}
					}

					if (WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).length() > 5 && k == 0) {
						writer1.write(" <td>");
						writer1.write("<li>" + "Minor errors :Look at the script errors" + "</li>");
						writer1.write(" </td>");
					} else {
						writer1.write(" <td>");
						for (int f = 0; f < WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
								.size(); f++) {
							if ((WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).contains("FAIL"))
									&& (WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i)) == 0)
									&& (WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i)) == 0)) {
								writer1.write("<li>Script did not execute fully</li>");
							} else {
								String statusItem = WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
										.get(f);
								if (!statusItem.contains("Error :")) {
									k = 1;
									writer1.write("<li>"
											+ WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i)).get(f)
											+ "</li>");
								}
							}
						}

						writer1.write(" </td>");
					}

					writer1.write("</tr>");
				}
			}
		} catch (Exception E) {

		}
	}

	public static void OnlyBookingsforloop(String environ, List<String> list1new111) {
		try {
			for (int i = 0; i < list1new111.size(); i++) {
				System.out.println(list1new111.get(i));
				if (list1new111.get(i).contains("") || list1new111.get(i).contains("")
						|| list1new111.get(i).contains("") || StringUtils.containsIgnoreCase(list1new111.get(i), "")
						|| list1new111.get(i).contains("")) {

					writer1.write("<tr>");
					regexforFilePicking(list1new111.get(i));
					if (WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).length() > 5) {
						writer1.write(" <td style=\"color:#00e600;font-weight:bold\">");
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)));
						writer1.write(" </td>");
					} else {
						writer1.write(" <td style=\"color:#FF0000;font-weight:bold\">");
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)));
						writer1.write(" </td>");
					}
					writer1.write(" <td>");
					writer1.write("NA");
					writer1.write(" </td>");
					writer1.write(" <td>");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i))));
					writer1.write(" </td>");

					writer1.write(" <td>");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i))));
					writer1.write(" </td>");
					writer1.write(" <td>");
					for (int f = 0; f < WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
							.size(); f++) {
						if ((WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).contains("FAIL"))
								&& (WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i)) == 0)
								&& (WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i)) == 0)) {
							writer1.write("<li>Script did not execute fully</li>");
						} else {
							writer1.write(
									"<li>" + WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i)).get(f)
											+ "</li>");
						}

					}
					writer1.write(" </td>");

					writer1.write("</tr>");
				}
			}
		} catch (Exception E) {

		}
	}

	public static void OnlyCustomerAccountforloop(String environ, List<String> list1new111) {
		try {
			for (int i = 0; i < list1new111.size(); i++) {
				System.out.println(list1new111.get(i));
				if (list1new111.get(i).contains("_THCUSTOMERACCOUNT")) {
					writer1.write("<tr>");
					regexforFilePicking(list1new111.get(i));

					writer1.write(" <td>");
					if (WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).split("-")[0]
							.contains("THPDP@")) {
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).split("-")[0]);
						// writer1.write(String.valueOf(WebBAPercentagePassAnalysis.Customeraccountdetails.trim().split("-")[0]));
						writer1.write(" </td>");
						writer1.write(" <td>");
						writer1.write(WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).split("-")[1]);

						// writer1.write(String.valueOf(WebBAPercentagePassAnalysis.Customeraccountdetails.trim().split("-")[1]));
						writer1.write(" </td>");
						writer1.write(" <td>");
						double capercent = Double.parseDouble(
								WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).split("-")[2]);
						if (capercent == 100) {
							writer1.write("ACTIVATED");
						} else {
							writer1.write("NOT ACTIVATED");
						}
						writer1.write(" </td>");
					} else {
						writer1.write("Not Available");
						writer1.write(" </td>");
						writer1.write(" <td>");
						writer1.write("Not Available");
						writer1.write(" </td>");
						writer1.write(" <td>");
						writer1.write("Not Available");
						writer1.write(" </td>");
					}

					writer1.write(" <td>");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i))));
					writer1.write(" </td>");
					writer1.write(" <td>");
					writer1.write(String.valueOf(WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i))));
					writer1.write(" </td>");
					writer1.write(" <td>");
					for (int f = 0; f < WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i))
							.size(); f++) {
						if ((WebBAPercentagePassAnalysis.bookingDetails.get(list1new111.get(i)).contains("FAIL"))
								&& (WebBAPercentagePassAnalysis.FailCount.get(list1new111.get(i)) == 0)
								&& (WebBAPercentagePassAnalysis.ErrorCount.get(list1new111.get(i)) == 0)) {
							writer1.write("<li>Script did not execute fully</li>");
						} else {
							writer1.write(
									"<li>" + WebBAPercentagePassAnalysis.PageFaildetails.get(list1new111.get(i)).get(f)
											+ "</li>");
						}

					}
					writer1.write(" </td>");

					writer1.write("</tr>");
				}
			}
		} catch (Exception E) {

		}
	}
}
//}
