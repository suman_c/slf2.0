package com.summaryreportgenerator;

import java.io.IOException;
import java.text.ParseException;

import com.utilities.ExportSummeryReport;
import com.utilities.ReportGenerationNew;
import com.utilities.SummaryReportWithDefects;

public class CreateAutomationSummaryReport_ExcelReport {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws ParseException, IOException {
		SummaryReportWithDefects summaryReport = new SummaryReportWithDefects();
	    summaryReport.reportForamtion();
		ExportSummeryReport.readXML();
		ExportSummeryReport.readXMlForFailure();
	}
}
