package com.summaryreportgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.utilities.GridMasterSummaryReport;
import com.utilities.ReportGenerationNew;

public class CreateMasterSummaryReport {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws ParseException, IOException {

		ReportGenerationNew pas = new ReportGenerationNew();

		String rptFlderPath, finalResultFldPath, prjPath;
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();

		rptFlderPath = prjPath + "//reports//";
		System.out.println("Report Folder Path : " + rptFlderPath);

		finalResultFldPath = prjPath + "//reports//";

		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();
		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);

				}
			}
		}

		if (datFolder.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);

			dateValue = filPath.getAbsolutePath();

			try {
				strdDate = fmt.parse(filPath.getName().trim());

				diff = sysDate.getTime() - strdDate.getTime();

				diffDays = diff / (24 * 60 * 60 * 1000);

				small = diffDays;

			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();

				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);

					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();

					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);

			long lastMod = Long.MIN_VALUE;

			File choice = null;
			String curPath = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();

				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = amPmFilVal;
							curPath = choice.toString();

						}
					}
				}
			}

			// choice = path for the latest folder
			// ================================================================================
			curPath = curPath.replace("\\", "#");
			System.out.println("current path:" + curPath);
			String[] exeDate = curPath.split("#");

			String lastDate = exeDate[exeDate.length - 2];

			String executionDate = null;
			HashMap<String, String> excutionsMap = new HashMap<String, String>();
			HashMap<String, String> duplicateExcutions = new HashMap<String, String>();
			String version=null;

			// our codes

			Map<String, Map<String, ArrayList<String>>> tableStructure = GridMasterSummaryReport.reportForamtion();
			for (String env : tableStructure.keySet()) {

				Map<String, ArrayList<String>> brandUrl = tableStructure.get(env);
				for (String brand : brandUrl.keySet()) {
					int failedCount = 0;
					int passedCount = 0;
					List<String> list = brandUrl.get(brand);
					for (String file : list) {

						File filename = new File(file);
						String url = file.replace("/", File.separator);
						String fileWithPath = choice + "\\" + filename.getName();

						String newUrlPath = url.replace("\\", "#");

						String[] splitNewUrlPath = newUrlPath.split("#");
						executionDate = splitNewUrlPath[splitNewUrlPath.length - 5];
						String finalPath = "\\" + splitNewUrlPath[splitNewUrlPath.length - 5] + "\\"
								+ splitNewUrlPath[splitNewUrlPath.length - 3] + "\\"
								+ splitNewUrlPath[splitNewUrlPath.length - 1];

						try {
							BufferedReader breader = new BufferedReader(
									new InputStreamReader(new FileInputStream(url)));
							String fcLine;

							while ((fcLine = breader.readLine()) != null) {
								if (fcLine.contains("reportStartTime")) {
									String senarioName = fcLine.substring(fcLine.indexOf("value=") + 7,
											fcLine.indexOf("\"></td>"));								
									String[] reportInfo = senarioName.split("##");
//									version=reportInfo[3];
									if (excutionsMap.keySet().contains(reportInfo[1])) {

										SimpleDateFormat format = new SimpleDateFormat("MM/dd HH:mm:ss");
										Date newtDate = format.parse(reportInfo[2]);
										String dd = excutionsMap.get(reportInfo[1]);
										String[] tt = dd.split("@@");

										Date exDate = format.parse(tt[1]);
										if (exDate.getTime() >= newtDate.getTime()) {
											if (reportInfo[0].equals("FAIL")) {
												failedCount++;
											} else {
												passedCount++;
											}

										} else {
											excutionsMap.put(reportInfo[1], reportInfo[0] + "@@" + reportInfo[2]);
											if (tt[0].equals("FAIL")) {
												failedCount++;
											} else {
												passedCount++;
											}

										}

									} else {
										excutionsMap.put(reportInfo[1], reportInfo[0] + "@@" + reportInfo[2]);
									}

								}

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					duplicateExcutions.put(brand, failedCount + "::" + passedCount);
				}

			}

			File fin = new File(rptFlderPath + "\\GridMasterSummaryReport.html");
			FileWriter writer = new FileWriter(fin);
			writer.write("<html><head><title>RETAIL REGRESSION REPORT</title>");
			writer.write(
					"<style> table{font-family=calibri, sans-serif;calibri, sans-serif;background-color: #F1F2F1;border-collapse: collapse;width: 30%;}");
			writer.write("td, th {border: 1px solid #dddddd;text-align: center;padding: 8px;}");
			// writer.write("tr:nth-child(even) {background-color:
			// #dddddd;}}</style>");
			writer.write("tr:nth-child(even)}</style>");
			writer.write("</head>");
			writer.write("<body>");
			//writer.write(
					//"<br><h1 style=\"color:black;font-size:220%;text-align:center;background-color: #C5E1CC;\">RETAIL REGRESSION REPORT</h1>");

			writer.write("<br><table align=\"center\"border=\"5\" style=\"width:1000px;\">");

		/*	writer.write("<tr><td bgcolor=\" #C5E1CC \"><b>ENVIRONMENT</td>");*/
			writer.write("<tr><td bgcolor=\" #C5E1CC \"><b>EXECUTION STATUS</td>");
			/*writer.write("<td bgcolor=\" #C5E1CC \"><b>BUILD VERSION</td>");*/
			writer.write("<td bgcolor=\" #C5E1CC \"><b>PASS PERCENTAGE</td>");
			writer.write("<td bgcolor=\" #C5E1CC \"><b>DATE OF EXECUTION</td></tr>");

			for (String env : tableStructure.keySet()) {
				Map<String, ArrayList<String>> brandUrl = tableStructure.get(env);

			/*	writer.write("<tr><td>" + env.toUpperCase() + "</td>");*/
				
				for (String brand : brandUrl.keySet()) {
					List<String> list = brandUrl.get(brand);
					float pasValFc = 0;
					float totalCount = 0;
					float passCount = 0;
					for (String file : list) {

						File filename = new File(file);
						// String url = brandUrl.get(brand);
						String url = file.replace("/", File.separator);
						String fileWithPath = choice + "\\" + filename.getName();

						String newUrlPath = url.replace("\\", "#");

						String[] splitNewUrlPath = newUrlPath.split("#");
						executionDate = splitNewUrlPath[splitNewUrlPath.length - 5];
						// System.out.println(executionDate+"&*&*&");
						String finalPath = "\\" + splitNewUrlPath[splitNewUrlPath.length - 5] + "\\"
								+ splitNewUrlPath[splitNewUrlPath.length - 3] + "\\"
								+ splitNewUrlPath[splitNewUrlPath.length - 1];

						try {
							BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(url)));
							String fcLine;
							while ((fcLine = br.readLine()) != null) {

								if (fcLine.contains("<td style=\"text-align:left\"")) {
									String valLineFc = fcLine;
									String[] valArrFc = valLineFc.split("\\</td>");

									String pasValueFc = valArrFc[valArrFc.length - 2];
									pasValueFc = pasValueFc.replace("<td style=\"text-align:left\">", "");
									pasValFc = pasValFc + Float.parseFloat(pasValueFc);

									String total = valArrFc[valArrFc.length - 5];
									total = total.replace("<td style=\"text-align:left\">", "");
									totalCount = totalCount + Float.parseFloat(total);

									String passedTests = valArrFc[valArrFc.length - 4];
									passedTests = passedTests.replace("<td style=\"text-align:left\">", "");
									passCount = passCount + Float.parseFloat(passedTests);
									// url = "file:"+"\\"+url;
								}
								if (fcLine.contains("reportStartTime")) {
									version="";
									String senarioName = fcLine.substring(fcLine.indexOf("value=") + 7,
											fcLine.indexOf("\"></td>"));								
									String[] reportInfo = senarioName.split("##");		
									if(reportInfo.length>3){
									version=reportInfo[3];
									}									
							    }
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					if (duplicateExcutions.containsKey(brand)) {
						String[] dd = duplicateExcutions.get(brand).split("::");
						int passed = Integer.parseInt(dd[1]);
						int failed = Integer.parseInt(dd[0]);

						passCount = passCount - passed;
						totalCount = totalCount - passed;
						totalCount = totalCount - failed;

					}

					pasValFc = (passCount / totalCount) * 100;

					if (pasValFc >= 90) {

						/*
						 * writer.write("<td bgcolor=\"#00FF00\"><a href=\"" +
						 * url + "\" target=\"_blank\">" + brand.toUpperCase() +
						 * "</a>");
						 */
						writer.write("<tr><td bgcolor=\"#18BD34\">" + brand.toUpperCase() + "</td>");
						// writer.write("</td>");

					} else if (pasValFc >= 70 && pasValFc < 90) {
						/*
						 * writer.write("<td bgcolor=\"#FFBF00\"><a href=\"" +
						 * url + "\" target=\"_blank\">" + brand.toUpperCase() +
						 * "</a>");
						 */
						writer.write("<tr><td bgcolor=\"#FFBF00\">" + brand.toUpperCase() + "</td>");
						// writer.write("</td>");
					} else {
						/*
						 * writer.write("<td bgcolor=\"#FF0000\"><a href=\"" +
						 * url + "\" target=\"_blank\">" + brand.toUpperCase() +
						 * "</a>");
						 */
						writer.write("<tr><td bgcolor=\"#FF0000\">" + brand.toUpperCase() + "</td>");
						// writer.write("</td>");
					}
				/*	writer.write("<td>" +version +"</td>");*/
					writer.write("<td>" + Math.round(pasValFc) + " %"+"</td>");
					writer.write("<td>" + executionDate + "</td>");
				}
			}
			writer.write("</table><br><br>");
			// writer.write("<div style=\"text-align: center\"><a
			// style=\"color:black;\"
			// href=\"http://10.136.75.89:90/GridMasterSummaryReport.html\"
			// target=\"_blank\">"+"http://10.136.75.89:90/GridMasterSummaryReport.html"+"</a></div><br><br>");

			// Commented by Hima to adjust the report in jenkins notification
			// view

			/*
			 * writer.write(
			 * "<div style=\"margin-left: 480px;\"><div style=\"margin-bottom:30px;\"><span style=\"background: #18BD34; padding :10px 20px;  \"></span>&nbsp;<b> GREATER THAN 90% GREEN - PASS </div>"
			 * +
			 * "<div style=\"margin-bottom:30px;\"><span style=\"background: #FFBF00; padding :10px 20px; \"></span>&nbsp;<b>  70 - 90 % ORANGE - PASS</div>"
			 * +
			 * "<div style=\"margin-bottom:30px;\"><span style=\"background: red; padding :10px 20px; \"></span>&nbsp;<b> LESS THAN 70% RED - PASS</div>"
			 * + "</div>");
			 */

			writer.write("<br><table align=\"center\"border=\"5\" style=\"width:350px;\">");

			writer.write("<tr><td bgcolor=\"#18BD34\"><b>GREATER THAN 90% - PASS</td></tr>");
			writer.write("<tr><td bgcolor=\"#FFBF00\"><b>BETWEEN 70% - 90 % - PASS</td></tr>");
			writer.write("<tr><td bgcolor=\"red\"><b>LESS THAN 70% - PASS</td></tr>");

			writer.write("</body>");
			writer.write("</html>");

			writer.close();

		}
	}
}
