package com.summaryreportgenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.utilities.WebBAUCalculatePercentagePass;
import com.utilities.ReportGenerationNew;
import com.utilities.WebBAUCalculatePercentagePass;
import com.utilities.WebBAUGridGenerator;

public class WebBAUMasterSummaryReport {

	@SuppressWarnings("unused")
	public static void main(String[] args) throws ParseException, IOException {

		ReportGenerationNew pas = new ReportGenerationNew();

		String rptFlderPath, finalResultFldPath, prjPath;
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		File readPrjPath = new File("");
		prjPath = readPrjPath.getAbsolutePath();

		rptFlderPath = prjPath + "//reports//";
		System.out.println("Report Folder Path : " + rptFlderPath);

		finalResultFldPath = prjPath + "//reports//";

		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();
		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);

				}
			}
		}

		if (datFolder.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);

			dateValue = filPath.getAbsolutePath();

			try {
				strdDate = fmt.parse(filPath.getName().trim());

				diff = sysDate.getTime() - strdDate.getTime();

				diffDays = diff / (24 * 60 * 60 * 1000);

				small = diffDays;

			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();

				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);

					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();

					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);

			long lastMod = Long.MIN_VALUE;

			File choice = null;
			String curPath = null;
			String buildDetails = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();

				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
							curPath = choice.toString();
							
						}
					}
				}
			}

			
			
			

			// choice = path for the latest folder
			// ================================================================================
			curPath = curPath.replace("\\", "#");
			String[] exeDate = curPath.split("#");

			
			String lastDate = exeDate[exeDate.length - 2];
			//System.out.println(lastDate+"====djksgd\n\n");
			//String executionDate = null;

			// Kirti's codes for new master summary report 

			Map<String, String> allMap = WebBAUGridGenerator.reportForamtion();
			
			Map<String, String> eMap = WebBAUCalculatePercentagePass.percentCalculation();
			List<String> teamAssign = new ArrayList<String>();
			
			
			String buildNum, brandEnvt, env, brand, bran,envPrin, branStat, passPercen, failBrand = null, failTeamAssign = null,failTeamAssign1 = null, passBran, enBran, pass = null;
			Float fsta;
			int failB = 0;
					
			File fin = new File(rptFlderPath + "\\Reports.html");
			FileWriter writer = new FileWriter(fin);
			
			writer.write("<html lang=\"en-US\"><head><meta charset=\"UTF-8\">");
			writer.write("<title>TUI Automation Report</title>");
			writer.write(" <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"></head>");
			writer.write("<body style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"structure\"><header style=\"background: #70cbf4;padding: 24px 48px;padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"logo-container\">");
			writer.write("<div class=\"logo\" style=\"display: inline-block;vertical-align: middle;\"><img style=\"width: 100px;\" src=\"http://static.thomson.co.uk/static-images/_ui/mobile/th/images/logo/TUI-Logo.svg\"></div>");
			writer.write("<div class=\"center-title\" style=\"display: inline-block;vertical-align: middle; width: calc(100% - 220px);text-align: center;\"><h1 style=\"font-size: 48px;color: #092a5e;\">Automation Summary Reports</h1> </div>");
			writer.write("<div class=\"logo\" style=\"display: inline-block;vertical-align: middle;\"><img style=\"width: 100px;\" src=\"http://www.sonata-software.com/sites/all/themes/theme841/logo.png\"></div></div></header>");
			writer.write("<nav style=\"padding: 0px 48px;background: #336799;\">");
	        writer.write("<div class=\"content-width\" style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\">");
	        
	        writer.write("<ul style=\"list-style:none;display: table;table-layout: fixed;width: 100%;height: 35px;margin: 0\">");
	       String[] labelArray= {"Smoke","Stability","Regression"};
	      
	       for(int l=0;l<labelArray.length;l++) { 
	          writer.write("<li style=\"display: table-cell;vertical-align: middle;text-align: center;\">");
	          writer.write("<a href=\"\" style=\"color: #FFFFFF;text-decoration: none;font-size: 20px;text-decoration: underline;\">"+labelArray[l]+"</a></li>");
	       }
	       
	        writer.write("</ul></div></nav>");
			
			writer.write("<body>");
			
			writer.write("<div class=\"content\" style=\"min-height: 300px;\">");
	        
	        writer.write("<br><br><table align=\"center\"border=\"2\" style=\"width:1205px;\">");
			writer.write("<tr><th bgcolor=\"  #b7e5f9  \" style=\"text-align:center;\"><b>ENVIRONMENT</th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>BUILD<b></th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>BRANDS</b></th>");	
			writer.write("<th bgcolor=\"  #b7e5f9  \"><b>DATE OF EXECUTION</b></th>");
			writer.write("<th bgcolor=\"  #b7e5f9  \" style=\"text-align:center;\"><b>FAILURES ASSIGNED TO</th></tr>");

			for ( Map.Entry<String, String> entry : allMap.entrySet()) {
				    buildNum = entry.getKey();
				    brandEnvt = entry.getValue();
				  
				    // code for environment 
				    String[] brandEnvSplit = brandEnvt.split("\\_");
				    env = brandEnvSplit[0];
				    envPrin = env;
				    if(env.equals("www")){
				    	envPrin = "pat";
				    }
				    
				    writer.write("<tr><td align=\"center\">" + envPrin.toUpperCase() + "</td>");
				    String[] build = buildNum.split("#");
				    writer.write("<td align=\"center\">"+build[0] + "</td>");
				    
				    
				    // code for brand
				    bran = brandEnvSplit[1].replace("\\[",""); 
				    String[] brandsAll = bran.split("\\,");
				    writer.write("<td><table align=\"center\" style=\"width:500px;\">");
				    
				    failTeamAssign = null;
				    failTeamAssign1 = null;
				    teamAssign.removeAll(teamAssign);
				    
				    for(int i=0;i<brandsAll.length;i++){
				    	
				    	branStat = brandsAll[i].replace("\\[", "");
				    	branStat = brandsAll[i].replace("]", "");
				    	String [] brandOnly = branStat.split("\\#");
				    	brand = brandOnly[0].replace("[","");
				    	brand = brand.replaceAll("\\s", "");
				    			
				    	enBran = env+"#"+brand;
				    	//System.out.println(enBran+"===\n\n");
				    	
				    	for (Map.Entry<String, String> passEntry : eMap.entrySet()){
				    		passBran = passEntry.getKey(); 
				    		pass = passEntry.getValue();
				    		if(passBran.equals(enBran)){
				    			
				    			fsta = Float.valueOf(pass);
				    			fsta = (float) Math.ceil(fsta);
				    			
				    	
				    	
				    	if(fsta==100){
				    		writer.write("<td bgcolor=\"#00FF00\" align=\"center\">" +brand.toUpperCase() + "</b><br>"+fsta+"%</td>");
				    		failTeamAssign = "No Failure";
				    		
				    	}
				    	/*
				    	else
				    		if(fsta>60){
				    			writer.write("<td bgcolor=\"#FFBF00\" align=\"center\">" +brand.toUpperCase() + "</b><br>"+fsta+"%</td>");
				    		}
				    		*/
				    	else{
				    		writer.write("<td bgcolor=\"#FF0000\" align=\"center\">" +brand.toUpperCase() + "</b><br>"+fsta+"%</td>");
				    		
				    		failBrand = brand.toUpperCase();
				    		failBrand = failBrand.replaceAll("\\s", "");
				    		
						    //code for failure assignment
				    		//System.out.println(failTeamAssign+"==iuy\n\n");
						    if(failBrand.equals("TH") || failBrand.equalsIgnoreCase("FC") || failBrand.equals("FJ") || failBrand.equals("MC") || failBrand.equals("LM")){
						    	
						    	failTeamAssign1 = "TH, FC, FH, MC & LM: Book Team";
						    	teamAssign.add(failTeamAssign1);
						    	
						    	
						    }
						    else
						    if(failBrand.equals("CR") || failBrand.equals("THFO") || failBrand.equals("FJFO")){
						    	failTeamAssign1 = "CR, THFO, FHFO: Cruise & FO Team";
						    	teamAssign.add(failTeamAssign1);
						    	
						    }
						    else
						    if(failBrand.equals("CA") || failBrand.equals("CDM")){
						    	failTeamAssign1 = "Service Team";
						    	teamAssign.add(failTeamAssign1);
						    	
						    }
						    
						    
						   
						    		
						    		
				    	}
				    	}
				    	}
				    	
				    }
				    writer.write("</table></td>");
				    writer.write("<td align=\"center\">" + lastDate + "</td>");
				    Set<String> uniqueTeam = new HashSet<String>(teamAssign);
				    System.out.println(uniqueTeam.size());
				    failTeamAssign = null;
				    for (String value : uniqueTeam) {
				    	
				    	failTeamAssign = failTeamAssign+",<br> "+value;
				    }
				  //  failTeamAssign = "Book Team";
				    writer.write("<td align=\"center\">" + failTeamAssign.replace("null,","")+ "</td>");
				    
				} // end of for loop for envt & brand
    
			writer.write("</table><br><br>");
			writer.write("</div>");
	        writer.write("<footer style=\"background:#336799;padding: 5px;\">");
	        writer.write("<div style=\"text-align:center;\">");
	        writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n" + 
	        		"<span style=\"background: green;padding: 1px 10px; \"></span>&nbsp; 100%-Passed </div>  ");
	        
	        writer.write("<div style=\"display:inline-block; padding-right: 10px;color: #FFFFFF\">\r\n" + 
	        		"<span style=\"background: #FFBF00; padding: 1px 10px; \"></span>&nbsp;70-99% Qualified</div>");
	        
	        writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n" + 
	        		"<span style=\"background: red;padding: 1px 10px;\"></span>&nbsp;Less than 70% - Failed</div>");
	        
	        writer.write("</footer></div></body>");
			writer.write("</body>");
			writer.write("</html>");

			writer.close();
		}
		}
	}
//}
