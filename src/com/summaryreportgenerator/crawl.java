package com.summaryreportgenerator;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

//import com.opencsv.CSVReader;
//import com.opencsv.CSVWriter;

public class crawl implements Runnable{

       int start=1;
       CSVReader reader,reader1;
       CSVWriter write;
       List<String[]> liveList,tstList;

       crawl(String liveCsv,String tstCsv){
              try {
                     reader = new CSVReader(new FileReader(liveCsv));
                     reader1 = new CSVReader(new FileReader(tstCsv));
                     write = new CSVWriter(new FileWriter(System.getProperty("user.dir")+File.separator+"SEO_final_validation.csv"));

                     liveList= Collections.synchronizedList(reader.readAll());
                     tstList = reader1.readAll();


                     String label = "Status,Actual,Expected,Component,URL";
                     write.writeNext(label.split(","));


              } catch (FileNotFoundException e) {
                     e.printStackTrace();
              } catch (IOException e) {
                     e.printStackTrace();
              }
       }

       @SuppressWarnings("unused")
       public void readCsv() throws InterruptedException{
              try {

                     String[] header = tstList.get(0);
                     Thread.sleep(100);


                     while(start < tstList.size())
                     {   

                           String[] tstArray = tstList.get(start);
                           start++;
                           if(start == tstList.size())
                                  break;
                           for(int i=1;i<liveList.size();i++){

                                  String[] liveArray = liveList.get(i);
                                  String URL = liveArray[0];

                                  liveArray[0] = liveArray[0].replaceAll("\\w+://", "");
                                  liveArray[0] = liveArray[0].substring(liveArray[0].indexOf("/"));

                                  tstArray[0] = tstArray[0].replaceAll("\\w+://", "");
                                  tstArray[0] = tstArray[0].substring(tstArray[0].indexOf("/"));

                                  if(liveArray[0].equalsIgnoreCase(tstArray[0])){
                                         String[] str = new String[5];
                                         str[0] = "Passed";str[1] = tstArray[0];
                                         str[2] = liveArray[0];str[3] = header[0];
                                         str[4] = URL;
                                         System.out.println(Arrays.toString(str));
                                         write.writeNext(str);
                                         for(int a=1;a<tstArray.length;a++){
                                                String[] str1 = new String[5];
                                                if(liveArray[a].equalsIgnoreCase(tstArray[a])){
                                                       str1[0] = "Passed";str1[1] = tstArray[a];
                                                       str1[2] = liveArray[a];str1[3] = header[a];
                                                       str1[4] = URL;
                                                       System.out.println(Arrays.toString(str1));
                                                       write.writeNext(str1);
                                                }else{
                                                       str1[0] = "Failed";str1[1] = tstArray[a] ;
                                                       str1[2] = liveArray[a];str1[3] = header[a];
                                                       str1[4] = URL;
                                                       System.out.println(Arrays.toString(str1));
                                                       write.writeNext(str1);
                                                }
                                         }
                                         liveList.remove(i);
                                         break;
                                  }
                           }
                     }
                     System.out.println("====Completed====");
                     reader.close();
                     reader1.close();
                     write.close();
              } catch (FileNotFoundException e) {
                     e.printStackTrace();
              } catch (IOException e) {
                     e.printStackTrace();
              }
       }

       public void run(){
              try {
                     readCsv();
              } catch (InterruptedException e) {
                     e.printStackTrace();
              }
       }


       public static void main(String[] args) throws IOException {

              if(args.length==2){
                     if(args[0].trim().endsWith("csv")&&args[1].trim().endsWith("csv"))
                     {
                           crawl d = new crawl(args[0].trim(),args[1].trim());
                           Thread t1 = new Thread(d);
                           t1.setName("t1");
                           t1.start();
                           Thread t2 = new Thread(d);
                           t2.setName("t2");
                           t2.start();
                           Thread t3 = new Thread(d);
                           t3.setName("t3");
                           t3.start();
                           Thread t4 = new Thread(d);
                           t4.setName("t4");
                           t4.start();
                           Thread t5 = new Thread(d);
                           t5.setName("t5");
                           t5.start();
                     }else{
                           System.out.println("Arguments cant be csv filenames :");
                     }
              }else{
                     System.out.println("Should be only two arguments");
              }
       }
}


