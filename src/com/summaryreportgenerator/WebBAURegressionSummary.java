package com.summaryreportgenerator;

import com.utilities.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;

public class WebBAURegressionSummary {


    @SuppressWarnings("unused")
    public static void main(String[] args) throws ParseException, IOException {

        ReportGenerationNew pas = new ReportGenerationNew();

        String rptFlderPath, finalResultFldPath, prjPath, resultRegressionPath,indiFilesPath;
        String folderPath = null;
        boolean isDirectryFlag;
        List<File> datFolder = new ArrayList<File>();

        File readPrjPath = new File("");
        //prjPath = readPrjPath.getAbsolutePath();
        prjPath = readPrjPath.getAbsolutePath();
    //    prjPath = "C:\\Tomcat\\webapps\\Reports";


        //rptFlderPath = prjPath + "//reports//";
        rptFlderPath = prjPath +  "\\Tomcat\\webapps\\Reports\\execution_reports\\";
        resultRegressionPath = prjPath + "\\Tomcat\\webapps\\Reports\\Regression\\";

        File del = new File(resultRegressionPath);
        finalResultFldPath = prjPath + "//execution_reports//";
        System.out.println("Report Folder Path : " + rptFlderPath);
        FileUtils.cleanDirectory(del);
        
        // Kirti's codes for new master summary report

        SimpleDateFormat fmt = new SimpleDateFormat();
        Date sysDate = new Date();

        File readFlder = new File(rptFlderPath);
        File[] listOfFolder = readFlder.listFiles();

        
        for (File lstFlder : listOfFolder) {
            isDirectryFlag = lstFlder.isDirectory();
            if (isDirectryFlag && !lstFlder.toString().contains("images")) {
                if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
                    fmt.applyPattern("dd-MM-yyyy");
                    datFolder.add(lstFlder);
                }
            }
        }

        if (datFolder.size() != 0) {
            Date strdDate;
            long small = 0, diff, diffDays;
            File filPath;
            String dateValue, dtVal;

            filPath = datFolder.get(0);
            dateValue = filPath.getAbsolutePath();
            try {
                strdDate = fmt.parse(filPath.getName().trim());
                diff = sysDate.getTime() - strdDate.getTime();
                diffDays = diff / (24 * 60 * 60 * 1000);
                small = diffDays;
            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (int j = 1; j < datFolder.size(); j++) {
                dtVal = datFolder.get(j).getName();
                try {
                    strdDate = fmt.parse(dtVal);
                    diff = sysDate.getTime() - strdDate.getTime();
                    diffDays = diff / (24 * 60 * 60 * 1000);
                    if (diffDays < small) {
                        small = diffDays;
                        dateValue = datFolder.get(j).getAbsolutePath();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            File amPmFilVal = new File(dateValue);
            long lastMod = Long.MIN_VALUE;
            File choice = null;
            if (amPmFilVal.exists()) {
                File[] subFolder = amPmFilVal.listFiles();
                for (File subFld : subFolder) {
                    if (subFld.isDirectory()) {
                        if (subFld.lastModified() > lastMod) {
                            lastMod = subFld.lastModified();
                            choice = subFld;
                            //System.out.println(choice+"====sadh\n\n");
                            folderPath = choice.toString();
                        }
                    }
                }
            }
        } 
        
 // end of code to get the latest folder
        
        Map<String, Map<String, ArrayList<Scenario>>> allMap = WebBAUSummaryGenerator.reportForamtion();
        
        String buildNum, env, brand, bran, envPrin, branStat, passPercen, failBrand, oriFPath, finalPath = null, failTeamAssign = null, failTeamAssign1 = null, passBran, enBran, pass, passPath = null;
		Map<String, ArrayList<Scenario>> brandScenarios;
		String scene = null;
		String status;
		String technicalDifficultes;
		String bookingId;
		String dateOfExecution;
		String fileName;
		String technicalDifficultesMessage;
		ArrayList<Scenario> scenarios = new ArrayList<Scenario>();
		Integer count;
		String allEnvt, allBrans,sumFileName = null, envtEach, fileSumName, sumBrandName;
		TreeSet<String> allBrands;
		
		TreeMap<String, TreeSet<String>> envtBranMap = new TreeMap<String, TreeSet<String>>();
		TreeSet<String> brandSumFileName = new TreeSet<String>();
		
        Float fsta;
        int failB = 0;
        InetAddress IP = InetAddress.getLocalHost();
		String ipaddress=IP.getHostAddress();
        indiFilesPath = "http://"+ipaddress+":8080";
        folderPath= folderPath.replace("C:\\Tomcat\\webapps","");

   
    
        for ( Entry<String, Map<String, ArrayList<Scenario>>> entry : allMap.entrySet()) {
		   
        	env = entry.getKey();
        	brandScenarios = entry.getValue();
        	
        	if(env.equals("www")) {
        		env = env.replace("www","PAT");
        	}
        	 
        	
        	for(Entry<String, ArrayList<Scenario>> branEntry : brandScenarios.entrySet()) {
        		brand = branEntry.getKey();
        		scenarios = branEntry.getValue();
        		int failcount = 0 ;	
        	 	int passcount = 0 ;
        		Integer sceSize = scenarios.size();
        		
        		
        		File fin = new File(resultRegressionPath + "\\"+env+"_"+brand+"_"+"RegressionSummaryReport.html");
        		
        	    
        		FileWriter writer = new FileWriter(fin);
        		
    			
        	    writer.write("<html lang=\"en-US\"><head><meta charset=\"UTF-8\">");
        		writer.write("<title>TUI Automation Summary Report</title>");
        		writer.write(" <meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"></head>");
        		writer.write("<body style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"structure\"><header style=\"background: #70cbf4;padding: 24px 48px;padding: 0;margin: 0;font-family: Arial,sans-serif;\"><div class=\"logo-container\">");
        		writer.write("<div class=\"logo\" style=\"padding: 25px;display: inline-block;vertical-align: middle;\"><img style=\"width: 70px;\" src=\"../images/TUI-Logo.svg\"></div>");
        		writer.write("<div class=\"center-title\" style=\"display: inline-block;vertical-align: middle; width: calc(100% - 220px);text-align: center;\"><h1 style=\"font-size: 20px;color: #092a5e;\">"+env.toUpperCase()+" "+brand+" Regression Summary Reports</h1> </div>");
        		writer.write("<div class=\"logo\" style=\"display: inline-block;vertical-align: middle;\"><img style=\"width: 70px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAC7CAMAAABIKDvmAAAA21BMVEX/////AAAAAABWVlb/UVGRkZGNjY3/gYF2dnb/kZF5eXlaWlr/cHDe3t6cnJzBwcHIyMigoKD5+flpaWn/vLz/1dX/Z2e0tLT/ysqnp6fs7Ozm5uaAgIDz8/MoKCg4ODhDQ0NKSkr/+PgbGxv/nJz/qKj/7e3S0tL/sbH/4eH/GBgPDw//5eX/MDD/Xl7/Rkb/k5P/xsb/iYn/JSX/Pj7/bW3/g4NkZGT/sLAtLS3/KioWFhb/YWH/EhL/eHj/ODjWAAD/Tk5senqFkpLHgoLkLi6/YmLegYHbYGCbnrD6AAAQUklEQVR4nO2daWMiuRGGm7bBjDkNxpgbmtvj+xjssY13kmyS//+LIql0lKTqBmc32U1G74cdo1ZLpUel0tENG0Wgo9zPrC+RrUAj0FAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaw9aXxsb2fz2e32Kru0R5Ftdn/98PtbevTx2+6/PtudZx8aH6tEX09Or1NK+j5roXIWp27lrfV63RJa+0VcXF7qq+vLNVH8Inq2Pq9Od+juwsr/HC1SIWjtQeOOp6+f5/NzgNKiyjkTKBZ38/l8tZbgZlaO7exUQ/3u3X90NJe3tWZHhBHXrDz8+T3aKZs5Szj87TRueBNacoRsRXv8Un7wlixudW+UpZvcO/m2kgfdTUf80hNt5zm7VEafH0UXzco33y+YclBfjv/98Hh2f+5V/o0lnNJFI+2iccUTV+bzgn30BvCKZ7I94XoBHf1u53yH5OicNIYxvaPNvHCdssw+fzMfJQ2tH+zT3Cma6kZHu2hc8i5wzLqxs7zzPMmjW7IYYFHiopM45m52rucouqXNnImbUGy+t/rIo5Hb2mB/iOsIH60dNITznjk3vFlZrrj3J3bIElpBiQ6mB1nRljDmNnWgwAhD7sfw4PnNo8HuwP53Kq5T0dnSDhrcwRI7yWnIe0I0GXQO3uEMFlXTjX/DkUNe6wluQeFmZscen8apNbAismc87aDh2MB1btPgw8SLllILqkdUTYmfP5WGmrvNNDGzY6JP4xumoVppT9K+smnc8KRL9w48tldZHngNZdqsdFWXXv40Gh/MLnGLCQUzu1SfxiMeKYtofpnSA5ayaTwRRdxcojyH4iY7kHhG2rGctUmuLLwZL40GXznBPXrUfYl++BXZ9Rh0rFc+IA6Xc5nag0aK9wqtiaGEBLDsyZdPr7KyL072NBq8hHvHzexoRNBAOmfeK+ZoeuVolE1DrDYyWvvmN9aW8E/buzgNGRVdp0qhwZIv5AjzRxcok8aDmFshCzH3Ie0RRVOXRKqGjEh9CwXgRbJYesnZN7J3gSk0LkUIgEmSmImMISlGzMWV7a6ey+2kASakLB3lZiErNIFzWbEcSgOncdyOpnEDqxAIySmr60waCdyVZPs51w4aHzI5IXeuR1moQAvPweEGtQizBjJN41TyXmS0OIvGVnrUynNTT7tW5sqlozviyAI8h1xkK8H6HNsp8Z1FvtvQNFQNM9JAUBaNluyMG2hHlrU797ALfclvNVxLWXqBZOBAw10507NfI0njVkWXB9+ZtDJofGgLYXS+k7lAO2lcoJMet91widpxaJXhVjR56KElVx1o4UDSWGgA0GTyCCyDxkqnQ1vdaR1r92nPhfGOaGGNuu9eS309Ri4xTeNCQda9RdF4M0umcoqL5jJpoMgrMqVN0lz7nASu0HU87B73oCGbjPbSJuyq0KHto2icozlL+CI5haXTuEdnXhDEUiZprr1OiR/RiWdiztNu9qCRy6DBFwIWYoLGBXYG6BZq059O4xI5AywWM47A9jwzP1wTWaRvZK/9vTx4SlalyjURQWOOz3hgaFIzeiqNQ6snsiZprr2fpzwZHqpz5GIkc06RNK5xgmmOCh0SAkGDDY7zRSIOlJKE2gSCUmmwwdFaJ7wA9p/1IruNn3m69KRnF7nYf7c6lpZsL1q8W52rQgdMoj6NbUSIqDCVBnV/+hHYp561yXW6HueJ9YnUjdedtqur0CFio0+DOeTq2xlMqldvR/O01XUaDbZiO78t/xDd93C9ncGSI/U51eeePB7JXA/Yhsyl/9brDWfgqwHYyhE0Ht0oAUs2/8FIGo2FkwibndTV8yefw0occgUjOzZrdTfzqncaqEPHiqCxctd24Gr+rJBC48zLmz5Jc2XTuKYfnajBIU/6siYVOCnGU7zb3W+q6nufht/CS7rZKTRa3i4NOjDlZD6bRouYLyzPT1K6ykjksJZ/3hSpQkf0Y+vQuPfLvpXgHNE0rvxhDKuCtG13Jo1zoqEikqo65Co1peyc6njLeN8WtbZL7h0al36IeLd6wynCTX0mQgTEqZQjsB00/Ai5xTTkYU7KA7KcZGcPU6JnzMbQonFIRWgYeu5xG00jcg/Xcjl5vpqyKsikMScW9WKFoLfV8vEiXbbqSTsQEjSuaRp3lNWwdVs5qSSNI2ppASalzIOZNL4QMeHJMkbOCGlTlhhJTuOpUatDB6ZxQXVtyqxA0liT62RwLvoILJPGGzHChKeZjZpcLNKvRtxQhpMxTG8LEY0ZfbSzIvxNRgO/dmLyh+UvHfkzaVz4PSvGBvYzeYTlv5+Sk5skt3vpiK5Cx5mVRG2BYMnhcKJonKbMHQnlR6Ds9cbCSxFDw1pgwHI9IXxaPNJ2A88D3eMqdBga5TSTFwR+gsa7Z7sU9B+518ymceq1veV7mfQOdw32wK1eeGfL5ESRU4tWRGOdtr2CJYcdR6G/rR6Z0QNFLTlII7JpwBhDMfKOGnLy7PPcmvZE64hn4hwwefrUsmnw+ETTkKEKFyJPWvCR51XqcJD7WmqS3bFPAebJXFT9/m1Bu9i73Nyuv8BK/v1MhLqW/xDuQlxIyKetiaHxUYYSW3PnZcur7excmTrfCquuDsu3iU46/OD+8PAGs9Ti+f7JcpiLs9s7lfnu26E7Rezatc3UhcVCjNdT+k3Q78+JyQcD+9TzgLsEnb/7RK41Db2TEzlxHsfaN7Ot1uJDaI0TcJPv7LzuymD3HvZpdakaepe1wz2ct9TpetJ6pg5Lr7dlI+JF3FmSJHAfyle2qD3hK+XtA/eWsi3eCdc4wTLSz/s5GlwPjzc35Bzq6P3hhmXM2uH/uRXeM8cKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAPLpbHz6z3/17oNvoHk+sY//2iD/lD9I9BAcmn8/ZdfD39W/frL3xwaB/Ffo59Vf4mLTkopLvwhlvwZVIyPnZRAAyvQwAo0sGgajW7XSRl0kdzsfopMbtDJforRYN9C3IzZuankTBrjGMvJ18y4VrFuzOv0OK6pP6tWlopTAH27V0haPUINfK2fUUFc2ovGQRaN4t40qsiGuvrzZG8aVe+aptFLqcen4fq7dWMz0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAKNQCPQCDQCjUAj0Ag0Ao1AI9AINAINi8bk96GB36zWNP4Db1b/2zQ2e9Ho5KtKRa/FNX5xEw/5VddgZmVB35rXBDCNuik779MQF6fxxr7dozEwhVRfPCcSF1/jJi+k5hVybOpv70UDqe7REOrEYyqZ0SC/EIBoIDV8GkKjuEMX4jZNaOLREBrGbSo5jpdE6r40ajSNk1Qa5FcyaBqDQAMp0MAKNLACDaxAAyvQwAo0sAINrEADK9DACjSwAg2sQAMr0MAKNLACDaxAA+vPTcM95eT63ElgL/UkkLKhEcc9KncqDQopo3FCJX+KxjFB4yDvqxnHRGq+FA+rRHI/jgtU9jguEqmFOO5TuafxhkitxnGTyj2MS1TyK1lIiiVjj4b9EOVnk/t7X/2Dn1lUlAgKCgoKCgoKCgoKCgoKCgr639WgUqn0nB8w7i3rFfpXl38/9Xi9dlKjUq9XyJ9Y/i+pOoXzsGFfH8T25DvDr1VlGH8neqouf0W/f9yP5QFuwz1fg1eXN3HK4XjlWOabVDWRmnyReaIPyEeowHHHraJ3bM6aY32k3TVvEWvrhMZw17RUVf08wKWx9lW+os/yuDovPsCFuqGhT+vxr0Hz9A1JY6Orc387WheoBMwH6KXuycCn8WK/ps4x17TNy1ifiLfN2bi2wy2sQ9CIOce4uBxEjV6tqPJwoHmGvLHcaBzMkKF+QoB8g9U8leltIdZ/B/BX3dznw+Dd3GGjc1DpjBUNVtCwzYoatNkt04ZswGtbqVaBf5lVJ/DXoKEby19hl+ebTV0hWKcH3ij+Wq/Vam3+O+RtRaOky29Hhdg8v2gUxcOJWmzcr606jrWqrg+XEY1JPGS9gh5qNOwj6CFjQz0ZitGjqMpQVLFB3VhSvzc+MuNTK4/unaiWT+JX9ZjH/Fb5JJ5i6xgN+KOmhtXAdtyx33FT/BCmILMzGpw5RABDo8eHz9f4xdw9sGgseScwb3HrqMTeQwyWNDSfhtKIFBo60lRlPtYJHdlxFd3BrnWaRvQi/dmhMfJoLNHPwEc6ODEag65qlqEhuqmA8dk0xLcYmv5jtQr6fohU33KhmsS1i0ZFjo96/KqeTnb0Zdc6Q6Mp3cuhUfS+xZG3HbsIhXEavGARDAwNFu5EFxgANg3xYel/3YSHXGemGaIBLu4cQgOyabB8Y/5PgVU0AbNK+hbXOkNjSo8UHopH1Tp6gFqyHy53IOAIGspETaMO5MaoIRaNDqD86g9HPoVv2kvTfsZnhDOMoEzWgEIfpP3PolGEfC/MyiqMiTjVOk2jrlyTWfuyUWqamW5SkHPpxH64XIeJFWi0YUhqGhtoZjs2T4EtGmOglyeeKcvZdFiqgo908VQIRXcjalJ0aECM7vLc8JLAUvu2Z52k0eAT2kBZi2dYNvLM//eiJs3EHmvR4NGnYWh0ZTfgfsU0KjKe95yWQjP06mLSjZzhFumRzWjInjvQ49eiAb5eE9hFs/PKft86Vtj09VUsXXr67nFBS1pdyzeHCr/jGzWLxlLUoGicKLOOTRTANPrKsBL5MkNj2SkAkq64z5p5SorGjrjBcgy5AQVBsMj9cRLZ1vWVddrRdBgb0EtD1jlN8JWmHd+q4DKSBhipaDCCkzHXyBSKabD1rLg+eYnp9yy46kMxfbAefMHJcs22m0YeLOJDsc1GwkAvmX3r2EhZLpcdtNZJpSHCTU9EPmz5AVStaPT4/CtpLN1B59Co4ctWUy11ITaPrejdlXPFbhp8SdMFJ2TG9eqqMwnrZNw40WEjiwZ4VNdaBfXkoFM0eKblFIrgS3mpog5diAYbBW15fUO/VgOC/WDVmoersk920+CzeFsuQ1/jdkH1i21dG9HgBQwbFA38t4wYJfwl04ksSNPgMWkoimig2GhGvaHRQzVVnBhZR28idWG510AbAvEN4MaeNJrxcCNB9uPiSM6dlHV6hi2qwOrQiF87agbJy0mYb+QkjkZJ3aZpiP2WKKKD580DZaGhYX3j1Vlb1eID5SuNsSyHFywTl3pC3YMG30vJG8XYbCszzSJSWmdWXweSj0uDqVRl+7fCVO8buS3Taq1e45tZ6VKGBl/HiSKsRX1N+fkA7e1QrDiJrRfThNnFDttPFmNtEK/uoF2vtw9M2h40BiZqDWIdISnrDA0epZpww6jYlCoKC5SaqvcqIy+tamiwOMUngYqHVRnUVBagt/sGdhyt4O+n62xtk6bQ+fsosZO09j0vZnIeqWpI61BhjanoNW/1tawWJ6PhuHmCa1gWDqbT4cacStULfe3p1UKBGVsr9C1/lR8b/UJbJeA1xon9MRrUCs3Ry8vkuI1HULs4mk5HxTaqy38pq+YUVSuodTT/s6by+Nbhwrr9AlvrM2uN8v8CTcPkuxwMpt0AAAAASUVORK5CYII=\"></div></div></header>");
        		writer.write("<nav style=\"padding: 0px 48px;background: #336799;\">");                                                      http://www.sonata-software.com/sites/all/themes/sonata/logo.png
        	    writer.write("<div class=\"content-width\" style=\"padding: 0;margin: 0;font-family: Arial,sans-serif;\">");
        	    
        	    writer.write("<ul style=\"list-style:none;display: table;table-layout: fixed;width: 100%;height: 35px;margin: 0\">");
        	    writer.write("<Header style=\"background:#336799;padding: 2px;\">");
        		   /*    String[] labelArray= {"Versions","Smoke","Stability","Regression"};
        		      
        		       for(int l=0;l<labelArray.length;l++) { 
        		          writer.write("<li style=\"display: table-cell;vertical-align: middle;text-align: center;\">");
        		          writer.write("<a href=\"\" style=\"color: #FFFFFF;text-decoration: none;font-size: 20px;text-decoration: underline;\">"+labelArray[l]+"</a></li>");
        		       }*/
        		       
        		        writer.write("</ul></div></nav>");
        				
        				writer.write("<body>");
        				
        				writer.write("<div class=\"content\" style=\"min-height: 300px;\">");
        				

        				writer.write("<p style=\"font-size:130%\"><a href=\"http://"+ipaddress+":8080"+"/Reports/Regression/MasterRegressionReport.html\">Back</a>   <a href=\"http://"+ipaddress+":8080"+"/Reports/\">Home</a>");
        				

        	    //	writer.write("<div class=\"center-title\" style=\"display: inline-block;vertical-align: center; width: calc(100% - 20px);text-align: center;\"><h1 style=\"font-size: 18px;color: #092a5e;\">Smoke Test</h1> </div>");
        		        writer.write("<br><br><table align=\"center\"border=\"2\" style=\"width:1205px;\">");
        				writer.write("<tr><th bgcolor=\"  #b7e5f9  \" style=\"text-align:center;\"><b>SL.NO</th>");
        				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>SCENARIO NAME</b></th>");	
        				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>STATUS</b></th>");	
        				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>ERROR MESSAGE</b></th>");	
        				//writer.write("<th bgcolor=\"  #b7e5f9  \"><b>BOOKING ID</b></th>");
        				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>DATE OF EXECUTION</b></th></tr>");
        				
        				count = 1;
        			
        		
        				
        				for ( Scenario scen : scenarios) {
        					scene = scen.getScenarioNameReg();
        			    	status = scen.getStatus();
        			    	technicalDifficultes = scen.getTechnicalDifficultes();
        			    	technicalDifficultesMessage = scen.getTechnicalDifficultesMessage();
        			    	bookingId = scen.getBookingId();
        			    	dateOfExecution = scen.getDateOfExecution();
        			    	fileName= scen.getfileName();
        			    	writer.write("<tr><td align=\"center\"><b>"+count+"</b></td>");
        			    	writer.write("<td><a href=\""+folderPath+"\\"+fileName+"\" target=\"_blank\"><b>"+scene+"</b></a></td>");

        			    	if(status.equals("true")) {
        			    		if(technicalDifficultes.equals("true")) {
        			    			status = "Fail";
        			    			
        			    		}
        			    		else {
        			    		status = "Fail";
        			    
        			    		}
        			    		writer.write("<td bgcolor=\"  #FF0000  \" align=\"center\"><b>"+status+"</b></td>");
        			    		writer.write("<td align=\"center\"><b>"+technicalDifficultesMessage+"</b></td>");
        			    		failcount++;
        			    	}
        			    	else {
        			    		status = "Pass";
        			    		writer.write("<td bgcolor=\"  #008000  \" align=\"center\"><b>"+status+"</b></td>");
        			    		writer.write("<td></b></td>");
        			    		passcount++;
        			    	}
        			    	
      			    	
        			    	/*
        			    	if(technicalDifficultes.equals(true)) {
            					
            					}
            					else {
            						writer.write("<td align=\"center\"><font color=\"red\"><b>Technical Difficulties</b></td>");
            						bookingId = "NIL";
            					}
            					*/
        			    	
        			    	//	writer.write("<td align=\"center\"><b>"+bookingId+"</b></td>");
        			    	
        			    	writer.write("<td align=\"center\"><b>"+dateOfExecution+"</b></td>");
        			    	
        			    	count++;
        				}
        				
        				
        				writer.write("</table><br><br>");
        				writer.write("</div>");
        				
        				
        				  writer.write("<br><br><table align=\"center\"border=\"2\" style=\"width:1205px;\">");
          				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>STATUS</b></th>");	
          				writer.write("<th bgcolor=\"  #b7e5f9  \"><b>COUNT</b></th></tr>");	
          		
          		    	writer.write("<tr><td align=\"center\"><b>PASS </b></td>");
			    		writer.write("<td align=\"center\"><b>"+passcount+"</b></td></tr>");
			    	
			    		writer.write("<tr><td align=\"center\"><b>FAIL </b></td>");
			    		writer.write("<td align=\"center\"><b>"+failcount+"</b></td></tr>");
			    		
			    		writer.write("<tr><td align=\"center\"><b>TOTAL</b></td>");
			    		writer.write("<td align=\"center\"><b>"+scenarios.size()+"</b></td></tr>");
			    		writer.write("</table><br><br>");
			    		writer.write("</div>");
        				
       			
        		        writer.write("<footer style=\"background:#336799;padding: 5px;\">");
        	    // writer.write("<div style=\"text-align:center;\">");
        	    //  writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n" +
        	    //	"<span style=\"background: green;padding: 1px 10px; \"></span>&nbsp; Passed </div>  ");
        		        
        		        /*writer.write("<div style=\"display:inline-block; padding-right: 10px;color: #FFFFFF\">\r\n" + 
        		        		"<span style=\"background: #FFBF00; padding: 1px 10px; \"></span>&nbsp;70-99% Qualified</div>");*/

        	    //    writer.write("<div style=\"display:inline-block;padding-right: 10px;color: #FFFFFF \">\r\n" +
        	    //  		"<span style=\"background: red;padding: 1px 10px;\"></span>&nbsp;Failed</div>");
        		        
        		        writer.write("</footer></div></body>");
        				writer.write("</body>");
        				writer.write("</html>");

        				writer.close();
        	}
        	
        
       
        
		
		

} // close of the main class
		}}

//}
