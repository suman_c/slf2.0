title executionReport
@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
set slf-framework=%WORKSPACE%
set jars-dir=%WORKSPACE%\Jars
if not exist %jars-dir% (mkdir %jars-dir%)
call downloadSLFJarAndDrivers.bat "%slf-framework%"
javac -d %slf-framework%\bin -cp "%jars-dir%\*;%jars-dir%\AppliTool_Libraries\*;%jars-dir%\*" %slf-framework%\src\com\driver\DriverExcuteThrough_Threads.java %slf-framework%\src\com\driver\Driver.java src\com\utilities\DriverReusable.java %slf-framework%\src\com\commonfunctions\CommonFunctions.java src\com\commonfunctions\Action.java %slf-framework%\src\com\utilities\Constants.java src\com\utilities\ReadExcel.java %slf-framework%\src\com\utilities\*.java  %slf-framework%\src\com\summaryreportgenerator\*.java 
::javac -d bin -cp "libraries\*;libraries\AppliTool_Libraries\*;drivers\*" src\com\commonfunctions\CommonFunctions.java;%slf-framework%\src\com\summaryreportgenerator\CreateAutomationExecutionReport.java
java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.summaryreportgenerator.CreateAutomationSummaryReport_ExcelReport
java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.summaryreportgenerator.CreateAutomationExecutionReport
java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.summaryreportgenerator.CreateAutomationSummaryReport
java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.summaryreportgenerator.CreateMasterSummaryReport
java -cp ".;%slf-framework%\bin;%jars-dir%\*" com.summaryreportgenerator.CreateJunitXMLReport
pause
::exit 0