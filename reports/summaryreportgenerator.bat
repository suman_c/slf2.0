title summaryReport
@echo off
set projectpath=%WORKSPACE%

javac -d bin -cp com\summaryreportgenerator\CreateAutomationSummaryReport.java  com\utilities\ExportSummeryReport.java com\utilities\SummaryReportWithDefects.java com\utilities\SummaryReportVO.java

java -cp ".;%projectpath%\bin;%projectpath%\Jars\*" com.summaryreportgenerator.CreateAutomationSummaryReport

::exit 0