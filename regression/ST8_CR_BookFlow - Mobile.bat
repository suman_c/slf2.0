@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
set slf-framework=C:\Automation\SLF2\phoenix-slf-framework\
set jars-dir=%slf-framework%\..\Jars
taskkill /im chromedriver.exe /f
taskkill /im chrome.exe /f
::if not exist %jars-dir% (mkdir %jars-dir%)
::call downloadSLFJarAndDrivers.bat "%slf-framework%"
javac -d %slf-framework%\bin -cp "%jars-dir%\*;%jars-dir%\AppliTool_Libraries\*;%jars-dir%\*" %slf-framework%\src\com\driver\DriverExcuteThrough_Threads.java %slf-framework%\src\com\driver\Driver.java %slf-framework%\src\com\utilities\DriverReusable.java %slf-framework%\src\com\commonfunctions\CommonFunctions.java %slf-framework%\src\com\commonfunctions\Action.java %slf-framework%\src\com\utilities\Constants.java %slf-framework%\src\com\utilities\ReadExcel.java

:::::::::::::::::::::::::::::::::ST8 Environment:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-FlyCR_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-CnS_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-SnC_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-BtoBCR_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-RepoCR_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-CabinOnly_Bookflow-mobile
java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads CR-ExUKRepoCR_Bookflow-mobile





:::::::::::::::::::::::::::::::::ST8 Environment:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
endlocal
goto :eof

pause
endlocal
goto :eof